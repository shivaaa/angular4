import {
  Component,
  OnInit
} from '@angular/core';

// import { AppState } from '../../app.service';

@Component({
  
  selector: 'footer', 
  providers: [ ],
  styleUrls: [ './footer.component.css' ],
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  loginType: string;
  displayLoginPopup: boolean;
  login: string;
  public localState = { value: '' };

  constructor(
    // public appState: AppState,
  ) {}

  public ngOnInit() {
    this.login=localStorage.getItem('isLoggedIn');
    console.log('hello `footer` component');
  }
  displayLogin(type){
    if(type=='login'){
    this.loginType='login'
    }
    else
    this.loginType='signup'
    this.displayLoginPopup = true; 
  }
  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false; 
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
