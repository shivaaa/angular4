import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class GalleryService {
    private environmentUrl :  any = environment;


  constructor(
  	    private http: Http,
    ) { 

  }

//   gallery(body) : Observable<any>{
//   	let galleryUrl = this.environmentUrl.baseUrl + "search-ms/collectionsByCeleb";
//       let headers = new Headers({ 'Content-Type': 'application/json' });
//     //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
//       let options = new RequestOptions({ headers: headers });
//       return this.http.post(galleryUrl, body, options)
//   		.map((res:Response) => res.json())
//   		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
//   }
  gallery(body) : Observable<any>{
    let galleryUrl = this.environmentUrl.baseUrl+'search-ms/collectionsByCeleb';
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(galleryUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  // like(obj : any) : Observable<any>{
  //     let likeUrl = this.baseUrl + "postCardStatus";
  //     let body = obj;
  //     let headers = new Headers({ 'Content-Type': 'application/json' });
  //   //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
  //     let options = new RequestOptions({ headers: headers });

  //     return this.http.post(likeUrl, body, options)
  // 		.map((res:Response) => res.json())
  // 		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  // }

}
