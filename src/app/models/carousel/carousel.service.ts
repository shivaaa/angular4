import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Carousel } from './carousel';
import { environment } from "../globals";

@Injectable()
export class CarouselService {
    private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  getCarouselBySlug(){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    let brandUrl = this.environmentUrl.baseUrl+"designer-ms/designers";
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAllDesignerCollections(){
      //let brandUrl = this.baseUrl+'brand/'+slug;
      let brandUrl = this.environmentUrl.elasticUrl+'widgets/_search?pretty=true&q=collectionType:"Designer Collection"';
      return this.http.get(brandUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAllCatagoryCollections(){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    let brandUrl = this.environmentUrl.elasticUrl+'widgets/_search?pretty=true&q=collectionType:"Catagory Collection"';
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
getAllDesignerProducts(){
   //let brandUrl = this.baseUrl+'brand/'+slug;
   let brandUrl = this.environmentUrl.elasticUrl+'products/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=designer:*';
   return this.http.get(brandUrl)
   .map((res:Response) => res.json())
   .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}

  getShopByVideo(){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    // let brandUrl = this.baseUrl+"shop-by-videos-ms/getShopByVideosById/81e8b665-4054-47d5-ac28-e8132d40e272";
    let brandUrl = this.environmentUrl.baseUrl+"shop-by-videos-ms/shopByVideos";
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getCatogeries(){
    let brandUrl = this.environmentUrl.baseUrl+'category-ms/category';
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
 }