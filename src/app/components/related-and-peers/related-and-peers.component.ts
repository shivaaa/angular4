import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output
} from '@angular/core';
import { Router } from '@angular/router';
// import { AppState } from '../../app.service';
import { MovieService } from "../../models/movie/movie.service";
import { LikeService } from '../../models/like/like.service';

declare var $:any;
@Component({
  
  selector: 'related-and-peers', 
  providers: [ ],
  styleUrls: [ './related-and-peers.component.css' ],
  templateUrl: './related-and-peers.component.html'
})
export class RelatedAndPeersComponent implements OnInit {
  movieStatusArray=[];
  selectedArray=[];
  index: string;
  page: string;
  dataArray = [];
  public showTopmovies: boolean;
  public userid: string;
  public movieObject: any;
  public moviename: any;
  public displayLoginPopup : boolean = false;
  @Input() pageType: any;
  @Input() relatedMovies : any;
  @Output() feedFilter : EventEmitter<any> = new EventEmitter();
  @Output() selectedCatagory : EventEmitter<any> = new EventEmitter();
  // @Input() topmoviesdata : any;
  @Input() data : any;
  @Input() categoryArray : any;
  @Input() peers : any;
  @Input() movieCast : any;
  public displayRatingPopup : boolean = false;
  public localState = { value: '' };
  public userrating:any;
  public userratings = [];
  public movienames=[];
  public isUserRatedArray;
  public userRatingArray;
  public currentRatingIndex;
  public nameofmovie:any;
  public value:number=0;
  public isLoggedIn : boolean = false;
  //public $: any;
  //public jQuery: any;
  

  constructor(
    // public appState: AppState,
    public router: Router,
    public movieService:MovieService,
    public likeService: LikeService,

  ) {}

  public ngOnInit() {
    $(document).ready(function() {

      $(function() {
    $(".fs-Accordion .expand").on( "click", function() {
      $(this).next().slideToggle(200);
      //$expand = $(this).find(">:first-child");
      
      if($(this).find(">:first-child").text() == "+") {
        $(this).find(">:first-child").text("-");
      } else {
        $(this).find(">:first-child").text("+");
      }
    });
  });
  $('.fs-Accordion .detail > ul li').click(function(){
      $(this).toggleClass('active');
      $(this).find(".check").toggleClass('active');
    });
    //$('.check').click(function(){alert();
    //$(this).parent().toggleClass('active');
    //$(this).toggleClass('active');
    //});

    $('.Mob-Srtby h3 span').click(function(){
    $('.Mob-Srtby').hide();
    });
    $('.Mob-Filtr h3 span').click(function(){
    $('.Mob-Filtr').hide();
    });

    $('.Mob_collections ul li:first-child').click(function(){
    $('.Mob-Srtby').show();
    });
    
    $('.Mob_collections ul li:last-child').click(function(){
    $('.Mob-Filtr').show();
    });
  });
    this.index='whatsnew';
    console.log('hello `related-and-peers` component');
    console.log('hello ' )
    console.log(this.relatedMovies);
    // console.log(this.topmoviesdata);
    console.log(this.peers);
    console.log(this.movieCast);
   console.log(this.data);

    if(localStorage.getItem("isLoggedIn") == "true") {
      this.isLoggedIn = true;
    }
   
    if(this.data){
      this.movieStatusArray=[];
      console.log(this.data);
      for(let i=0;i<this.data.length;i++){
        this.dataArray.push(this.data[i]._source);
        this.checkMovieStatus(this.data[i]._source.id,i);
      }
    }
    var urlBreakup = this.router.url.split('/');
    console.log(urlBreakup);
    this.page = urlBreakup[1];
    
    // let userrating ={
    //   nameofmovie:'',
    //   rating:'',
    // };

    // if(this.dataArray){
    //   for(let i=0;i<this.dataArray.length;i++){
    //     this.checkMovieStatus(this.dataArray[i].id);
    //   }
    // }

  }

  // ratingPopup (movie, index) {  
   
  //   this.movieObject=this.topmoviesdata.Items[index];
  //   this.currentRatingIndex = index;
  //   this.displayRatingPopup = true;
  // }

  // submitRating () {
  //   this.displayRatingPopup = false;
  // }
  
  // submitingRatingPopup (userrating) {
  //   if(userrating){
  //   console.log(userrating);
  //   this.isUserRatedArray[this.currentRatingIndex] = "true";
  //   this.userRatingArray[this.currentRatingIndex] = userrating.rating;
  //   this.displayRatingPopup = false;}
  //   else
  //     this.displayRatingPopup = false;

  // }

  // public ngOnChanges(changes) {
    
  //   if(changes.topmoviesdata && changes.topmoviesdata.currentValue) {
  //     console.log(changes);
  //     console.log(changes.topmoviesdata.currentValue);
  //     this.isUserRatedArray = new Array(changes.topmoviesdata.currentValue.Items.length);
  //     this.userRatingArray = new Array(changes.topmoviesdata.currentValue.Items.length);
  //     console.log(changes.topmoviesdata.currentValue.Items[0]);
  //     for(let i=0; i<this.isUserRatedArray.length; i++) {
  //       console.log(changes.topmoviesdata.currentValue.Items[0]);
  //       this.userid= localStorage.getItem('username');
  //       let userdata={
          
  //         'entityId':changes.topmoviesdata.currentValue.Items[i].id,
  //         'userId': String(this.userid),
         
  //       };
  //       // this.checkIfUserRated(userdata,i);
  //     }
  //     console.log(this.isUserRatedArray);
  //   }
  // }

  // checkIfUserRated(userdata, index) { 
  //   this.movieService.checkUserRating(userdata).subscribe(
  //     res => {
  //         console.log(res);
         
  //         if(res.message.Items[0]){
            
  //           this.isUserRatedArray[index] = "true";
  //           this.userRatingArray[index] = res.message.Items[0].ratingValue;
       
  //       }
  //       else
  //         this.isUserRatedArray[index] = "false";
          
  //       },
  //       error => {
  //         console.log("Error in rating orders");
         
  //       }
  // );
    
    
    
    
    
   
  // }
  loginPopup() {
    // this.displayLoginPopup = true;
    let urlBreakup = this.router.url.split('/');
    urlBreakup.splice(0, 1);
    let redirect = urlBreakup.join('/');
    localStorage.setItem("preLogin",redirect);
    // this.router.navigate(['login']);  
    this.displayLoginPopup = true; 
  }
  getMovieStatus(movie){
    // this.checkMovieStatus(movie);
    console.log(movie);
    return true;
  }
  checkMovieStatus(movieId,index){
    let profileDataObj = {
      id : this.dataArray[index].id,
      name : this.dataArray[index].name,
      slug : this.dataArray[index].slug,
      profilePic : this.dataArray[index].profilePic,
      type : 'movie'
    };
    let isFollowBody = {
      userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
      entityId : movieId,
      type : "follow"
    };
    this.likeService.isLiked(isFollowBody).subscribe(
      res => {
          console.log(res);
        if(res.data) {
          this.movieStatusArray.push(res);
          // this.statusArray[i]=res;
          // console.log(res.totalCount);
         
        }
        },
        error => {
          console.log("Error");
        }
  );

  }
  selectIndex(subModule,i){
    if(this.selectedArray.indexOf(subModule)==-1){
      this.selectedArray.push(subModule);
    }
    else{
      
      this.selectedArray.splice(this.selectedArray.indexOf(subModule),1);
    }
    // this.categoryArray[subModule].active = !this.categoryArray[subModule].active; 
console.log(subModule);
this.selectedCatagory.next( this.selectedArray);
  }
  changeStatus(movieItem,index){
    this.likeMovieStatus(movieItem.data.LastEvaluatedKey.id,index);
    if(movieItem.data.Count=='0'){
      movieItem.data.Count='1';
      movieItem.totalCount=+movieItem.totalCount+1;
    }
    else{
      movieItem.data.Count='0';
      movieItem.totalCount=+movieItem.totalCount-1;
    }
    console.log(movieItem);
    
  }
  likeMovieStatus(movieId,index){
   let profileDataObj = {
      id : this.dataArray[index].id,
      name : this.dataArray[index].name,
      slug : this.dataArray[index].slug,
      profilePic : this.dataArray[index].profilePic,
      type : 'movie'
    };
    let followBody = {
      userId : localStorage.getItem("username"),
      entityId : this.dataArray[index].id,
      entityObj : profileDataObj,
      type : "follow"
    };
    this.likeService.like(followBody).subscribe(
        res => {
            console.log(res);
          },
          error => {
            console.log("Error in posting follow");
          }
    );
  }
  checkActive(subModule){
    for(let i=0;i<this.selectedArray.length;i++){
      if(this.selectedArray[i]==subModule)
      return true;
    }

  }

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false; 
  }

  gotomoviepage(first,second){
    // this.router.navigate([first,second]);
    this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
    this.router.navigate([first , second]));
  }
  eventEmitt(type){
    this.index=type;
  console.log(type);
  this.feedFilter.next(type);
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
