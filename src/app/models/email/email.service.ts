import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class EmailService {
  private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  sendEmail(body) : Observable<any>{
    let emailUrl = this.environmentUrl.baseUrl+'email-ms/sendEmail';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(emailUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  shareCardViaEmail(body) : Observable<any>{
    let emailUrl = this.environmentUrl.baseUrl+'email-ms/sendFeedUrl';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(emailUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  

}
