import { Injectable, Inject } from "@angular/core";
import { Jsonp, Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs";
import { Fashion } from "./fashion";
import { environment } from '../globals';

@Injectable()
export class FashionService {
    public baseUrl = 'http://mock.flikster.com:4000/';

    private environmentUrl :  any = environment;

    constructor(
        private http : Http,
    ){}
    
    shopTheLook(){
        // let fashionUrl = this.environmentUrl.baseUrl+"looks-ms/looks";        
        let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"shop-the-look"';                

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    getshopbyvideosbyIndustry(industry){
        let fashionUrl = this.environmentUrl.elasticUrl+'shopbyvideos/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=industry:"'+industry+'"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    getAllShopbyVideos(){
        let fashionUrl = this.environmentUrl.elasticUrl+'shopbyvideos/_search?pretty=true&sort=createdAt:desc&size=1000&q=*';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }
    getAuctionData(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'auctions/_search?pretty=true&sort=createdAt:desc&size=10&q=status:"active" AND "'+type+'".name:"*"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    
    }
    getshopbyvideosByBrand(industry){
        let fashionUrl = this.environmentUrl.elasticUrl+'shopbyvideos/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=industry:"'+industry+'" AND category: "Brand"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }


    allgetshopbyvideos(){
        let fashionUrl = this.environmentUrl.elasticUrl+'shopbyvideos/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=*';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }
    getshopbyvideos(page,industy){
        let fashionUrl = this.environmentUrl.elasticUrl+'shopbyvideos/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=*%20AND%20industry:"'+industy+'"%20AND%20'+page;        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }
    getLatestProducts(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?size=20&pretty=true&sort=createdAt:desc&q=status:ACTIVE%20AND%20tags:"'+type+'"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getClothingProducts(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?size=20&pretty=true&sort=createdAt:desc&q=status:ACTIVE%20AND%20tags:"'+type+'"%20AND%20tags:"clothing"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getBrands(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND brand:*%20AND%20tags:"'+type+'"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))


    }
    getFootwareProducts(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND tags:"'+type+'" AND category:"footwear"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getShirtsProducts(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND tags:"menfashion"%20AND%20category:"tees"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getSmallScreenProds(){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND celeb.role:%27anchor%27';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getBigScreenProds(industry){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND movie.name:* AND industry:"'+industry+'"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))


    }
    getAccessories(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND tags:"'+type+'" AND category:"accessories"';        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    getDesignerProducts(type){
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=10&q=status:ACTIVE AND designer:*%20ANDcategory:'+type;        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error')) 
    }
    getBrandDetails(slug){
        let fashionUrl =this.environmentUrl.baseUrl+'brand-ms/getBrandBySlug/'+slug;        
        // let fashionUrl = "http://apiv3.flikster.com/v3/looks-ms/looks";

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }
    

    latestObsession(){
        console.log(this.environmentUrl);
        let fashionUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND obsessionStatus:"true"';
        console.log(fashionUrl);
        // let fashionUrl = 'http://apiv3-es.flikster.com/products/_search?pretty=true&q=obsessionStatus:"true"';

        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    styleBoard(){
        let fashionUrl = this.environmentUrl.elasticUrl+'styleboard/_search?pretty=true&sort=createdAt:desc&q=status:"ACTIVE"';
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    stealThatStyle(industry){
        let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"steal-that-style" AND industry:"'+industry+'"';                        
        console.log(fashionUrl);
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    stealThatStyleFashion(industry,type){
        if(type=='celebrity' || type=='movie'){
            let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"steal-that-style" AND tags:"'+type+'-fashion" AND industry:"'+industry+'"';                        
            console.log(fashionUrl);
            return this.http.get(fashionUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

        }
        // if(type=='movie'){
        //     let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"steal-that-style" AND tags:"'+type+'-fashion" AND industry:"'+industry+'"';                        
        //     console.log(fashionUrl);
        //     return this.http.get(fashionUrl)
        //     .map((res:Response) => res.json())
        //     .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

        // }
        else{
        let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"steal-that-style" AND tags:"'+type+'-fashion"';                        
        console.log(fashionUrl);
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }
    }
    stealThatStyleCelebStore(industry,slug){

         let fashionUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND type:"steal-that-style" AND industry:"'+industry+'" AND tags:"'+slug+'"';                        
        console.log(fashionUrl);
        return this.http.get(fashionUrl)
                .map((res:Response) => res.json())
                .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
            }

    bestSelling(industry,count){
        let fashionUrl = this.environmentUrl.elasticUrl+'trending/_search?pretty=true&sort=rank:asc&size='+count+'&q=type:"trending"%20AND%20industry:"'+industry+'"';
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    bargain(type){
        let fashionUrl
        if(type == "menfashion")
            fashionUrl = this.environmentUrl.elasticUrl+'products/_search?sort=createdAt:desc&size=10000&pretty=true&q=status:ACTIVE AND isMenfashion:true';
        if(type == "womenfashion")
            fashionUrl = this.environmentUrl.elasticUrl+'products/_search?sort=createdAt:desc&size=10000&pretty=true&q=status:ACTIVE AND isWomenfashion:true';
        if(type == "celeb-store")
            fashionUrl = this.environmentUrl.elasticUrl+'products/_search?sort=createdAt:desc&size=10000&pretty=true&q=status:ACTIVE AND isCelebstore:true';
        if(type == "movie-store")
            fashionUrl = this.environmentUrl.elasticUrl+'products/_search?sort=createdAt:desc&size=10000&pretty=true&q=status:ACTIVE AND isMoviestore:true';
        
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    shareyourstyle(){
        let fashionUrl = this.baseUrl+'/share-your-style';
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    getDesignerById(id){
        let fashionUrl = this.environmentUrl.elasticUrl+"designer-ms/getDesignerById/"+id;
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }

    getDesignerBySlug(slug) {
        let designerUrl = this.environmentUrl.baseUrl+"designer-ms/getDesignerBySlug/"+slug ;
        return this.http.get(designerUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getDesignerProductsByGender(type) {
        let designerUrl = this.environmentUrl.elasticUrl+"products/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:ACTIVE AND designer:*%20AND%20category:"+type+"%20fashion";
        return this.http.get(designerUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getCategoryWidgets(slug) {
        let designerUrl = this.environmentUrl.elasticUrl+'widgets/_search?pretty=true&q=location:"'+slug+'"';
        return this.http.get(designerUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getCategories(){
        let fashionUrl = this.environmentUrl.baseUrl+"category-ms/category ";
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }

    getImagesForSYS(type){
        let fashionUrl = "ADD END POINT";
        return this.http.get(fashionUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

    }

    getContentForSYS() {
        let SYSUrl = this.environmentUrl.baseUrl+"share-your-style-ms/shareYourStyles";
        return this.http.get(SYSUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    createContentForSYS(content : any) : Observable<any>{
        let newContentUrl = this.environmentUrl.baseUrl+"share-your-style-ms/createShareYourStyle";
        let body = content;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
        let options = new RequestOptions({ headers: headers });

        return this.http.post(newContentUrl, body, options)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
    }
}