import { BrowserModule,Title } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterOutlet,RouterModule,PreloadAllModules } from '@angular/router';
// import { appRoutingProviders, routing } from './app.routes';
import { YoutubePlayerModule } from 'ng2-youtube-player';
import { EmbedVideo } from 'ngx-embed-video';
import { FacebookModule } from 'ngx-facebook';
// import { ClipboardModule }  from 'angular2-clipboard';
/*
 * Platform and Environment providers/directives/pipes
 */
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { ImageZoomModule } from 'angular2-image-zoom';
// import { ZoomableDirective } from 'ng2-zoomable';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
// pages
import { HomeComponent } from './pages/home';
import { IndustryComponent } from "./pages/industry/industry.component";
import { ProfileComponent } from "./pages/profile/profile.component";
import { ProductPageComponent } from "./pages/product-page/product-page.component";
import { FashionComponent } from "./pages/fashion/fashion.component";
import { FeedComponent } from "./pages/feed/feed.component";
import { ContentPageComponent } from "./pages/content/content.component";
import { DummyComponent } from "./pages/dummy/dummy.component";
import { AuctionPageComponent } from './pages/auction/auction-page.component';
import { UserProfileComponent } from "./pages/userprofile/userprofile.component";

import { SeeAllComponent } from "./pages/see-all/see-all.component";
import { CheckoutComponent } from "./pages/checkout/checkout.component";
import { BrandStoreComponent } from "./pages/brandstore/brandstore.component";
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';
import { StaticPageComponent } from './pages/static-page/static-page.component';



//components 
import { TitlebarComponent } from './components/titlebar/titlebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CardAssociationdataComponent } from './components/card-associationdata/card-associationdata.component';
import { CardSocialbarComponent } from './components/card-socialbar/card-socialbar.component';
import { CardCommentsComponent } from './components/card-comments/card-comments.component';
import { RelatedAndPeersComponent } from "./components/related-and-peers/related-and-peers.component";
import { PhotosRsbComponent } from "./components/photos-rsb/photos-rsb";
import { CollectionsComponent } from "./components/collections/collections.component";
import { FeedBarComponent } from "./components/feedbar/feed-bar.component";
import { bannerComponent } from "./components/banner/banner.component";
import { castcrewComponent } from "./components/cast-crew/cast-crew.component";
import { ShareStyleComponent } from "./components/sharestyle/sharestyle.component";
import { ImageCollections } from './components/image-collections/image-collections.component';

//popups
import { GetTheAppComponent } from './popups/get-the-app/get-the-app.component';
import { LoginComponent } from "./popups/login/login.component";
import { RateMovieComponent } from "./popups/rate-movie/rate-movie.component";
import { UploadComponent } from "./popups/upload/upload.component";
import { EditCheckoutComponent } from './popups/edit-checkout/edit-checkout.component';

import { LooksCheckoutComponent } from './popups/looks-checkout/looks-checkout.component';
//cards
import { FsCardComponent } from './cards/fs-card.component';
import { CardNowshowingComponent } from './cards/card-nowshowing/card-nowshowing.component';
import { CardGalleryComponent } from "./cards/card-gallery/card-gallery.component";
import { UpCommingComponent } from "./cards/up-comming/up-comming.component";
import { cardtrailerComponent } from "./cards/card-trailer/card-trailer.component";
import { CardTweetComponent } from "./cards/card-tweet/card-tweet.component";
import { CardDialogueComponent } from "./cards/card-dialogue/card-dialogue.component";
import { NewsComponent } from "./cards/card-news/card-news.component";
import { QuoteComponent } from "./cards/card-quote/card-quote.component";
import { CardProductComponent } from "./cards/card-product/card-product.component";
import { UserReviewComponent } from "./cards/card-user-review/card-user-review.component";
import { StyleCardComponent } from "./cards/card-style/card-style.component";
import { CardyoutubeComponent } from "./cards/card-youtube/card-youtube.component";
import { RelatedFlicksComponent } from './cards/card-related-flicks/card-related-flicks.component';
import { CriticReviewComponent } from './cards/card-critic-review/card-critic-review.component';



//services
import { CelebService } from "./models/celeb/celeb.service";
import { MovieService } from "./models/movie/movie.service";
import { ProductService } from "./models/product/product.service";
import { SearchService } from "./models/search/search.service";
import { FashionService } from "./models/fashion/fashion.service";

import { ContentService } from "./models/content/content.service";
import { UserService } from "./models/user/user.service";
import { BrandService } from "./models/brand/brand.service";
import { CarouselService } from "./models/carousel/carousel.service";
import { CardService } from "./models/card/card.service";

import { CommentsService } from "./models/comments/comments.service";
import { LikeService } from "./models/like/like.service";

import { TransactionService } from "./models/transaction/transaction.service";
import { GalleryService } from './models/gallery/gallery.service';
import { EmailService } from './models/email/email.service';

import { SeoService } from './models/seo/seo.service';





import '../styles/styles.scss';
import '../styles/headings.css';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ShareButtonsModule } from 'ngx-sharebuttons';
//import {ShareModule} from 'ng2share/share.module'
import { ShareModule } from '@ngx-share/core';
import { HttpClientModule } from "@angular/common/http";
import { HttpClientJsonpModule } from "@angular/common/http";

import { SocialLoginModule, AuthServiceConfig } from "angular4-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angular4-social-login";
import { AuthService } from "angular4-social-login";

// import { MetaModule, MetaService } from 'ng2-meta';
// import { Title } from '@angular/platform-browser';
// import { MetaService } from 'ng2-meta';
import { MetaModule } from '@ngx-meta/core';
import { MetaGuard } from '@ngx-meta/core';
import {
    MetaLoader,
    MetaStaticLoader,
    PageTitlePositioning
} from '@ngx-meta/core';


export function metaFactory(): MetaLoader {
    return new MetaStaticLoader({
      applicationName: 'Flikster',
      pageTitleSeparator: ' - ',
      pageTitlePositioning: PageTitlePositioning.AppendPageTitle,
        
       
        defaults: {
            // title: 'Latest Movie News | Trailers | Telugu | Hindi | Tamil | Malayalam | Kannada | Buy Celebrity Costumes & Accessories Online',
            // description: 'Buy Celebrity Dresses and Accessories, Latest Movie News, Reviews, Box Office Collections, Celebrity Photos, Latest Trailers, Video Songs, Audio Songs, Gossips, Music, Trending Videos, Fun Videos, Celebrity Tweets, Movie Dialogues, Movie Store, Celebrity Store, First Looks, Tv Shows, Movie Store, Designer Store, Women FAshion, Men Fashion, Celebrity Dresses Auctions, Share Your Style and much more only on flikster.com',
            // 'og:image': 'https://bucket-for-image-test.s3.amazonaws.com/flikstre-logo2.png',
            // 'og:type': 'website',
            // 'og:keywords':'Movie News, Trending News, Latest Movie Trailers, Latest News, Movie Reviews,Celebrity Images, Video Songs, Music, Evergreen Songs, Funny Videos, Prank Videos, Popular Songs,Celebrity Gossips, Celebrity Dresses',

        }
    });
}

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("43629679493-besd8r9qe6n0d2hlnep1573ppkcgaqma.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("433236906796403")
  }
]);

export function provideConfig() {
  return config;
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ImageCollections,
    HomeComponent,
    TitlebarComponent,
    FooterComponent,
    GetTheAppComponent,
    CarouselComponent,
    FsCardComponent,
    RelatedFlicksComponent,
    CardAssociationdataComponent,
    CardSocialbarComponent,
    CardNowshowingComponent,
    CardGalleryComponent,
    UpCommingComponent,
    CardCommentsComponent,
    IndustryComponent,
    ProfileComponent,
    castcrewComponent,
    PhotosRsbComponent,
    RelatedAndPeersComponent,
    cardtrailerComponent,
    CollectionsComponent,
    CardTweetComponent,
    CardDialogueComponent,
    NewsComponent,
    bannerComponent,
    LoginComponent,
    ProductPageComponent,
    CardProductComponent,
    FashionComponent,
    FeedBarComponent,
    FeedComponent,
    ContentPageComponent,
    DummyComponent,
    RateMovieComponent,
    AuctionPageComponent,
    UserProfileComponent,
    QuoteComponent,
    EditProfileComponent,
    UploadComponent,
    SeeAllComponent,
    UserReviewComponent,
    CriticReviewComponent,
    StyleCardComponent,
    CheckoutComponent,
    BrandStoreComponent,
    ShareStyleComponent,
    CardyoutubeComponent,
    StaticPageComponent,
    EditCheckoutComponent,
    LooksCheckoutComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    // ClipboardModule,
    YoutubePlayerModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpModule,
    ImageZoomModule,
    Ng2CarouselamosModule,
    EmbedVideo.forRoot(),
    RouterModule.forRoot(ROUTES, { useHash: false }),
    BrowserModule.withServerTransition({appId: 'my-app'}),
    FacebookModule.forRoot(),
    HttpClientModule, // (Required) for share counts
    HttpClientJsonpModule, // (Optional) For linkedIn & Tumblr counts
    ShareButtonsModule.forRoot(),
    SocialLoginModule,
    ShareModule.forRoot(),
    MetaModule.forRoot({
        provide: MetaLoader,
        useFactory: metaFactory,
    }),
    // RouterModule.forRoot(ROUTES, { preloadingStrategy: PreloadAllModules })
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    CelebService,
    MovieService,
    ProductService,
    SearchService,
    FashionService,
    ContentService,
    UserService,
    BrandService,
    CarouselService,
    CardService,
    CommentsService,
    TransactionService,
    LikeService,
    GalleryService,
    EmailService,
    SeoService,
    Title,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ]
})

export class AppModule { }
