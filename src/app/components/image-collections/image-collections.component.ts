
import {
    Component,
    OnInit
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  
  @Component({  
    
    selector: 'image-collection', 
    providers: [ ],
    styleUrls: [ './image-collections.component.css' ],
    templateUrl: './image-collections.component.html'
  })
  export class ImageCollections implements OnInit {
    public localState = { value: '' };
  
    constructor(
      // public appState: AppState,
    ) {}
  
    public ngOnInit() {
      console.log('hello `footer` component');
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }