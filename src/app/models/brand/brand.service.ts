import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Brand } from './brand';
import { environment } from '../globals';

@Injectable()
export class BrandService {
  private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  getBrandBySlug(slug){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    // let brandUrl = "http://apiv3.flikster.com/v3/brand-ms/getBrandById/78ed60c6-3fcd-4cab-8220-bd2d31800570";
    let brandUrl = this.environmentUrl.baseUrl+'brand-ms/getBrandBySlug/'+slug;
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAllBrands(){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    // let brandUrl = "http://apiv3.flikster.com/v3/brand-ms/getBrandById/78ed60c6-3fcd-4cab-8220-bd2d31800570";
    let brandUrl = this.environmentUrl.elasticUrl+'brands/_search?pretty=true&sort=createdAt:desc&size=1000&q=*';
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getallproducts(slug){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    // let brandUrl = "http://apiv3.flikster.com/v3/brand-ms/getBrandById/78ed60c6-3fcd-4cab-8220-bd2d31800570";
    let brandUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=1000&q=status:ACTIVE AND brand:"'+slug+'"';
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}