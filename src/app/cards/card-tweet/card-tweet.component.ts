import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
import { Router } from '@angular/router';
  
  @Component({
    
    selector: 'card-tweet', 
    providers: [ ],
    styleUrls: [ './card-tweet.component.css' ],
    templateUrl: './card-tweet.component.html'
  })
  export class CardTweetComponent implements OnInit {
    page: string;
    shareProfilePic: any;
    shareSlug: any;
    miliseconds: number;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    public cardSlug:any;
    public assnData: any;
    public localState = { value: '' };
    public isMiniCard : boolean;
    @Input() content: any;
    @Input() miniCard : any;
    public contentData;
    public cardId;
    public today: number = Date.now();

    constructor(
      // public appState: AppState,
      public router :Router
    ) {}
  
    public ngOnInit() {
      var urlBreakup = this.router.url.split('/');
      console.log(urlBreakup);
      this.page = urlBreakup[1];
      this.cardSlug=urlBreakup[2];
      console.log('hello `card-tweet` component');
      console.log(this.content);
     this.contentData=this.content;
     if(this.contentData)
      console.log(this.contentData);
     this.createdAt=this.contentData.createdAt;
     console.log(this.createdAt);
     this.cardId = this.contentData.id;
     this.shareSlug=this.contentData.slug;
     this.shareProfilePic = this.contentData.profilePic;
      if(this.contentData)
      if(this.contentData.celeb){
        if(this.contentData.celeb[0])
        this.assnData=this.contentData.celeb[0];
       }
      if(this.contentData)
      if(this.contentData.movie ){
        if(this.contentData.movie[0])
        this.assnData=this.contentData.movie[0];
       }
      
      console.log(this.assnData);
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='sec';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
      
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    gotocontent(){
      if(this.cardSlug !=this.contentData.slug)
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
  }
  