import {
    Component,
    OnInit, Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
import { Router } from '@angular/router';
  
  @Component({
    
    selector: 'card-quote', 
    providers: [ ],
    styleUrls: [ './card-quote.component.css' ],
    templateUrl: './card-quote.component.html'
  })
  export class QuoteComponent implements OnInit {
    page: string;
    shareProfilePic: any;
    shareSlug: any;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    assnData: any;
    @Input() miniCard: boolean;
    @Input() content : any;
    public localState = { value: '' };
    public isMiniCard : boolean = false;
    public contentData;
    public cardId;
    public cardSlug :any;
    public today: number = Date.now();
    constructor(
      // public appState: AppState,
      public router : Router,
    ) {}
  
    public ngOnInit() {
      var urlBreakup = this.router.url.split('/');
      this.page = urlBreakup[1];
      this.cardSlug=urlBreakup[2];
      console.log('hello `quote` component');
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      console.log(this.isMiniCard);
      console.log(this.content);
     
      
      if(this.content._source){
        this.contentData = this.content._source;
      this.createdAt=this.content._source.createdAt;
      this.cardId = this.content._id;
      }
      else{
        this.contentData = this.content;
        this.createdAt=this.content.createdAt;
        this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;

      }
      if(this.contentData){
      if(this.contentData.celeb)
        this.assnData=this.contentData.celeb[0];
    
        if(this.contentData.movie)
          this.assnData=this.contentData.movie[0];
      }
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='s';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
    }
  
    gotocontent(){
      if(this.cardSlug !=this.contentData.slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }

    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

  }
  