import {
    Component,
    OnInit,
    EventEmitter,
    Output
  } from '@angular/core';
  
// import { AppState } from '../../app.service';
import { FashionService } from "../../models/fashion/fashion.service";
import { ProductService } from "../../models/product/product.service";
import { ShareYourStyle } from "./share-your-style";
import { ActivatedRoute, Router } from "@angular/router";
require('aws-sdk/dist/aws-sdk');

  @Component({
    
    selector: 'upload', 
    providers: [ ],
    styleUrls: [ './upload.component.css' ],
    templateUrl: './upload.component.html'
  })
  export class UploadComponent implements OnInit {
    displayText: string;
    public localState = { value: '' };
    public isPhotoUploaded : boolean = false;
    public isProductTagged : boolean = false;
    public uploadAlert : boolean = false;
    public tagAlert : boolean = false;
    public isUploading : boolean = false;
    public cardsDispArray  = new Array(6);
    public carouselData;
    public shareYourStyle;
    public searchText;
    public globalSearchText;
    public searchResult;
    public globalSearchResult;
    public products;
    public pivot;
    public mainPicture;
    @Output() closeUploadPopup : EventEmitter<any> = new EventEmitter();
  
    constructor(
      // public appState: AppState,
      public fashionService : FashionService,
      public productService : ProductService,
      public router: Router,
    ) {}
  
    public ngOnInit() {
      console.log('hello `upload` component');
      this.shareYourStyle = new ShareYourStyle();
      this.shareYourStyle.userObject = {
        username : localStorage.getItem("name"),
        profilePic : localStorage.getItem("profilePic"),
      }
      this.shareYourStyle.serviceName = "share-your-style";
      this.shareYourStyle.createdBy = "Flikster"; 
      this.shareYourStyle.contentType = "share-your-style"
      this.changeDispCard(1,2);

      for(let index=0; index < this.cardsDispArray.length; index++) {
        this.cardsDispArray[index] = false;
      }
      this.cardsDispArray[0] = true;         //default card
    }

    closePopup() {
      this.closeUploadPopup.next();
    }

    changeDispCard(template, count) {
      this.shareYourStyle.styleTemplate = template.toString();
      this.shareYourStyle.product = new Array(count-1);
      this.shareYourStyle.image = new Array(count);
      for(let index=0; index < this.cardsDispArray.length; index++) {
        this.cardsDispArray[index] = false;
      }
      for(let index=0; index < this.shareYourStyle.image.length; index++) {
        this.shareYourStyle.image[index] = "../../../assets/img/blank.jpeg";
      }
      this.cardsDispArray[template-1] = true;
      console.log(this.cardsDispArray);
    }

    removePic(index) {
      console.log(index);
      if(index == 'main') {
        this.mainPicture = null;
        this.isPhotoUploaded = false;
      } else {
        this.shareYourStyle.image[index] = "../../../assets/img/blank.jpeg";
        this.shareYourStyle.product[index-1] = {};
      }
      let isProductTagged = false;
      for(let index=0; index < this.shareYourStyle.product.length; index++) {
        if(this.shareYourStyle.product[index]) {
          if(this.shareYourStyle.product[index].serviceName == "product")
            isProductTagged = true;
        }
      }
      if(!isProductTagged) {
        this.isProductTagged = false;
      }
      else
      this.isProductTagged = true;

      console.log(this.shareYourStyle.image);
      console.log(this.shareYourStyle.product);
    }

    setPivot(num) {
      this.pivot = num;  
      console.log(this.shareYourStyle.image);
      for(let index=0; index < this.shareYourStyle.image.length; index++) {
        if(this.shareYourStyle.image[index] == "../../../assets/img/blank.jpeg" || this.shareYourStyle.image[index] == "../../../assets/img/blank_upload.jpeg")
          this.shareYourStyle.image[index] = "../../../assets/img/blank.jpeg";
      }
      if(this.shareYourStyle.image[num] == "../../../assets/img/blank.jpeg" || this.shareYourStyle.image[num] == "../../../assets/img/blank_upload.jpeg")
        this.shareYourStyle.image[num] = "../../../assets/img/blank_upload.jpeg";
    }

    setPicture(item) {
      console.log(item);
      if(!this.pivot)
      this.pivot = 1;
      this.shareYourStyle.image[this.pivot] = item.profilePic;
      this.shareYourStyle.product[this.pivot-1] = item;
      console.log(this.shareYourStyle.product);
      this.isProductTagged = true;
      this.tagAlert = false;
      this.searchResult = [];
      this.globalSearchResult = [];
    }

    getCategoryImages(type) {
      this.fashionService.getImagesForSYS(type).subscribe(
        res => {
          console.log(res);
          this.carouselData = res;
        },
        error => {
          console.log("Error");
          this.carouselData = this.getMockCarouselData();
        }
      );
    }

    post() {
      if(this.shareYourStyle.title){
      this.shareYourStyle.slug = this.shareYourStyle.title.toLowerCase().split(' ').join('-');
      if(!localStorage.getItem("username")) {
        localStorage.setItem("preLogin","share-your-style");
        // this.router.navigate(['login']); 
      this.displayLoginPopup = true; 
      } else {
        if(!this.isPhotoUploaded) {
          this.uploadAlert = true;
        } else if(!this.isProductTagged) {
          this.tagAlert = true;
        } else {
          this.shareYourStyle.userId = localStorage.getItem("username");
          this.fashionService.createContentForSYS(this.shareYourStyle).subscribe(
            res => {
              console.log(res);
              this.closeUploadPopup.next();
              this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
              this.router.navigate(['share-your-style']));
            },
            error => {
              console.log("Error");
            }
          );
        }
      }
    }
    else{
      this.displayText='Please Add Title to your Style'
    }
    }

    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

    fileEvent(fileInput: any, type) {
      this.isUploading = true;
      let AWSservice = (<any>window).AWS;
      AWSservice.config.accessKeyId = 'AKIAJT7SFJQEJTU5AOKA';
      AWSservice.config.secretAccessKey = '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';
      let bucket = new AWSservice.S3({params: {Bucket : 'bucket-for-image-test'}});
      console.log(fileInput.target.files);
      for(let i=0; i<fileInput.target.files.length; i++) {
         let file = fileInput.target.files[i];
         let params = {Key : file.name, Body : file};
         var _self = this;
         bucket.upload(params, function(err, res){
           console.log('error', err);
           console.log('response', res);
           if(res) {
             _self.isUploading = false;
             if(type == "profilePic") {
              _self.shareYourStyle.image[0] = res.Location;
              _self.mainPicture = res.Location;
              _self.isPhotoUploaded = true;
              _self.uploadAlert = false;
             } else {
                if(!type)
                  type = 1;
                _self.shareYourStyle.image[type] = res.Location;
             }
           }
         })
      }
   }

    search() {
      if(this.searchText != "" && this.searchText != " ") {
        this.productService.searchProductBySlug(this.searchText)
        .subscribe(res => {
            console.log(res);
            this.searchResult = res.hits.hits;
          },
          error => {
            console.log("Error in search");
          }
        );
      }
    }

    globalSearch() {
      if(this.globalSearchText != "" && this.globalSearchText != " ") {
        this.productService.globalSearchRelProducts(this.globalSearchText)
        .subscribe(res => {
            console.log(res);
            this.globalSearchResult = res.hits.hits;
          },
          error => {
            console.log("Error in search");
          }
        );
      }
    }

  public displayLoginPopup : boolean = false;

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }

    getMockCarouselData() {
      return [
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "https://images-eu.ssl-images-amazon.com/images/I/41SHCddUziL._AC_UL260_SR200,260_.jpg",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            },
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "https://rukminim1.flixcart.com/image/312/312/shirt/s/h/y/46-bfrybluesht02-being-fab-original-imaekjr8ymhnxznp.jpeg?q=70",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            },
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "https://ae01.alicdn.com/kf/HTB1Dx7xLXXXXXaGXFXXq6xXFXXXQ/Mens-Dress-Shirts-Men-Clothes-Shirt-For-Men-Camisa-Masculina-Long-Sleeve-Fashion-Full-Sleeve-Shirts.jpg",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            },
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "http://aubertliano.com/img/products/men-shirts-2.jpg",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            },
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "https://4.imimg.com/data4/LB/NW/MY-23623090/mens-printed-shirt-500x500.jpg",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            },
            {
              name : "Mock Product 01",
              slug : "test-product-for-payment",
              id : "123456",
              profilePic : "http://www.dolchefashion.com/wp-content/uploads/2017/08/2017-brand-name-men-dress-shirts-mens-casual-shirts-latest-cgchsei-.jpg",
              assnSlug : "mahesh-babu",
              assnPic : "https://talesofore.files.wordpress.com/2015/04/blah-quotes-7.jpg",
              assnType : "celeb"
            }
          ];
    }
  }
  