import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';

// import { AppState } from '../../app.service';
import { EmailService } from '../../models/email/email.service';
import { MetaService } from '@ngx-meta/core';
@Component({
  selector: 'auction',
  providers: [],
  styleUrls: ['./static-page.component.css'],
  templateUrl: './static-page.component.html'
})

export class StaticPageComponent implements OnInit {

  public message: any;
  public show: boolean = true;
  public displayLoginPopup: boolean = false;
  public emailName;
  public email;
  public emailDataList;
  public emailTextarea;
  public slug: string;
  public localState = { value: '' };
  public pageType;
  constructor(
    // public appState: AppState,
    public router: Router,
    public emailService: EmailService,
    private metaService: MetaService,
  ) { }
  public ngOnInit() {

    window.scrollTo(0, 0);
    var urlBreakup = this.router.url.split("/");
    this.slug = urlBreakup[1];
    if (this.slug == 'about') {
      this.metaService.setTitle('About Flikster | Information | Flikster');
      this.metaService.setTag('description', 'Flikster is an attempt to capture the spirit of Indian Entertainment Industry. We cover the broad section of celebrity trends, fashion, beauty, latest celebrity updates including their movie trailers and the most viewed songs. We provide you a platform to Bid and Win auctions of celebrity memorabilia and the products owned by them.');
      this.metaService.setTag('keywords', 'Fashion, Movies, Entertainment');
      this.metaService.setTag('og:title', 'About Flikster | Information | Flikster');
      this.metaService.setTag('og:description', 'Flikster is an attempt to capture the spirit of Indian Entertainment Industry. We cover the broad section of celebrity trends, fashion, beauty, latest celebrity updates including their movie trailers and the most viewed songs. We provide you a platform to Bid and Win auctions of celebrity memorabilia and the products owned by them.');
      this.metaService.setTag('og:keywords', 'Fashion, Movies, Entertainment');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');

    }
    if (this.slug == 'privacy') {
      this.metaService.setTitle('Privacy Information | Flikster - Movis & Fashion');
      this.metaService.setTag('description', 'Latest Privacy news, comment and analysis from flikster');
      this.metaService.setTag('keywords', 'Privacy, News, Information');
      this.metaService.setTag('og:title', 'Privacy Information | Flikster - Movis & Fashion');
      this.metaService.setTag('og:description', 'Latest Privacy news, comment and analysis from flikster');
      this.metaService.setTag('og:keywords', 'Privacy, News, Information');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');

    }
    if (this.slug == 'terms') {
      this.metaService.setTitle('Terms Of Service | Flikster - Movies & Fashion');
      this.metaService.setTag('description', "User's Acknowledgment and Acceptance of Terms");
      this.metaService.setTag('keywords', ' Terms, Conditions, Acceptance');
      this.metaService.setTag('og:title', 'Terms Of Service | Flikster - Movies & Fashion');
      this.metaService.setTag('og:description', "User's Acknowledgment and Acceptance of Terms");
      this.metaService.setTag('og:keywords', ' Terms, Conditions, Acceptance');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');

    }
    if (this.slug == 'return-policy') {
      this.metaService.setTitle('Return Policy & Refunds');
      this.metaService.setTag('description', "To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging");
      this.metaService.setTag('keywords', ' Returns, Refunds, Exchanges');
      this.metaService.setTag('og:title', 'Return Policy & Refunds');
      this.metaService.setTag('og:description', "To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging");
      this.metaService.setTag('og:keywords', ' Returns, Refunds, Exchanges');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');

    }
    if (this.slug == 'careers') {
      this.metaService.setTitle('Careers | Flikster - Movies & Fashion');
      this.metaService.setTag('description', "Flikster aims at living best experience as a team. A job need not be a revision of what you already know, our dynamic platform provides an opportunity to learn while you explore");
      this.metaService.setTag('keywords', 'Flikster Careers, Flikster Jobs, Jobs In Flikster');
      this.metaService.setTag('og:title', 'Careers | Flikster - Movies & Fashion');
      this.metaService.setTag('og:description', "Flikster aims at living best experience as a team. A job need not be a revision of what you already know, our dynamic platform provides an opportunity to learn while you explore");
      this.metaService.setTag('og:keywords', 'Flikster Careers, Flikster Jobs, Jobs In Flikster');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');

    }
    if (this.slug == 'faq') {
      this.metaService.setTitle('Frequently Asked Questions | Flikster - Movies & Fashion');
      this.metaService.setTag('description', "Drop us an email, if you have queries about flikster account, refunds, orders, etc");
      this.metaService.setTag('keywords', 'Frequently Asked Questions, queries, flikster account');
      this.metaService.setTag('og:title', 'Frequently Asked Questions | Flikster - Movies & Fashion');
      this.metaService.setTag('og:description', "Drop us an email, if you have queries about flikster account, refunds, orders, etc");
      this.metaService.setTag('og:keywords', 'Frequently Asked Questions, queries, flikster account');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');
    }

  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
  showcontent() {
    this.show = !this.show;
  }
  sendEmail() {
    let isSendEmailBody = {
      name: this.emailName,
      mail: this.email,
      subject: this.emailDataList,
      message: this.emailTextarea
    };

    this.emailService.sendEmail(isSendEmailBody).subscribe(
      res => {
        if (res) {

          if (res.statusCode == 200) {
            this.message = "Email Sent Successfully";
            this.emailName = "";
            this.email = "";
            this.emailDataList = "";
            this.emailTextarea = "";

          } else {
            console.log(res.message);
          }
        }
      },
      error => {
        console.log("Error");
      }
    );

  }

  goToLogin() {
    this.show = false;
    this.displayLoginPopup = true;
  }

  closeLoginPopup() {
    console.log("event emmitted");
    this.displayLoginPopup = false;

  }
  close() {
    this.router.navigateByUrl('/dummy', { skipLocationChange: true }).then(() =>
      this.router.navigate(['home']));
  }
}