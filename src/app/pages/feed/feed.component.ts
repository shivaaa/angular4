import {
    Component,
    OnInit,
    Input,
    OnDestroy
  } from '@angular/core';
  import { ActivatedRoute, Router } from "@angular/router";
  // import { AppState } from '../../app.service';
  import { UserService } from "../../models/user/user.service";
  import { FashionService } from "../../models/fashion/fashion.service";
  import { ContentService } from "../../models/content/content.service";
  import { MovieService } from "../../models/movie/movie.service";
  import { ProductService } from "../../models/product/product.service";
  import { MetaService } from '@ngx-meta/core';
  import { Observable } from 'rxjs/Rx';
  @Component({
  
    
    selector: 'feed', 
    providers: [ ],
    styleUrls: [ './feed.component.css' ],
    templateUrl: './feed.component.html'
  })
  export class FeedComponent implements OnInit , OnDestroy {
    displayLoginPopup: boolean;
    contentFeed: string;
    allContentDataCoun: any;
    slugContent: any;
    contentFilter: string;
    displayBoutique: string;
    displayContent: string;
    profilePic: string;
    industry: string;
    topMovies: any;

    public allContentDatas: any;
    public slug: any;
    
    public localState = { value: '' };
    public followSuggesions : any;
    public bestSelling : any;
    public botiqueItems : any;
    public botiqueItemSlugs : any;
    public isCartItemSlugs : boolean = false;
    public isOrderItemSlugs : boolean = false;

    public trendingProds;
    public ordersList;

    public pageNo;
    public contentTypePage:number;
    public trailers;
    public contentType3 = "trailer";
    public topTrailers;
    public displayRatingPopup : boolean = false;
    public displayUploadPopup : boolean = false;
    public isShareYourStyle : boolean = false;
    public content ="content";
    public newscontent ="news";
    public gallerycontent="gallery";
    public content1 ="gallery";
    public Industry;
    public allContentData =[];
    public allContentDataCount;
    public allContentDataByType=[];
    public userContentData;
    public userContentDataCount;
    public likeOrBookmark;
    public sysContentData;
    public cardtype;
    public dialougecontent="dialouge";
    public content2 ="dialouge";
    public nowshowingcontent="nowshowing";
    public content3 ="nowshowing";
    public tweet="tweet";
    public content4 ="tweet";
    private alive: boolean = true;
    public typeToDisplay:string='share-your-style'

    // public show=5;
    constructor(
      // public appState: AppState,
      public user : UserService,
      public fashion : FashionService,
      public contentService : ContentService,
      public movieService : MovieService,
      public router: Router,
      public activatedRoute: ActivatedRoute,
      private productService : ProductService,
      private metaService: MetaService,
     
      //public contentData : ContentService,
      
      
      
    ) {
      
    }
    
    public ngOnInit() {
      Observable.interval(3000).takeWhile(() => this.alive).subscribe(() => this.checkLoadmore());
      this.allContentData=[];
      // this.contentTypePage=1;
      // this.pageNo = 1;
      window.scrollTo(0,0);
      this.profilePic=localStorage.getItem('profilePic');
      if(!this.profilePic)
        this.profilePic="http://www.hjakober.ch/wp-content/uploads/nobody_m_1024x1024.jpg";
      console.log('hello `feed` component');
      localStorage.setItem("rootRoute","feed");
      var urlBreakup = this.router.url.split("/");
      this.contentFeed=urlBreakup[1];
      this.industry =urlBreakup[2];
      this.contentFilter=urlBreakup[3];
      var ind =localStorage.getItem("Industry");
      if(ind){
        
        // this.metaService.setTitle(' '+this.industry);
        if(ind=='bollywood'){
          this.metaService.setTitle( 'Bollywood Movie News | Trailers | Reviews | Songs | Collections ');
          this.metaService.setTag('keywords','Bollywood News, Bollywood Movie Reviews, Bollywood Songs, Hindi Songs, Bollywood Trailers, Hindi Video Songs, Bollywood Actors, Bollywood Actress,Bollywood Movie Stills, Bollywood Movie Wallpapers, Evergreen Hindi Songs, Evergreen Love Songs');
          this.metaService.setTag('og:title', 'Bollywood News | Bollywood Movie Reviews | Bollywood Movie Songs | Bollywood Movies Collections | Bollywood Trailers');
          this.metaService.setTag('og:description','Latest Bollywood Movie News, Bollywood Movie Reviews, Rating, Trailers, Stills, Posters, Box Office Collctions, Interviews, Hindi Songs, Gallery, Bollywood Songs, Cast & Crew, Evergreen Hindi Songs, Dialogues, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Bollywood News, Bollywood Movie Reviews, Bollywood Songs, Hindi Songs, Bollywood Trailers, Hindi Video Songs, Bollywood Actors, Bollywood Actress,Bollywood Movie Stills, Bollywood Movie Wallpapers, Evergreen Hindi Songs, Evergreen Love Songs');
        }
        if(ind=='tollywood'){
          this.metaService.setTitle( 'Telugu Movie News | Trailers | Reviews | Songs | Collections ');
          this.metaService.setTag('description','Latest Telugu Movie News, Telugu Movie Reviews, Rating, Trailers, Box Office Collections, Interviews, Telugu Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Evergreen Telugu Songs, Love Songs, Tollywood Cinema Actors and much more only on flikster');
          this.metaService.setTag('keywords',' Telugu Movie News, Telugu Movie Reviews, Telugu Songs, Telugu Cinema News, Telugu Video Songs, Telugu Movie Songs, Tollywood News,Telugu Trailers, Telugu Actors, Telugu Actress, Telugu Movie Stills, Telugu Movie Wallpapers, Evergreen Telugu Songs, Evergreen Love Songs');
          this.metaService.setTag('og:description','Latest Telugu Movie News, Telugu Movie Reviews, Rating, Trailers, Box Office Collections, Interviews, Telugu Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Evergreen Telugu Songs, Love Songs, Tollywood Cinema Actors and much more only on flikster');
          this.metaService.setTag('og:keywords',' Telugu Movie News, Telugu Movie Reviews, Telugu Songs, Telugu Cinema News, Telugu Video Songs, Telugu Movie Songs, Tollywood News,Telugu Trailers, Telugu Actors, Telugu Actress, Telugu Movie Stills, Telugu Movie Wallpapers, Evergreen Telugu Songs, Evergreen Love Songs');
          this.metaService.setTag('og:title', 'Telugu Movie News | Telugu Movie Reviews | Telugu Movie Songs | Telugu Movies Collections | Telugu Movie Trailers');
         
        }
        if(ind=='kollywood'){
          this.metaService.setTitle('Tamil Movie News | Trailers | Reviews | Songs | Collections ');
          this.metaService.setTag('description','Latest Tamil Movie News, Tamil Movie Reviews, Rating, Box Office Collections, Interviews, Tamil Songs, Gallery, Trailers, Cast & Crew, Posters, Stills, Wallpapers, Tamil Cinema Actors, Kollywood Actress, Evergreen Tamil Songs, Love Songs and much more only flikster');
          this.metaService.setTag('keywords','Tamil Movie News, Tamil Songs, Tamil Cinema News, Tamil Trailers, Tamil Actors, Tamil Actress, Tamil Movie Stills, Tamil Movie Wallpapers,Evergreen Tamil Songs, Evergreen Love Songs');
        
          this.metaService.setTag('og:title', 'Tamil Movie News | Tamil Movie Reviews | Tamil Movie Songs | Tamil Movies Collections | Tamil Movie Trailers');
          this.metaService.setTag('og:description','Latest Tamil Movie News, Tamil Movie Reviews, Rating, Box Office Collections, Interviews, Tamil Songs, Gallery, Trailers, Cast & Crew, Posters, Stills, Wallpapers, Tamil Cinema Actors, Kollywood Actress, Evergreen Tamil Songs, Love Songs and much more only flikster');
          this.metaService.setTag('og:keywords','Tamil Movie News, Tamil Songs, Tamil Cinema News, Tamil Trailers, Tamil Actors, Tamil Actress, Tamil Movie Stills, Tamil Movie Wallpapers,Evergreen Tamil Songs, Evergreen Love Songs');
          }
        if(ind=='sandalwood'){
          this.metaService.setTitle( 'Kannada Movie News | Trailers | Reviews | Songs | Collections');
          this.metaService.setTag('description',' Latest Kannada Movie News, Kannada Movie Reviews, Rating, Kannada Movie Songs, Gallery, Cast & Crew, Trailers, Stills, Wallpapers, Posters, Kannada Actress, Sandalwood Actors, Evergreen Kannada Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('keywords','Kannada Movie Songs, Kannada Movies News, Kannada Movie Reviews, Kannada News, Kannada Actress, Kannada Actors, Evergreen Kannada Songs, Love Songs');
       
          this.metaService.setTag('og:title', 'Kannada Movie News | Kannada Movie Reviews | Kannada Movie Songs | Kannada Movie Trailers');
          this.metaService.setTag('og:description',' Latest Kannada Movie News, Kannada Movie Reviews, Rating, Kannada Movie Songs, Gallery, Cast & Crew, Trailers, Stills, Wallpapers, Posters, Kannada Actress, Sandalwood Actors, Evergreen Kannada Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Kannada Movie Songs, Kannada Movies News, Kannada Movie Reviews, Kannada News, Kannada Actress, Kannada Actors, Evergreen Kannada Songs, Love Songs');
          }
        if(ind=='mollywood'){
          this.metaService.setTitle(' Malayalam Movie News | Trailers | Reviews | Songs | Collections');
          this.metaService.setTag('description', 'Latest Malayalam Movie News, Malayalam Movie Reviews, Rating, Malayalam Movie Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Trailers, Malayalam Actress, Mollywood Actors, Evengreen Malayalam Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('keywords','Mollywood News, Malayalam Movie Reviews, Malayalam Movie Songs, Malayalam Movie News, Malayalam Actress, Malayalam Actors, Evengreen Malayalam Songs, Evergreen Love Songs');
       
          this.metaService.setTag('og:title', 'Malayalam Movie News | Malayalam Movie Reviews | Malayalam Movie Songs | Malayalam Movie Trailers');
          this.metaService.setTag('og:description','Latest Malayalam Movie News, Malayalam Movie Reviews, Rating, Malayalam Movie Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Trailers, Malayalam Actress, Mollywood Actors, Evengreen Malayalam Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Mollywood News, Malayalam Movie Reviews, Malayalam Movie Songs, Malayalam Movie News, Malayalam Actress, Malayalam Actors, Evengreen Malayalam Songs,Evergreen Love Songs');
 }
      }
      // this.metaService.setTitle(' '+urlBreakup[1]);
      if(urlBreakup[1] == "share-your-style") {
        // this.metaService.setTitle(' '+'share-your-style');
        this.isShareYourStyle = true;
        this.getContentForSYS();        
      }
      
     
      else if(urlBreakup[3]){
        let route = this.activatedRoute.params.subscribe(params => {
          console.log(params);
          this.contentTypePage=0;
          this.contentFilter=params.slug;
          this.getContentByType(params.slug,this.industry);
        });
      }
      else {
        this.contentFilter=null;
        if(urlBreakup[1] == "feed") {
          let route = this.activatedRoute.params.subscribe(params => {
            this.allContentData = [];
            this.getIndustryFeed();
          });
        } else if(urlBreakup[1] == "boutique") {
          if(localStorage.getItem("username")) {
            this.displayBoutique=null;
            this.getUserOrders();
            this.getWishlistItems();
          }
        } else if(urlBreakup[1] == "trending") {
            this.getTrendingProds(localStorage.getItem("Industry"));
        } else if(urlBreakup[1] == "bookmark" || urlBreakup[1] == "like") {
            this.likeOrBookmark = urlBreakup[1];
            this.userContentData = [];
            this.getUserContent(urlBreakup[1]);
        } else if(urlBreakup[1] == "orders") {
          if(localStorage.getItem("username")) {
            this.getUserOrdersForTracking();
          }
        }
      }
      // this.getFollowSuggesion();
      this.getBestSelling(localStorage.getItem("Industry"));
      this.getTopMovies();
      window.addEventListener("scroll", function(){
        amountscrolled()
      }, false)
      function amountscrolled(){
        var winheight= window.innerHeight || (document.documentElement || document.body).clientHeight
        var docheight = getDocHeight()
        var scrollTop = window.pageYOffset
        var trackLength = docheight - winheight
        var pctScrolled = Math.floor(scrollTop/trackLength * 100) // gets percentage scrolled (ie: 80 or NaN if tracklength == 0)
        
        if(pctScrolled>85){
          localStorage.setItem('scrollLimit','true');
        }
      }
      function getDocHeight() {
        var D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        )
    }
    }
    public ngOnDestroy(){
      this.contentFeed=null;
      this.alive = false;
    }
    
    ratingPopup () {
      this.displayRatingPopup = true;
    }

    submitRating () {
      this.displayRatingPopup = false;
    }

    submitingRatingPopup () {
      this.displayRatingPopup = false;
    }

    uploadPopup () {
      var login=localStorage.getItem('isLoggedIn');
      if(login=='true')
      this.displayUploadPopup = true;
      else
      this.displayLoginPopup = true; 
    }
  
    closeUploadPopup () {
      this.displayUploadPopup = false;
      if(this.isShareYourStyle) {
        this.getContentForSYS();        
      }
    }
    closeLoginPopup () {
      console.log("event emmitted");
      this.displayLoginPopup = false; 
    }
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

    getContentForSYS() {
     this.fashion.getContentForSYS().subscribe(
       res=> {
         if(res){
           this.sysContentData =res.Items;
          //  this.sysContentData=this.sysContentData.reverse();
           console.log("************All SYS Content Data***************");
           console.log(this.sysContentData);
          }
      },
      error => {
        console.log("error");
      }
    );
  }

  getTrendingProds(industry) {
      this.fashion.bestSelling(industry,1000).subscribe(
        res => {
          console.log(res);
          if(res){
            if(res.hits.hits[0])
            this.trendingProds = res.hits.hits;
          }
        },
        error => {
          console.log("Error");
        }
      );
  }

  checkLoadmore(){
    if(this.contentFeed=='feed'){
    
    var scroll= localStorage.getItem('scrollLimit');
    if(scroll=='true'){
      if(!this.contentFilter){
        // this.contentTypePage=1;
     this.loadMoreData('feed');
    
     
      }
     else{
      // this.pageNo = 1;
      this.loadMoreData('content');
      
     }
      localStorage.removeItem('scrollLimit');
    }
  }
  
  }
  
  getWishlistItems() {
    this.productService.getCartByUser(localStorage.getItem("username")).subscribe(
      res => {
        console.log(res);
        var wishListSlugArray = new Array();
        for(let i=0; i<res.Items.length; i++) {
          wishListSlugArray.push(res.Items[i].productDetails.productSlug);
          console.log(wishListSlugArray);
          if(i == res.Items.length-1) {
            console.log(wishListSlugArray);
            this.appendForBotique(wishListSlugArray,'cart');
          }
        }
      },
      error => {
        console.log("error getting cart items");
      }
    );
  }
  loadMoreData(type){
    if(type=='feed'){
    
    this.getIndustryFeed();
    }
    else{
      this.getContentByType(this.contentFilter,this.industry);

    }
  }

  appendForBotique(items,type) {
    console.log(items);
    console.log(type);
    if(!this.botiqueItemSlugs) {
      this.botiqueItemSlugs = [];
    }
    this.botiqueItemSlugs = this.botiqueItemSlugs.concat(items);
    console.log(this.botiqueItemSlugs);
    if(type == 'cart') {
      this.isCartItemSlugs = true;
    }
    if(type == 'orders') {
      this.isOrderItemSlugs = true;
    }
    if(this.isCartItemSlugs && this.isOrderItemSlugs) {
      this.removeDuplicateSlugs(this.botiqueItemSlugs);
    }
  }

  removeDuplicateSlugs(array) {
    let tempArray = new Array();
    for(let i=0; i<array.length; i++) {
      if(tempArray.indexOf(array[i]) == -1) {
        tempArray.push(array[i]);
      }
    }
    this.getProductsForBotique(tempArray);  
  }

  getUserOrders() {
    this.productService.getAllUserOrders(localStorage.getItem("username")).subscribe(
      res => {
        console.log(res);
        var wishListSlugArray = new Array();
        for(let i=0; i<res.hits.hits.length; i++) {
          for(let j=0; j<res.hits.hits[i]._source.product.length; j++) {
            wishListSlugArray.push(res.hits.hits[i]._source.product[j].productSlug);
            if(i == res.hits.hits.length-1 && j == res.hits.hits[i]._source.product.length-1)
              this.appendForBotique(wishListSlugArray,'orders');
          }
        }
      },
      error => {
        console.log("error getting orders");
      }
    );
  }

  getProductsForBotique(slugsArray) {
    console.log(slugsArray);
    let botiqueProdArray = new Array();
    for(let i=0; i<slugsArray.length; i++) {
        this.productService.getProductBySlug(slugsArray[i]).subscribe(
        res => {
          console.log("Product service");
          console.log(res);
            if(res) {
              if(res.hits.hits.length>0){
              botiqueProdArray.push(res.hits.hits[0]);
              if(i == slugsArray.length-1) {
                this.botiqueItems = botiqueProdArray;
                console.log(this.botiqueItems);
              }
            }
            }
        },
        error => {
          console.log("error getting product");
        }
      );
    }
    console.log(this.botiqueItems);
    if(this.botiqueItems.length<1){
      this.displayBoutique='No items in the Botique';
    }
  }
  // reverseItems(array) {
  //   let tempArray = new Array(array.length);
  //   for(let i=0; i<array.length; i++) {
  //     tempArray[array.length-i-1] = array[i];
  //   }
  //   return tempArray;
  // }
  getUserOrdersForTracking() {
    this.productService.getAllUserOrders(localStorage.getItem("username")).subscribe(
      res => {
        console.log(res);
        this.ordersList = res.hits.hits;
      },
      error => {
        console.log("error getting orders");
      }
    );
  }

    getFollowSuggesion(){
      this.user.getFollowSuggestions().subscribe(
        res => {
          console.log(res);
          if(res){
            this.followSuggesions =res;
          }
        },
        error => {
          console.log("Error in follow suggestions");
        }
      );
    }

    // getTopContent(type) {
    //   this.contentService.getTopContent(type).subscribe(
    //     res => {
    //       console.log(res);
    //       if(res){
    //           this.topTrailers = res;
    //       }
    //     },
    //     error => {
    //       console.log("Error in getting top content");
    //     }

    //   );
    // }

    getBestSelling(industry){
      this.fashion.bestSelling(industry,1).subscribe(
        res => {
          console.log(res);
          if(res){
            if(res.hits.hits[0])
            this.bestSelling = res.hits.hits[0]._source.products;
          }
        },
        error => {
          console.log("Error in Best Selling");
        }
      );
    }

    getTopMovies() {
      let industry=localStorage.getItem('Industry')
      this.movieService.getTopMovies(industry).subscribe(  
       res => {
         console.log("TOP MOVIES");
         console.log(res);
         if(res) {
           var tempData = [];
           for(let i=0; i<res.hits.hits.length; i++) {
            if(res.hits.hits[i]._source) {
              if(res.hits.hits[i]._source.slug) {
                if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                  tempData.push(res.hits.hits[i]);
                }
              }
            }
           }
          this.topMovies = tempData;
         
           console.log(this.topMovies); 
         }

       },
       error => {
         this.topMovies = this.topMoviesMock();
         console.log("error getting top-movies");
       }
     );
   }


   

   getAllContent(){
     this.contentService.getAllContent().subscribe(
       res=>
       {
         if(res){
           console.log(res.Items);
           var tempData = [];
           for(let i=0; i<res.hits.hits.length; i++) {
            if(res.hits.hits[i]._source) {
              if(res.hits.hits[i]._source.slug) {
                if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                  tempData.push(res.hits.hits[i]);
                }
              }
            }
           }
           this.allContentData = tempData;
           console.log("************All Content Data***************");
           console.log(this.allContentData);

         }
     },
     error => {
      console.log("error getting all content data");
    }
    );
   }

   getIndustryFeed() {
    if(this.pageNo>0)
    this.pageNo=this.pageNo+1;
    else
    this.pageNo=1;
     let industry = localStorage.getItem("Industry").toLowerCase();
     this.contentService.getIndustryFeed(industry,this.pageNo).subscribe(
       res=>
       {
         if(res){
          this.allContentDataCount = res.hits.total;
           for(let i=0; i<res.hits.hits.length; i++) {
            if(res.hits.hits[i]._source) {
              if(res.hits.hits[i]._source.slug) {
                if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                  this.allContentData.push(res.hits.hits[i]);
                }
              }
            }
           }
          //  this.allContentData = res.hits.hits;
           console.log("************Industry Feed***************");
           console.log(this.allContentData);
         }
     },
     error => {
      console.log("error getting all content data");
    }
    );
   }

   getUserContent(feedType) {
    if(this.pageNo>0)
    this.pageNo=this.pageNo+1;
    else
    this.pageNo=1;
     this.displayContent=null;
     var user = {
       userId : localStorage.getItem("username"),
       type : feedType
     };
     this.contentService.getUserFeed(user,this.pageNo).subscribe(
       res=>
       {
         if(res){
           console.log(res);
            this.userContentDataCount = res.hits.total;
           for(let i=0; i<res.hits.hits.length; i++) {
            if(res.hits.hits[i]._source && res.hits.hits[i]._source.contentType !='share-your-style') {
              
              this.userContentData.push(res.hits.hits[i]);
            }
           }
           console.log("************User Content Data***************");
           console.log(this.userContentData);
           if(this.userContentData.length<1){
             if(feedType=='like'){
              this.displayContent="No liked posts to display";
             }
             else
             this.displayContent="No  post to has been bookmarked";
           }

         }
     },
     error => {
      console.log("error getting all content data");
    }
    );
   }
   getMoreContent(){
    this.getContentByType(this.contentFilter,this.industry);
   }

   getContentByType(slug,industry){
     if(this.slugContent!=slug){
       this.allContentDataByType=[];
       this.slugContent=slug;
     }
     if(this.contentTypePage>0){
      this.contentTypePage=this.contentTypePage+1;
     }
     else
     this.contentTypePage=1;
    this.contentService.getContentByTypeIndustry(slug,industry,this.contentTypePage).subscribe(
      res=>
      {
        if(res){
          
          this.allContentDataCoun = res.hits.total;
          console.log(this.allContentDataCoun);
          console.log("************All Content Data***************");
          let tempData = [];
           for(let i=0; i<res.hits.hits.length; i++) {
            if(res.hits.hits[i]._source) {
              if(res.hits.hits[i]._source.slug) {
                if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                  this.allContentDataByType.push(res.hits.hits[i]);
                }
              }
            }
           }
          //  this.allContentDataByType.push(tempData);
          console.log(this.allContentDataByType);

        }
    },
    error => {
     console.log("error getting all content data");
   }
   );
  }

  topMoviesMock() {
    return [
      {
        'movieSlug':'fidaa',
        'movieTitle':'Fidaa',
        'profilePic':'https://images-na.ssl-images-amazon.com/images/M/MV5BOTFiZmFkZTAtNDMzOS00MmRkLWI5MWYtYzQ4YzIxMjQzZWFkL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMjczODk3NjA@._V1_SY1000_CR0,0,654,1000_AL_.jpg',
        'movieId':'12321',
        'averageRating':'7'
        
      },
      {
        'movieSlug':'dj',
        'movieTitle':'DJ',
        'profilePic':'http://media2.intoday.in/indiatoday/images/stories/dj-poster-story_647_021817033906.jpg',
        'movieId':'2134',
        'averageRating':'8'
      },
      {
        'movieSlug':'Bahubali-2',
        'movieTitle':'Bahubali 2',
        'profilePic':'http://st1.bollywoodlife.com/wp-content/uploads/2017/01/baahubali-2-new-posterhindi.jpg',
        'movieId':'2934',
        'averageRating':'9'
      },
      {
        'movieSlug':'ninnu-kori',
        'movieTitle':'Ninnu Kori',
        'profilePic':'https://igmedia.blob.core.windows.net/igmedia/telugu/news/ninnukori100617_c.jpg',
        'movieId':'23934',
        'averageRating':'10'
      },

    ];
  }

  }

  
  