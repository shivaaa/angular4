import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  import { ActivatedRoute, Router } from "@angular/router";
  
  @Component({
    
    selector: 'card-product', 
    providers: [ ],
    styleUrls: [ './card-product.component.css' ],
    templateUrl: './card-product.component.html'
  })
  export class CardProductComponent implements OnInit {
    assnData: any;
    isMiniCard: boolean;
    @Input() content: any;
    
    @Input() miniCard : boolean;
    public localState = { value: '' };
    public cardId;
  
    constructor(
      // public appState: AppState,
      public router: Router,
    ) {}
  
    public ngOnInit() {
      console.log('hello `card-product` component');
      if(this.content){
        console.log(this.content);
        if(this.content.celeb) {
          if(this.content.celeb[0])
          this.assnData = this.content.celeb[0];
        }
        if(this.content.movie) {
          if(this.content.movie[0])
          this.assnData = this.content.movie[0];
        }
        this.cardId = this.content.slug;
        console.log(this.content);
        console.log(this.assnData);
      }
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      console.log(this.content);
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

    gotoProduct(){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['product/' + this.content.slug]));
    }

  }