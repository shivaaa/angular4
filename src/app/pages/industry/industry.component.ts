import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
// import { AppState } from '../../app.service';
import { Router } from '@angular/router';
import { MovieService } from "../../models/movie/movie.service";
import { FashionService } from "../../models/fashion/fashion.service";
import { ContentService } from "../../models/content/content.service";
import { CarouselService } from '../../models/carousel/carousel.service';
import { LikeService } from '../../models/like/like.service';
import { MetaService } from '@ngx-meta/core';

  @Component({
    
    selector: 'industry', 
    providers: [ ],
    styleUrls: [ './industry.component.css' ],
    templateUrl: './industry.component.html'
  })
  export class IndustryComponent implements OnInit {
    womenArray=[];
    menArray=[];
    topInterviewVideos=[];
    topTeasersVideos=[];
    statusArray= [];
    topMoviesData: any;
    shopbyVideosData: any;
    public dialougeArray: any;
    public quoteArray: any;
    public tweetArray: any;

    womencata: any;
    public mencatg: any;
    public miniCard :boolean = true;
    public localState = { value: '' };
    public industry;
    public stealThatStyle = "stealThatStyle";
    public news = "news";
    public videos = "videos";
    public shopbyvideos='shopbyvideos';  
    public gallery = "gallery";
    public topMovies = [];
    public stealThatStyleData : any;
    public topContentNews : any;
    public topContentGallery : any;
    public topContentVideos=[];
    public caurosel:any;
    public rootRoute;
    public  subRoute;
    public widgets;
    constructor(
      // public appState: AppState,
      public router : Router,
      public movieService : MovieService,
      public fashionService : FashionService,
      public contentService : ContentService,
      public carouselService: CarouselService,
      public likeService: LikeService,
      private metaService: MetaService,
      
    ) {}
  
    public ngOnInit() {
      window.scrollTo(0,0);
      console.log('hello `industry` component');

      var urlBreakup = this.router.url.split("/");
      console.log(urlBreakup);
      this.industry = urlBreakup[2];
      if(this.industry){
        
        // this.metaService.setTitle(' '+this.industry);
        if(this.industry=='bollywood'){
          this.metaService.setTitle( 'Bollywood News | Bollywood Movie Reviews | Bollywood Movie Songs | Bollywood Movies Collections | Bollywood Trailers');
          this.metaService.setTag('description','Latest Bollywood Movie News, Bollywood Movie Reviews, Rating, Trailers, Stills, Posters, Box Office Collctions, Interviews, Hindi Songs, Gallery, Bollywood Songs, Cast & Crew, Evergreen Hindi Songs, Dialogues, Love Songs and much more only on flikster');
          this.metaService.setTag('keywords','Bollywood News, Bollywood Movie Reviews, Bollywood Songs, Hindi Songs, Bollywood Trailers, Hindi Video Songs, Bollywood Actors, Bollywood Actress,Bollywood Movie Stills, Bollywood Movie Wallpapers, Evergreen Hindi Songs, Evergreen Love Songs');
          this.metaService.setTag('og:title', 'Bollywood News | Bollywood Movie Reviews | Bollywood Movie Songs | Bollywood Movies Collections | Bollywood Trailers');
          this.metaService.setTag('og:description','Latest Bollywood Movie News, Bollywood Movie Reviews, Rating, Trailers, Stills, Posters, Box Office Collctions, Interviews, Hindi Songs, Gallery, Bollywood Songs, Cast & Crew, Evergreen Hindi Songs, Dialogues, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Bollywood News, Bollywood Movie Reviews, Bollywood Songs, Hindi Songs, Bollywood Trailers, Hindi Video Songs, Bollywood Actors, Bollywood Actress,Bollywood Movie Stills, Bollywood Movie Wallpapers, Evergreen Hindi Songs, Evergreen Love Songs');
        }
        if(this.industry=='tollywood'){
          this.metaService.setTitle( 'Telugu Movie News | Telugu Movie Reviews | Telugu Movie Songs | Telugu Movies Collections | Telugu Movie Trailers');
          this.metaService.setTag('description','Latest Telugu Movie News, Telugu Movie Reviews, Rating, Trailers, Box Office Collections, Interviews, Telugu Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Evergreen Telugu Songs, Love Songs, Tollywood Cinema Actors and much more only on flikster');
          this.metaService.setTag('keywords',' Telugu Movie News, Telugu Movie Reviews, Telugu Songs, Telugu Cinema News, Telugu Video Songs, Telugu Movie Songs, Tollywood News,Telugu Trailers, Telugu Actors, Telugu Actress, Telugu Movie Stills, Telugu Movie Wallpapers, Evergreen Telugu Songs, Evergreen Love Songs');
          this.metaService.setTag('og:description','Latest Telugu Movie News, Telugu Movie Reviews, Rating, Trailers, Box Office Collections, Interviews, Telugu Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Evergreen Telugu Songs, Love Songs, Tollywood Cinema Actors and much more only on flikster');
          this.metaService.setTag('og:keywords',' Telugu Movie News, Telugu Movie Reviews, Telugu Songs, Telugu Cinema News, Telugu Video Songs, Telugu Movie Songs, Tollywood News,Telugu Trailers, Telugu Actors, Telugu Actress, Telugu Movie Stills, Telugu Movie Wallpapers, Evergreen Telugu Songs, Evergreen Love Songs');
          this.metaService.setTag('og:title', 'Telugu Movie News | Telugu Movie Reviews | Telugu Movie Songs | Telugu Movies Collections | Telugu Movie Trailers');
         
        }
        if(this.industry=='kollywood'){
          this.metaService.setTitle('Tamil Movie News | Tamil Movie Reviews | Tamil Movie Songs | Tamil Movies Collections | Tamil Movie Trailers');
          this.metaService.setTag('description','Latest Tamil Movie News, Tamil Movie Reviews, Rating, Box Office Collections, Interviews, Tamil Songs, Gallery, Trailers, Cast & Crew, Posters, Stills, Wallpapers, Tamil Cinema Actors, Kollywood Actress, Evergreen Tamil Songs, Love Songs and much more only flikster');
          this.metaService.setTag('keywords','Tamil Movie News, Tamil Songs, Tamil Cinema News, Tamil Trailers, Tamil Actors, Tamil Actress, Tamil Movie Stills, Tamil Movie Wallpapers,Evergreen Tamil Songs, Evergreen Love Songs');
        
          this.metaService.setTag('og:title', 'Tamil Movie News | Tamil Movie Reviews | Tamil Movie Songs | Tamil Movies Collections | Tamil Movie Trailers');
          this.metaService.setTag('og:description','Latest Tamil Movie News, Tamil Movie Reviews, Rating, Box Office Collections, Interviews, Tamil Songs, Gallery, Trailers, Cast & Crew, Posters, Stills, Wallpapers, Tamil Cinema Actors, Kollywood Actress, Evergreen Tamil Songs, Love Songs and much more only flikster');
          this.metaService.setTag('og:keywords','Tamil Movie News, Tamil Songs, Tamil Cinema News, Tamil Trailers, Tamil Actors, Tamil Actress, Tamil Movie Stills, Tamil Movie Wallpapers,Evergreen Tamil Songs, Evergreen Love Songs');
          }
        if(this.industry=='sandalwood'){
          this.metaService.setTitle( 'Kannada Movie News | Kannada Movie Reviews | Kannada Movie Songs | Kannada Movie Trailers');
          this.metaService.setTag('description',' Latest Kannada Movie News, Kannada Movie Reviews, Rating, Kannada Movie Songs, Gallery, Cast & Crew, Trailers, Stills, Wallpapers, Posters, Kannada Actress, Sandalwood Actors, Evergreen Kannada Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('keywords','Kannada Movie Songs, Kannada Movies News, Kannada Movie Reviews, Kannada News, Kannada Actress, Kannada Actors, Evergreen Kannada Songs, Love Songs');
       
          this.metaService.setTag('og:title', 'Kannada Movie News | Kannada Movie Reviews | Kannada Movie Songs | Kannada Movie Trailers');
          this.metaService.setTag('og:description',' Latest Kannada Movie News, Kannada Movie Reviews, Rating, Kannada Movie Songs, Gallery, Cast & Crew, Trailers, Stills, Wallpapers, Posters, Kannada Actress, Sandalwood Actors, Evergreen Kannada Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Kannada Movie Songs, Kannada Movies News, Kannada Movie Reviews, Kannada News, Kannada Actress, Kannada Actors, Evergreen Kannada Songs, Love Songs');
          }
        if(this.industry=='mollywood'){
          this.metaService.setTitle('Malayalam Movie News | Malayalam Movie Reviews | Malayalam Movie Songs | Malayalam Movie Trailers');
          this.metaService.setTag('description', 'Latest Malayalam Movie News, Malayalam Movie Reviews, Rating, Malayalam Movie Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Trailers, Malayalam Actress, Mollywood Actors, Evengreen Malayalam Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('keywords','Mollywood News, Malayalam Movie Reviews, Malayalam Movie Songs, Malayalam Movie News, Malayalam Actress, Malayalam Actors, Evengreen Malayalam Songs, Evergreen Love Songs');
       
          this.metaService.setTag('og:title', 'Malayalam Movie News | Malayalam Movie Reviews | Malayalam Movie Songs | Malayalam Movie Trailers');
          this.metaService.setTag('og:description','Latest Malayalam Movie News, Malayalam Movie Reviews, Rating, Malayalam Movie Songs, Gallery, Cast & Crew, Stills, Posters, Wallpapers, Trailers, Malayalam Actress, Mollywood Actors, Evengreen Malayalam Songs, Love Songs and much more only on flikster');
          this.metaService.setTag('og:keywords','Mollywood News, Malayalam Movie Reviews, Malayalam Movie Songs, Malayalam Movie News, Malayalam Actress, Malayalam Actors, Evengreen Malayalam Songs,Evergreen Love Songs');
 }
      }
      localStorage.setItem("Industry",this.industry)
      localStorage.removeItem('subCatagori');
      if(localStorage.getItem("displayLoginInIndustryPage") == "true" && localStorage.getItem("isLoggedIn") != "true") {
        this.goToLogin();
        localStorage.removeItem("displayLoginInIndustryPage");
      }
        

      this.getTopMovies(this.industry);
      this.getTopContent("news",this.industry);
      this.getTopContent("teasers-promos",this.industry);
      this.getTopContent("video-song",this.industry);
      this.getTopContent("gallery",this.industry);
      this.getTopContent("interview",this.industry);
      this.getshopbyvideos(this.industry);
      this.getStealThatStyle(this.industry);
      this.getTweets(this.industry);
      localStorage.setItem("mainRoute","Industry");
      localStorage.setItem("rootRoute",this.industry);
      this.getAllCatogeries();
      this.getCategoryWidgets(this.industry);
      
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    getshopbyvideos(slug){
      this.fashionService.getshopbyvideosbyIndustry(slug).subscribe(
        res => {
          console.log("THIS IS shopbyvideos DATA");
          console.log(res);
          this.shopbyVideosData=res.hits.hits;
          console.log( this.shopbyVideosData);

          // if(res){
            
          //     this.pagecontent = res;
          // }
        },
        error => {
          console.log("Error in getting top content");
        }
      );
    }

    getCategoryWidgets(industry) {
      this.fashionService.getCategoryWidgets(industry).subscribe(
        res => {
          if(res) {
            console.log(res);
            this.widgets = res.hits.hits;
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log("***********widgets**********")
           console.log(this.widgets);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }

    seemore(first,second,third){
      localStorage.removeItem('category');
      localStorage.removeItem('subPage');
      if(third=='womenware')
      localStorage.setItem("rootRoute",'womenfashion');
      if(third=='menware')
        localStorage.setItem("rootRoute",'menfashion');
      this.router.navigate([first,second,third]);  
    }
    goToClothing() {
      localStorage.removeItem('category');
      localStorage.setItem("subPage","Clothing");
      localStorage.setItem("rootRoute","womenfashion");
     
        localStorage.setItem("items","15");
        this.rootRoute = localStorage.getItem("rootRoute");
        this.subRoute = localStorage.getItem("subRoute");
      
      // if (this.rootRoute=="menfashion"){
      //   localStorage.setItem("items","7");
      // }
      this.router.navigate([this.rootRoute,this.subRoute]);  
    }
    goToAllContent(content,type){
      localStorage.removeItem('subPage');
      localStorage.removeItem('category');
      var pageRedirect=localStorage.getItem('rootRoute');
      this.router.navigate([content,type,pageRedirect]);  
    }
    getTweets(industry){
      let body ={
        'industry':industry,
      };
      this.contentService.industryTweets(body).subscribe(
        res => {
            console.log(res);
             this.tweetArray=res.tweet.hits.hits;
             this.quoteArray=res.quote.hits.hits;
             this.dialougeArray=res.dialouge.hits.hits; 
             console.log(this.tweetArray);
             console.log(this.quoteArray);
             console.log(this.dialougeArray);
            // for(let i=0;i<res.tweet.hits.hits.length;i++){
            //   this.qtd.push(res.tweet.hits.hits[i]);
            // }
            
            // this.success=res;
            // this.submitted=true;
            // this.gethighestbid(this.auctionslug);
           },
          error => {
            console.log("Error in posting orders");
          }
    );
  
  
  }

    // getTopMovies() {
    //    this.movieService.getTopMovies("tollywood").subscribe(
    //     res => {
    //       console.log(res);
    //       if(res) {
    //         this.topMovies=res;            
           
    //       }
    //     },
    //     error => {
    //       console.log("error getting top-movies");
    //     }
    //   );
    // }
    getTopMovies(industry) {
      // let industry=localStorage.getItem('Industry')
      this.movieService.getTopMovies(industry).subscribe(  
       res => {
         console.log("TOP MOVIES");
         console.log(res);
         if(res) {
          this.topMoviesData = res.hits.hits;
          for(let i=0;i<this.topMoviesData.length;i++){
            this.topMovies.push(this.topMoviesData[i]._source);
          }
         
           console.log(this.topMovies); 
           this.statusArray=[];
           for(let i=0;i<this.topMovies.length;i++){
             console.log(this.topMovies[i].id);
           this.isFollowing(this.topMovies[i].id,i);

           }
           console.log(this.statusArray);
         }

       },
       error => {
         console.log("error getting top-movies");
       }
     );
   }

   isFollowing(id,i) {
    let isFollowBody = {
      userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
      entityId : id,
      type : "follow"
    };
      this.likeService.isLiked(isFollowBody).subscribe(
          res => {
              console.log(res);
            if(res.data) {
              this.statusArray[i]=res;
              // this.statusArray.push(res.data);
              console.log(res.totalCount);
              // return res.totalCount;
              // return (res.data);
              // if(res.data.Count == 0) {
              //   this.isFollow = false;
              // } else {
              //   this.isFollow = true;
              // }
              // this.numOfFollowers = res.totalCount;
            }
            },
            error => {
              console.log("Error");
            }
      );
    }

    getTopContent(type,industry) {
      this.contentService.getTopContent(type,industry).subscribe(
        res => {
          console.log(res.hits.hits);
          if(res){
            if(type == "news") {
              this.topContentNews = res.hits.hits;  
            } else if(type == "gallery") {
              this.topContentGallery = res.hits.hits;
            } else if(type == "video-song") {
              for(let i=0;i<res.hits.hits.length;i++){
              this.topContentVideos.push(res.hits.hits[i]._source);
              }
            }
            else if(type=="interview"){
              for(let i=0;i<res.hits.hits.length;i++){
              this.topInterviewVideos.push(res.hits.hits[i]._source);
            }
            }
            else if(type=="teasers-promos"){
              for(let i=0;i<res.hits.hits.length;i++){
               this.topTeasersVideos.push(res.hits.hits[i]._source);
              }
            }
            
          }
        },
        error => {
          console.log("Error in getting top content");
        }

      );
    }

    getStealThatStyle(industry){
      this.fashionService.stealThatStyle(industry).subscribe(
        res => {
          console.log(res);
          if(res){
            this.stealThatStyleData = res.hits.hits;
          }
        },
        error => {
          console.log("Error in stealThatStyle");
        }

      );
    }
    getAllCatogeries(){
      this.carouselService.getCatogeries().subscribe(
        res=>{
          console.log(res);
          if(res){
            this.caurosel = res.Items;
            console.log("**********Collections********");
            console.log(this.caurosel[0].menFashion);
            this.mencatg=this.caurosel[0].menFashion;
            for(let i=0;i<this.mencatg.length;i++){
              this.menArray.push(this.mencatg[i][0])
            }
            
            this.womencata=this.caurosel[0].womenFashion;
            for(let i=0;i<this.womencata.length;i++){
              this.womenArray.push(this.womencata[i][0])
            }
            console.log("*********men********");
            console.log(this.mencatg);
            
          }

      },
      error => {
        console.log("Error in c togeries");
      }
    );
    }
    

  public displayLoginPopup : boolean = false;
  goToLogin() {
    this.displayLoginPopup = true;
  }

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }
}
  