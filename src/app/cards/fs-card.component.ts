import { Component, OnInit, Input} from '@angular/core';

// import { AppState } from '../app.service';
import { ContentService } from '../models/content/content.service';
  import { ActivatedRoute, Router } from "@angular/router";

@Component({
  
  selector: 'fs-card', 
  providers: [ ],
  styleUrls: [ './fs-card.component.css' ],
  templateUrl: './fs-card.component.html'
})
export class FsCardComponent implements OnInit {
  public localState = { value: '' };
  @Input() contentType : any;
  @Input() contentData : any;
  @Input() relatedFlick: any;
  @Input() miniCard: any;
  // @Input() pageType : any;
  // @Input() pageType1 : any;
  // @Input() pageType2 : any;
  // @Input() pageType3 : any;
  // @Input() pageType5 : any;
  // @Input() contents : any;
  public slug = this.contentType;
  private id: string = '-twi5MBq1TQ';
  private height: number = 460;
  private width: any = '100%';
  private contentId;
  public contentsType:any;
  public gallerySlug:any; 
    
  constructor(
    // public appState: AppState,
    public contentService: ContentService,
      public router: Router,
  ) {}

  public ngOnInit() {
    console.log('hello `fs-card` component');
    console.log(this.contentData);
    console.log(this.contentType);
    var urlBreakup = this.router.url.split("/");
    this.contentsType=urlBreakup[1];
    this.gallerySlug=urlBreakup[2];
    if(urlBreakup[1] == "bookmark" || urlBreakup[1] == "like") {
      this.contentId = this.contentData.entityId;
      this.getContent();
    }
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
  checkFunction(content,page){
  if(content=='gallery' && page=='content' && this.contentData.slug==this.gallerySlug)
  return true;
  else
  return false;

  }

    getContent(){
      this.contentService.getContentById(this.contentId).subscribe(
        res => {
          
          if(res){
            console.log(res);
            this.contentData = res;
            this.contentType = res.contentType;
          }
        },
        error => {
          console.log("Error in follow suggestions");
        }
      );
    }

}
