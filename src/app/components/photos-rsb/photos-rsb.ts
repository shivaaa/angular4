import {
    Component,
    OnInit,OnChanges,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
import { ContentService } from '../../models/content/content.service';
import { Router } from '@angular/router';
  
  @Component({
    
    selector: 'photos-rsb', 
    providers: [ ],
    styleUrls: [ './photos-rsb.css' ],
    templateUrl: './photos-rsb.html'
  })
  export class PhotosRsbComponent implements OnInit, OnChanges {
    contentData: any;
    public localState = { value: '' };
    @Input() galleryPhotos : any;
    @Input() celebrityCollection:any;
    @Input() gallerySlug:any;
    
    public galleryOfPhotos: any;
  
    constructor(
      // public appState: AppState,
      public contentService: ContentService,
      public router : Router
    ) {}
  
    public ngOnInit() {
      console.log('hello `photos-rsb` component');
      
      console.log(this.galleryPhotos);
      
      
    }

    public ngOnChanges(changes) {
      console.log(changes);
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    gotocontent(){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.id]));
    }

    
  }