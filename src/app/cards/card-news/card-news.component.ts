import {
    Component,
    OnInit, Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
import { ContentService } from '../../models/content/content.service';
  
import { ActivatedRoute, Router } from "@angular/router";
  @Component({
    
    selector: 'card-news', 
    providers: [ ],
    styleUrls: [ './card-news.component.css' ],
    templateUrl: './card-news.component.html'
  })
  export class NewsComponent implements OnInit {
    image:any;
    shareProfilePic: any;
    shareSlug: any;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    public page: string;

    @Input() miniCard : boolean;
    @Input() content : any;
    @Input() type : any;
    public localState = { value: '' };
    public isMiniCard : boolean = false;
    public contentData : any;
    public assnData : any;
    public cardNews;
    public cardId;
    public pageType:any; 
    public pageType1:any;
    public today: number = Date.now();
    public cardSlug:any
  
    constructor(
      // public appState: AppState,
      public contentService : ContentService,
      public router: Router,
    ) {}
  
    public ngOnInit() {
      // this.getNewsCard();
      var urlBreakup = this.router.url.split('/');
      console.log(urlBreakup);
      var slug = urlBreakup[2];
      this.cardSlug=urlBreakup[2];
      this.page = urlBreakup[1];
      console.log('hello `news` component');
      console.log("FORM NEWS COMPONENT");
      this.contentData = this.content;
      this.createdAt=this.content.createdAt;
      this.image = this.content.profilePic;
      console.log(this.contentData);
      if(this.contentData.celeb) {
        if(this.contentData.celeb[0])
        this.assnData=this.contentData.celeb[0];
      }
      if(this.contentData.movie) {
        if(this.contentData.movie[0])
        this.assnData=this.contentData.movie[0];
      }
     // this.assnData = this.cardNews.title;
     console.log("qwertyuioplkjhgfdsazxcvbnm");
     console.log(this.assnData);
     console.log("qwertyuioplkjhgfdsazxcvbnm");
     console.log(this.isMiniCard);
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
      console.log(this.shareProfilePic);

      console.log(this.cardId);
      console.log(this.isMiniCard);
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='sec';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);  
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

    getNewsCard(){
      this.contentService.getContentBySlug("news").subscribe(
          res => {
              console.log(res);
              if(res){
                this.cardNews = res;
                // this.assnData = this.cardNews.title;
                console.log("*********CARD NEWS**************");
                console.log(this.cardNews);
                this.cardId = this.cardNews.id;
              }
            },
            error => {
              console.log("Error in Card Gallery");
            }
      );
  }

    gotocontent(){
      if(this.cardSlug !=this.contentData.slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }
}
  