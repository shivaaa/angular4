import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Card } from './card';

@Injectable()
export class CardService {
   // public baseUrl;

  constructor(
  	    private http: Http,
    ) { 

  }

  getGalleryCardBySlug(){
    //let brandUrl = this.baseUrl+'brand/'+slug;
    let brandUrl = "http://apiv3.flikster.com/v3/gallery-ms/getGalleryById/11ecc79f-0f95-44c1-909a-30774b07d2a9";
    return this.http.get(brandUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  
}