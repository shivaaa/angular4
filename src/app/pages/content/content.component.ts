import {
  Component,
  OnInit
} from '@angular/core';


// import { AppState } from '../../app.service';
import { ActivatedRoute, Router } from "@angular/router";
import { ContentService } from '../../models/content/content.service';
import { MetaService } from '@ngx-meta/core';

// import { SeoService } from '../../models/seo/seo.service';

@Component({

  selector: 'contentpage',
  providers: [],
  styleUrls: ['./content.component.css'],
  templateUrl: './content.component.html'
})
export class ContentPageComponent implements OnInit {
  cardIdentity: any;
  rootRoute: string;
  mainRoute: string;
  rootPage: string;
  page: string;
  relcontent = [];
  public localState = { value: '' };
  public contentSlug: string;
  public contentType: string;
  public relatedFlick: string;
  public contents: string;
  public content: any;
  public pageType = "contentpage";
  public pageType2 = "releatedfliks";
  public pageType3 = "releatedfliks1";
  public content1 = "gallery";
  public gallerycontent = "gallery";
  public relatedFlicks = "relatedFlick"
  public pageType5 = "relatedFlicks";
  public miniCard: boolean = true;

  constructor(
    // public appState: AppState,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public contentService: ContentService,
    // public seoService: SeoService,
    private metaService: MetaService
  ) { }

  public ngOnInit() {
    window.scrollTo(0, 0);
    // console.log('hello `contentpage` component');
    this.rootPage = localStorage.getItem('rootRoute');
    this.mainRoute = localStorage.getItem('mainRoute');
    this.rootRoute = localStorage.getItem('rootRoute');
    var urlBreakup = this.router.url.split("/");
    this.page = urlBreakup[1];
    this.contentSlug = urlBreakup[2];

    console.log(this.contentSlug);
    if (this.page == 'content') {
      this.relcontent = [];
      let route = this.activatedRoute.params.subscribe(params => {
        console.log(params);
        this.getcontent(params.slug);
      });
    }

    // this.getcontent(this.contentSlug);

    // if(this.contentType)

  }

  getcontent(contentSlug) {
    this.contentService.getContentBySlug(contentSlug).subscribe(
      res => {
        if (res) {
          console.log(res);
          if (res.hits.hits[0])
            this.content = res.hits.hits[0]._source;
          if (this.content)
            this.contentType = this.content.contentType;
          this.cardIdentity = this.content.id;

          let name;
          if (this.content.name)
            name = this.content.name;
          if (this.content.title)
            name = this.content.title;
          // 
          this.metaService.setTitle(' ' + name);
          this.metaService.setTag('og:title', this.content.seo_title ? this.content.seo_title : name);
          this.metaService.setTag('og:description', this.content.seo_description ? this.content.seo_description : name);
          this.metaService.setTag('og:keywords', this.content.seo_keywords ? this.content.seo_keywords.toString() : name);
          this.metaService.setTag('og:image', this.content.profilePic);
          this.metaService.setTag('fb:app_id', '433236906796403');
          this.metaService.setTag('og:type', 'article');
          this.metaService.setTag('og:url', 'http://flikster.com');
          // 




          // 
          // this.seoService.setOgTitle(this.content.seo_title ? this.content.seo_title : name);
          // this.seoService.setOgImage(this.content.profilePic);
          // 


          console.log(this.content.id);
          console.log(this.content.tags);
          if (this.content.tags)
            for (let i = 0; i < this.content.tags.length; i++) {
              this.getRelatedFliks(this.contentType, this.content.tags[i]);
            }
        }
        console.log(this.relcontent);
      },

      error => {
        console.log("error getting content");
      }
    );

  }


  getRelatedFliks(contentType, tag) {
    this.contentService.getRelatedFliks(contentType, tag).subscribe(
      res => {
        if (res) {
          console.log(res);
          for (let i = 0; i < res.hits.hits.length; i++) {
            console.log(this.cardIdentity);
            console.log(res.hits.hits[i]._source.id);
            if (res.hits.hits[i]._source.id == this.cardIdentity) {
              console.log('hit');
              res.hits.hits.splice(i, 1);
            }
            // if(this.relcontent==[]){
            // this.relcontent.push(res.hits.hits[i]._source);
            // }
            // else
            //   {
            for (let j = 0; j < this.relcontent.length; j++) {
              if (res.hits.hits[i])
                if (res.hits.hits[i]._source.id == this.relcontent[j].id) {
                  res.hits.hits.splice(i, 1);
                }

            }
            if (res.hits.hits[i])
              this.relcontent.push(res.hits.hits[i]._source);
            //  }
            // }
          }


          // this.getRelatedFliks(res.contentType);
        }
      },
      error => {
        console.log("error getting content");
      }
    );
    // this.contentService.getContentBySlug(this.contentSlug).subscribe(
    //   res => {
    //     if(res) {
    //       console.log(res);
    //       this.getRelatedFliks(res.contentType);
    //     }
    //   },
    //   error => {
    //     console.log("error getting content");
    //   }
    // );
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
