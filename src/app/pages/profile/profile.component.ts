import { Component, OnInit } from '@angular/core';
import { Celeb } from '../../models/celeb/celeb';
import { Movie } from "../../models/movie/movie";  
import { CelebService } from '../../models/celeb/celeb.service';
import { MovieService } from "../../models/movie/movie.service";
import { FashionService } from "../../models/fashion/fashion.service";
import { ContentService } from "../../models/content/content.service";
import { ProductService } from "../../models/product/product.service";
import { Observable, Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router } from "@angular/router";
import { MetaService } from '@ngx-meta/core';
import { LikeService } from '../../models/like/like.service';


//import { AppState } from '../../app.service';
import { GalleryService } from '../../models/gallery/gallery.service';
  declare var $:any;


@Component({
  
  selector: 'profile', 
  providers: [ ],
  styleUrls: [ './profile.component.css' ],
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  typeOfPage: string;
  passCasting: boolean=false;
  loginPopup: boolean =false;
  followArray=[];
  selectedSubcatagory=[];
  categoryArray=[];
  filterType: any;
  filterFeed: boolean =false;
  store: boolean =false;
  displayStyle: boolean;
  seo: any=null;
  industry: string;
  stealThatStyleCelebData: any;
  gallerySlug=[];
  displayFilm: boolean = false;
  showToggle: boolean = false;
  contentData: any;
  public contents: any;
  public celebContent:any;
  public contentsCount: any;
  public pageNo;
  public galleries =[];
  public moviePhotos: any;
  public gallery: any;
  content: any;
  public miniCard: boolean = true;
  public localState = { value: '' };
  public celeb: Observable<Celeb>;
  public profileData;
  public SuggestedMovies=[];
  public profileObsData;
  public isCeleb : boolean = false;
  public isMovie : boolean = false;
  public readmore : boolean = false;
  public tweetsData=[];
  public galleryPhotos : any;
  public isDesigner : boolean = false;
  public movie: Observable<Movie>;
  public pageType:any;
  public bestSelling : any;
  public filmography : any;
  public celebrityCollection : any;
  public cast : any;
  public crew : any;
  public movieCast;
  public relatedMovies : any;
  public displayCastCrew:boolean=false;
  public peers : any;
  public slug : string;
  public contentType ='product';
  public contentType1 = "news";
  public contentType2 = "gallery";
  public contentType3 = "trailer";
  public topTrailers;
  public isDisplay : boolean = false;
  public show=5;
   public $: any;
    public jQuery: any;

  constructor(
   // public appState: AppState,
    public celebService: CelebService,
    public router: Router, 
    public route: ActivatedRoute, 
    public movieService: MovieService,
    public fashionService : FashionService,
    public contentService : ContentService,
    public productService : ProductService,
    public galleryService : GalleryService,
    private metaService: MetaService,
    public likeService: LikeService,

  ) {}

  public ngOnInit() {

// $(document).ready(function(){
//         $('.feed-li').click(function(){
//         $('.mob-celeb-feed').toggle();
//       });
// });
$(document).ready(function(){
        $('.feed-li').click(function(){
        $('.mob-celeb-feed').toggle();
      });
    });

    this.passCasting=false;
    this.SuggestedMovies=[];
    this.pageNo = 1;
    window.scrollTo(0,0);
    console.log('hello `profile` component');
    this.industry = localStorage.getItem('Industry');
    var urlBreakup = this.router.url.split("/");
    this.typeOfPage=urlBreakup[1];
    // if(this.profileData){
    //   console.log(this.profileData);
    // this.metaService.setTitle(' '+this.profileData.name);
    // }
    // else
    // this.metaService.setTitle(' '+urlBreakup[2]);
    this.slug = urlBreakup[2];
    console.log(this.slug);
    if(urlBreakup[1] == "celeb-store" || "movie-store"){
      this.store=true;
    }
    else
    this.store=false;
    if(urlBreakup[1] == "celeb" || urlBreakup[1] == "celeb-store") {
      this.getPeers(this.slug);
      this.isCeleb = true;
      this.profileObsData = this.celebService.getCelebBySlug(this.slug);
      // this.profileData = this.contentService.getContentByTag(this.slug);
      // this.getCelebrityCollection(this.slug);
    } else if (urlBreakup[1] == "movie"  || urlBreakup[1] == "movie-store") {
      this.isMovie = true;
      this.profileObsData = this.movieService.getMovieBySlug(this.slug);
      // this.getMoviePhotos(this.slug);
    } else if (urlBreakup[1] == "designer") {
      this.isDesigner = true;
      this.profileObsData = this.fashionService.getDesignerBySlug(this.slug);
      // this.getMoviePhotos(this.slug);
    }
      
    if(urlBreakup[1] == "movie"  || urlBreakup[1] == "celeb" ||urlBreakup[1] == "designer") {
      this.getContentByTag(this.slug);
      this.contents = [];
    } else if(urlBreakup[1] == "movie-store"  || urlBreakup[1] == "celeb-store") {
      this.getProductsByTag(this.slug);
      this.contents = [];
    }

    
    this.getMoviesByTag(this.slug);
    this.getPhotosByTag(this.slug);  
    // this.getRelatedMovie(this.slug);
    this.getTweetsByTag(this.slug);
    this.getStealThatStyleForCelebStore(this.industry,this.slug)
    

if(this.slug){
this.isGallery(this.slug);
}

    // this.getProductBySlug('aqua-sphere-kayenne-swim-goggle,-made-in-italy');
    this.pageType = urlBreakup[1];
    this.profileObsData.subscribe(
      res => {
        console.log(res);
        if(res.Items[0]) {
          this.profileData = res.Items[0];
          console.log(this.profileData);
         
          if(urlBreakup[1] == "movie")
            {
              
              this.cast = res.Items[0].cast;
              this.crew = res.Items[0].crew;
              if(this.cast){
                this.followArray=[];
                for(let i=0;i<2;i++){
                  if(this.cast[i])
                  this.checkPeerFollow(this.cast[i].id);
                }
              }
              if(this.crew){
                for(let i=0;i<2;i++){
                  if(this.crew[i])
                  this.checkPeerFollow(this.crew[i].id);
                }
              }
              this.passCasting=true;
              let movieCast = new Array();
              if(this.profileData.cast) {
                for(let j=0; j<this.profileData.cast.length; j++) {
                  movieCast.push(this.profileData.cast[j]);
                }
                this.movieCast = movieCast;
                console.log(this.movieCast);
              }
            } else if(urlBreakup[1] == "designer") {
              this.peers = this.profileData.celeb;
            }
            console.log("********PROFILE DATA************");
            console.log(this.profileData);
            if(this.profileData.genre)
            for(let i=0;i<this.profileData.genre.length;i++){
              this.getSuggestedMovies(this.profileData.genre[i],this.profileData.industry);
            }
        }
      },
      error => {
        console.log("error getting data");
      }
    );
  }

  feedFilterFromBanner(filter) {
    this.displayCastCrew=false;
    if(filter == 'filmography') {
      this.getFilmography();
      this.displayFilm=true;
      
      
    } else {
      this.displayFilm=false;
      this.getContentByTag2(this.slug, filter);
    }
  }
  castCrewDisplay(){
    this.displayCastCrew=true;
  }
  showStoryLine(){
    this.displayCastCrew=false;
    if(!this.displayCastCrew)
    window.scrollTo(0,document.querySelector(".plr25").scrollHeight);
  }
  gotomoviepage(first,second){
    this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
    this.router.navigate([first , second]));
  }
  getSuggestedMovies(genre,industry){
    this.movieService.getMovieByGenre(genre,industry).subscribe(
      res => {
        console.log(res);
        if(res){
          for(let i=0;i<res.hits.hits.length;i++){
            if(res.hits.hits[i]._source.slug != this.slug){
              if(this.SuggestedMovies.indexOf(res.hits.hits[i]._source)==-1){
                this.SuggestedMovies.push(res.hits.hits[i]._source);
              }
            }
          }
          console.log(this.SuggestedMovies);
          
        }
      },
      error => {
        console.log("Error in suggested-movies");
      }
    );
    
    

  }
  celebFilterFromBanners(filter) {
    console.log(filter);
    if(filter == 'style') {
      console.log(filter);
      this.displayStyle=true;
      this.getStealThatStyleForCelebStore(this.industry,this.slug);
      
    } else {
      this.displayStyle=false;
     // this.getContentByTag2(this.slug, filter);
    }
  }
  public submitState(value: string) {
    console.log('submitState', value);
   // this.appState.set('value', value);
    this.localState.value = '';
  }

  checkStoryline(story){
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    if(story.storyLine!=null){
      return true;
    }
    else 
    return false;
  }

  // getCastAndCrew(slug){
  //   this.movieService.getCastAndCrew(slug).subscribe(
  //     res => {
  //       console.log(res);
  //       if(res){
  //         this.castCrew = res;
  //       }
  //     },
  //     error => {
  //       console.log("Error in Cast and Crew");
  //     }
  //   );
  // }
  getMovieBySlug(slug){
      this.movieService.getMovieBySlug(slug).subscribe(
        res => {
          console.log(res);
          if(res){
            this.profileData = res;
            let movieCast = new Array();
            if(this.profileData.cast) {
              for(let j=0; j<this.profileData.cast.length; j++) {
                movieCast.push(this.profileData.cast[j]);
              }
              this.movieCast = movieCast;
              console.log(this.movieCast);
            }
          }
        },
        error => {
          console.log("Error in Cast and Crew");
        }
      );
    }

  readMore(){
    this.isDisplay = !this.isDisplay;
    console.log(this.isDisplay);
  }
  checkData(length){
    console.log(length);
    if(length>360)
      this.showToggle=true;
    return true;
  }

  getPhotosByTag(slug){
    this.contentService.getPhotosByTag(this.slug).subscribe(
      res => {
        console.log(res);
        this.gallery = res.hits.hits;
        console.log("********PHOTOS*********");
        console.log(this.gallery);
      },
      error => {
        console.log("Error in Photos");
      }
    );
  }

  getMoviesByTag(slug){
    this.contentService.getMoviesByTag(this.slug).subscribe(
      res => {
        console.log(res);
        this.moviePhotos = res.hits.hits;
        console.log("********Movie PHOTOS*********");
        console.log(this.moviePhotos);
      },
      error => {
        console.log("Error in Photos");
      }
    );
  }

  getCelebrityCollection(slug){
    this.celebService.getCelebrityCollections(slug).subscribe(
      res => {
        console.log(res);
        if(res){
          this.celebrityCollection = res;
        }
      },
      error => {
        console.log("Error in Celebrity Collection");
      }
    );
  }

  // getTopContent(type) {
  //   this.contentService.getTopContent(type).subscribe(
  //     res => {
  //       console.log(res);
  //       if(res){
  //           this.topTrailers = res;
  //       }
  //     },
  //     error => {
  //       console.log("Error in getting top content");
  //     }
  //   );
  // }
  getContent(){
    
    this.pageNo=this.pageNo+1;
    this.getContentByTag(this.slug);
  }
  selectedCatagory(catagory){
    
    console.log('eeevent emitted');
    console.log(catagory);
    this.selectedSubcatagory=catagory;
    // this.getProductsByTag(this.slug);
  }
  getProductContent(){
    this.pageNo=this.pageNo+1;
    this.getProductsByTag(this.slug);
  }
  getStealThatStyleForCelebStore(industry,slug){ 
    this.celebContent=[]; 
    this.fashionService.stealThatStyleCelebStore(this.industry,this.slug).subscribe(
      res => {
        console.log(res);
        if(res){
          this.stealThatStyleCelebData = res.hits.hits;
          if(res.hits.hits)
          for(let i=0; i<res.hits.hits.length; i++){
            if(res.hits.hits[i]._source){
              this.celebContent.push(res.hits.hits[i]._source);
            }
          }
          console.log(this.celebContent);
        }
      },
      error => {
        console.log("Error in stealThatStyle");
      }

    );
  }

  getContentByTag(slug){
    this.contentService.getContentByTag(this.slug, this.pageNo).subscribe(
      res => {
        if(res) {
          console.log(res);
          this.contentsCount = res.hits.total;
         if(res.hits.hits)
         for(let i=0; i<res.hits.hits.length; i++) {
          if(res.hits.hits[i]._source) {
            if(res.hits.hits[i]._source.slug) {
              if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                this.contents.push(res.hits.hits[i]);
              }
            }
          }
         }
         console.log("***********content**********")
         console.log(this.contents);
        }
      },
      error => {
        console.log("error getting content");
      }
    );
  }
  getContentFilterByTag(slug,type){
    this.contents=[];
    this.contentService.getContentFilterByTag(this.slug, this.pageNo,type,this.typeOfPage).subscribe(
      res => {
        if(res) {
          console.log(res);
          this.contentsCount = res.hits.total;
         if(res.hits.hits)
         for(let i=0; i<res.hits.hits.length; i++) {
          if(res.hits.hits[i]._source) {
            if(res.hits.hits[i]._source.slug) {
              if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                this.contents.push(res.hits.hits[i]);
              }
            }
          }
         }
         console.log("***********content**********")
         console.log(this.contents); 
        }
      },
      error => {
        console.log("error getting content");
      }
    );

  }

  getProductsByTag(slug){
    this.contents=[];
    this.categoryArray=[];
    this.pageNo='all';
    this.contentService.getProductsByTag(this.slug, this.pageNo).subscribe(
      res => {
        if(res) {
          console.log(res);
          this.contentsCount = res.hits.total;
          if(res.hits.hits)
         for(let i=0; i<res.hits.hits.length; i++) {
          if(res.hits.hits[i]._source) {
            if(res.hits.hits[i]._source.slug) {
              if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                if(this.selectedSubcatagory.length==0)
                this.contents.push(res.hits.hits[i]);
                else{
                  for(let j=0;j<this.selectedSubcatagory.length;j++){
                    if(res.hits.hits[i]._source.catagory[0][3])
                    if(this.selectedSubcatagory[j]==res.hits.hits[i]._source.catagory[0][3]){
                      this.contents.push(res.hits.hits[i]);
                    }
                    if(!res.hits.hits[i]._source.catagory[0][3])
                    if(this.selectedSubcatagory[j]==res.hits.hits[i]._source.catagory[0][2]){
                      this.contents.push(res.hits.hits[i]);
                    }
                  }
                }
              }
            }
          }
         }
         console.log("***********products**********")
         console.log(this.contents);
         for(let i=0;i<this.contents.length;i++){
           if(this.contents[i]._source.category[0]){
             if(this.contents[i]._source.category[0][3]){
            if(this.categoryArray.indexOf(this.contents[i]._source.category[0][3])==-1){
               console.log(this.contents[i]._source.category[0][3]);
              this.categoryArray.push(this.contents[i]._source.category[0][3]);
            }
             }
             if(!this.contents[i]._source.category[0][3]){
              if(this.categoryArray.indexOf(this.contents[i]._source.category[0][2])==-1){
                 this.categoryArray.push(this.contents[i]._source.category[0][2]);
              }
               }
           }
         }
         console.log(this.categoryArray);
        }
      },
      error => {
        console.log("error getting content");
      }
    );
  }
  getTweetsByTag(slug){
    this.contentService.getTweetsByTag(this.slug).subscribe(
      res => {
        if(res) {
          console.log(res);
          let tempData=[];
          if(res.hits.hits)
          for(let i=0; i<res.hits.hits.length; i++) {
          if(res.hits.hits[i]._source) {
            if(res.hits.hits[i]._source.slug) {
              if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                tempData.push(res.hits.hits[i]);
              }
            }
          }
         }
          //this.tweetsData = tempData;
          for(let i=0;i<tempData.length;i++){
            this.tweetsData.push(tempData[i]._source)
          }
          // this.contentType = this.contents.contentType;
         // this.getRelatedFliks(this.contentType);
         console.log("***********tweetsData**********")
         console.log(this.tweetsData);
        }
      },
      error => {
        console.log("error getting content");
      }
    );

  }
  feedFilter(type){
    this.displayCastCrew=false;
    this.displayStyle=false;
    this.displayFilm=false;
    // this.filmography=[];
    this.filterFeed=true;
    this.filterType=type;
    console.log(type);

    console.log(this.filterFeed);
    // this.pageNo=this.pageNo+1;
    this.getContentFilterByTag(this.slug,type);
  }
  getFeedContent(){
    this.pageNo=this.pageNo+1;
    this.getContentFilterByTag(this.slug,this.filterType);
  }
  

  getContentByTag2(slug,filter){
    this.contentService.getContentByTag2(this.slug,filter).subscribe(
      res => {
        if(res) {
          console.log(res);
          let tempData=[];
          if(res.hits.hits)
         for(let i=0; i<res.hits.hits.length; i++) {
          if(res.hits.hits[i]._source) {
            if(res.hits.hits[i]._source.slug) {
              if(res.hits.hits[i]._source.slug.split('-')[0] != "temp" && res.hits.hits[i]._source.slug.split('-')[0] != "test") {
                tempData.push(res.hits.hits[i]);
              }
            }
          }
         }
          this.contents = tempData;
          // this.contentType = this.contents.contentType;
         // this.getRelatedFliks(this.contentType);
         console.log("***********content**********")
         console.log(this.contents);
        }
      },
      error => {
        console.log("error getting content");
      }
    );
  }

  getFilmography() {
    this.movieService.getMoviesForCeleb(this.slug).subscribe(
      res => {
        console.log(res);
        if(res){ 
          this.relatedMovies = res.hits.hits;
          let allFilmography = new Array();
          if(this.relatedMovies)
          for(let i=0; i<this.relatedMovies.length; i++) {
            let filmography = this.newFilmography();
            filmography.title = this.relatedMovies[i]._source.title;
            filmography.slug = this.relatedMovies[i]._source.slug;
            filmography.profilePic = this.relatedMovies[i]._source.profilePic;
            filmography.releaseDate = this.relatedMovies[i]._source.dateOfRelease;
            filmography.storyLine = this.relatedMovies[i]._source.storyLine;
            filmography.genre = this.relatedMovies[i]._source.genre;
            if(this.relatedMovies[i]._source.cast) {
              for(let j=0; j<this.relatedMovies[i]._source.cast.length; j++) {
                if(this.relatedMovies[i]._source.cast[j].slug == this.slug) {
                  filmography.characterName = this.relatedMovies[i]._source.cast[j].characterName;
                  filmography.role = this.relatedMovies[i]._source.cast[j].castRole;
                }
              }
            }
            if(this.relatedMovies[i]._source.crew) {
              for(let j=0; j<this.relatedMovies[i]._source.crew.length; j++) {
                if(this.relatedMovies[i]._source.crew[j].slug == this.slug) {
                  filmography.characterName = this.relatedMovies[i]._source.crew[j].characterName;
                  filmography.role = this.relatedMovies[i]._source.crew[j].crewRole;
                }
              }
            }
            allFilmography.push(filmography);
          }
          this.filmography = allFilmography;
        }
      },
      error => {
        console.log("Error in Related Movie");
      }
    );
  }
  showmore(){
    this.readmore=true;
  }
  showReview(){
    console.log('event emitted');
    this.getReviewCard(this.slug);
  }

  getReviewCard(slug){
    this.movieService.getReviewCard(slug).subscribe(
      res => {
        console.log(res);
        if(res){
          if(res.hits.hits[0])
           this.contentData = res.hits.hits[0]._source;
        }
        console.log(this.contentData);
        
      },
      error => {
        console.log("Error in Related Movie");
      }
    );

  }

  newFilmography() {
    return {
      title : "",
      slug: "",
      profilePic : "",
      characterName : "",
      role : "",
      releaseDate : "",
      storyLine : "",
      genre:[],
    }
  }

  goToMovie(slug) {
    this.router.navigate(['movie',slug]); 
  }

  // gotoProduct(){
  //   this.getStealThatStyleForCelebStore(this.industry,this.slug);
  //   this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
  //   this.router.navigate(['product/' + this.celebContent.slug]));
  // }

  getPeers(slug){
    this.followArray=[];
    this.movieService.getMoviesForCeleb(slug).subscribe(
      res => {
        console.log(res);
        if(res){
          this.relatedMovies = res.hits.hits;
          let peers = new Array();
          if(this.relatedMovies)
          for(let i=0; i<this.relatedMovies.length; i++) {
            if(this.relatedMovies[i]._source.cast) {
              for(let j=0; j<this.relatedMovies[i]._source.cast.length; j++) {
                peers.push(this.relatedMovies[i]._source.cast[j]);
              }
            }
            
          }
          console.log(peers);
          this.removeDuplicates(peers);
          
        }
      },
      error => {
        console.log("Error in Related Movie");
      }
    );
  }
  checkPeerFollow(id){
    let isFollowBody = {
      userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
      entityId : id,
      type : "follow"
    };
      this.likeService.isLiked(isFollowBody).subscribe(
          res => {
              console.log(res);
            if(res.data) {
              this.followArray.push(res);
              
            }
            },
            error => {
              console.log("Error");
            }
      );
      console.log(id);
console.log(this.followArray);
  }
  followCeleb(celeb,i,followArr){
    var logged=localStorage.getItem('isLoggedIn');
    if(logged=='true'){
      if(followArr.data.Count=='0'){
        followArr.data.Count='1';
      
      }
      else{
        followArr.data.Count='0';
      }
    
console.log(celeb+i);
var profileDataObj = {
  id : celeb.id,
  name : celeb.name,
  slug : celeb.slug,
  profilePic : celeb.profilePic,
  type : 'celeb'
};
let followBody = {
  userId : localStorage.getItem("username"),
  entityId : celeb.id,
  entityObj :profileDataObj,
  type : "follow"
};
this.likeService.like(followBody).subscribe(
    res => {
        console.log(res);
      },
      error => {
        console.log("Error in posting follow");
      }
);

}
else
this.loginPopup=true;
  }
  closeLoginPopup () {
    console.log("event emmitted");
    this.loginPopup = false; 
  }
removeDuplicates(array) {
  console.log(array);
  console.log(this.slug);
  let tempArray = new Array();
  for(let i=0; i<array.length; i++) {
    var itemAvilable = false;
    for(let j=0; j<tempArray.length; j++) {
      if(array[i].slug == tempArray[j].slug) {
        itemAvilable = true;
      }
    }
    if(!itemAvilable)
    tempArray.push(array[i]);
  }
  for(let i=0; i<tempArray.length; i++) {
    if(tempArray[i].slug == this.slug) {     //this is to check profile slug and remove
      tempArray.splice(i, 1);
    }
  }
  console.log(tempArray);
  this.peers = tempArray;  
  for(let i=0;i<this.peers.length;i++){
    this.checkPeerFollow(this.peers[i].id);
  }
}

  getProductBySlug(slug){
    this.productService.getProductBySlug(slug).subscribe(
      res => {
        console.log(res);
       
        if(res){
          this.content = res.Items[0];
          console.log(this.content);
        }
      },
      error => {
        console.log("Error in Related Movie");
      }
    );
  }

  isGallery(slug) {
    let isGalleryBody = {
      celebSlug : this.slug,
    };
    this.galleryService.gallery(isGalleryBody).subscribe(
        res => {
            console.log(res);
            // this.galleryCounter = res.totalCount;
            // this.galleries = res.data;
            if(res.data)
            for(let i=0;i<res.data.length;i++){
              this.galleries.push(res.data[i].gallery);
            }
            if(res.data)
            for(let i=0;i<res.data.length;i++){
              this.gallerySlug.push(res.data[i].slug);
            }
            
          },
          error => {
            console.log("Error");
          }
      );
    }

  


}
