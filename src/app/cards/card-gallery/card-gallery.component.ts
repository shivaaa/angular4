import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  import { Router } from '@angular/router';
  // import { AppState } from '../../app.service';
  import { CardService } from '../../models/card/card.service';
  
  
  @Component({
    
    selector: 'card-gallery', 
    providers: [ ],
    styleUrls: [ './card-gallery.component.css' ],
    templateUrl: './card-gallery.component.html'
  })
  export class CardGalleryComponent implements OnInit {
    shareProfilePic: any;
    shareSlug: any;
    Show: string;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    public isMiniCard: boolean = false;
    public assnData: any;
    @Input() miniCard : boolean;     
    @Input() content: any;
    public today: number = Date.now();
    public localState = { value: '' };
    public cardGallery;
    public page: string;
    public cardId: string;
    public contentData : any;
    public cardtype;
    public carouselItem;
    public pivot:any;
    public cardSlug:any;
    public displayCarousel: boolean = false;
    items: Array<any> = [];

  
    constructor(
      // public appState: AppState,
      public cardService : CardService,
      public router : Router,
    ) {}
  
    public ngOnInit() {
      if(this.content) {
        console.log("*************THES IS CONTENT GALLERY***********");
        console.log(this.content);
        this.contentData = this.content;
        this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
        if(this.contentData.celeb)
          if(this.contentData.celeb[0])
          this.assnData=this.contentData.celeb[0];
        if(this.contentData.movie)
          if(this.contentData.movie[0])
          this.assnData=this.contentData.movie[0];
       
        if(this.assnData)
        console.log(this.assnData);
        if(this.content.imageTemplte)
          this.cardtype = this.content.imageTemplte.toString();
          
          this.createdAt=this.content.createdAt;
      }
        var urlBreakup = this.router.url.split("/");
        console.log(urlBreakup);
        this.page = urlBreakup[1]; 
        this.cardSlug=urlBreakup[2];
      console.log('hello `card-gallery` component');
      // if(this.page=='feed')
      // this.getCardGallery();
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='sec';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
      this.Show=this.ShowTime+this.time;
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    setpiviot(i){
      this.pivot=i;
      this.items = this.content.media.gallery;
      this.carouselItem = this.content.media.gallery[this.pivot];
      this.displayCarousel = true;
    }
    prev() {
      if(this.pivot == 0)
        this.pivot = this.items.length-1;
      else
        this.pivot--;
      this.carouselItem = this.items[this.pivot];
    }

    next() {
      if(this.pivot == this.items.length-1)
        this.pivot = 0;
      else
        this.pivot++;
      this.carouselItem = this.items[this.pivot];
    }


    getCardGallery(){
        this.cardService.getGalleryCardBySlug().subscribe(
            res => {
                console.log(res);
                if(res){
                  this.cardGallery = res;
                  console.log("*********CARD GALLEY**************");
                  console.log(this.cardGallery);
                  this.cardId = this.cardGallery.id;
                }
              },
              error => {
                console.log("Error in Card Gallery");
              }
        );
    }


    

    gotocontent(){
      if(this.cardSlug !=this.contentData.slug ){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }
  
  }