import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Celeb } from './celeb';
import { environment } from '../globals';

@Injectable()
export class CelebService {

  private environmentUrl :  any = environment;
  
  constructor(
  	    private http: Http,
    ) {

  }

  getCelebBySlug(slug) {
  	let celebUrl = this.environmentUrl.baseUrl+'celeb-ms/getCelebBySlug/'+slug;
      return this.http.get(celebUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getCelebrityCollections(slug){
    let celebUrl = this.environmentUrl.baseUrl+'celeb-ms/collections/'+slug;
    return this.http.get(celebUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
