import {
    Component,
    OnInit
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  import { Router,ActivatedRoute } from "@angular/router";
  import { FashionService } from "../../models/fashion/fashion.service";
  import { BrandService } from "../../models/brand/brand.service";
  import {Location} from '@angular/common'; 

  @Component({
    
    selector: 'brandstore', 
    providers: [ ],
    styleUrls: [ './brandstore.component.css' ],
    templateUrl: './brandstore.component.html'
  })
  export class BrandStoreComponent implements OnInit 
  {
    shopByData=[];
    public productArray = [];
    branddata: any;
    public brandArray = [];
    Allbrand: any;    
    public widgets;
    shopbyVideosData: any;
    public localState = { value: '' };
    public pageType:any;
    public industry : any;
    public bannerType='socialIconsBanner'
    public doubleRows = "double-rows";
    public productRows='productcarousel';
    public stealThatStyleData : any;
    public brand;
    public slug : string ;
    public shopbyvideos ='shopbyvideos';
  
    constructor(
      private _location: Location,
      // public appState: AppState,
      public router: Router,
      public fashionService : FashionService,
      public brandService : BrandService,
      public activatedRoute: ActivatedRoute,
    ) {}
  
    public ngOnInit() {
      console.log('hello `brandstore` component');
      var urlBreakup = this.router.url.split("/");
      this.pageType = urlBreakup[1];
      window.scrollTo(0,0);
      this.slug = urlBreakup[2];
      
      this.getallproducts(this.slug);
      this.industry=localStorage.getItem('Industry');
      if(this.pageType=='brand-store')
      this.getshopbyvideosbyBrand(this.industry);
      this.getAllBrands();
      
      if(this.pageType=='brand-store'){
        this.getCategoryWidgets('brand-store');
        let route = this.activatedRoute.params.subscribe(params => {
          console.log(params);
          this.getBrand(params.slug);
          // this.getProduct(params.slug);
          window.scrollTo(0,0);
        });
       
      }
      
    }
    goBack(){
      this._location.back();
      
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    getBrand(slug){
      this.brandService.getBrandBySlug(this.slug).subscribe(
        res =>{
          if (res) {
            this.brand=res;
            
            
            console.log(this.brand);
          }
        },
        error => {
          console.log("Error in Brand");
        }
      );
    }
    getallproducts(slug){
      this.brandService.getallproducts(this.slug).subscribe(
        res =>{
          if (res) {
            this.branddata=res.hits.hits;
            
            
            console.log(this.branddata);
            for(let i=0;i<this.branddata.length;i++){
              this.productArray.push(this.branddata[i]._source)
            }
          }
        },
        error => {
          console.log("Error in Brand");
        }
        
      );
      console.log(this.productArray);
    }

    getCategoryWidgets(tag) {
      this.fashionService.getCategoryWidgets(tag).subscribe(
        res => {
          if(res) {
            console.log(res);
            this.widgets = res.hits.hits;
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log("***********widgets**********")
           console.log(this.widgets);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }
    getAllBrands(){
      this.brandService.getAllBrands().subscribe(
        res =>{
          if (res) {
            this.Allbrand=res.hits.hits;
            
            
            console.log(this.Allbrand);
            for(let i=0;i<this.Allbrand.length;i++){
              this.brandArray.push(this.Allbrand[i]._source)
            }
          }
        },
        error => {
          console.log("Error in Brand");
        }
      );
    }
    getshopbyvideosbyBrand(industry){
      this.fashionService.getshopbyvideosByBrand(industry).subscribe(
        res => {
          console.log("THIS IS shopbyvideos DATA");
          console.log(res);
          this.shopbyVideosData=res.hits.hits;
          console.log( this.shopbyVideosData);
          for(let i=0;i<this.shopbyVideosData.length;i++){
            this.shopByData.push(this.shopbyVideosData[i]._source);
          }
  
          // if(res){
            
          //     this.pagecontent = res;
          // }
        },
        error => {
          console.log("Error in getting top content");
        }
      );

    }
  }
  
  
  