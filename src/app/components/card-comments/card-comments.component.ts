import { Component, OnInit,Input, Pipe, PipeTransform,Attribute } from '@angular/core';
import { Router } from '@angular/router';
// import { AppState } from '../../app.service';
import { CommentsService } from '../../models/comments/comments.service';
import { DatePipe } from '@angular/common';
// import { CommentsService } from "src/app/models/comments/comments.service";

@Component({
  
  selector: 'card-comments', 
  providers: [ ],
  styleUrls: [ './card-comments.component.css' ],
  templateUrl: './card-comments.component.html'
})
export class CardCommentsComponent  implements OnInit  {
  nullComments: boolean;
  time: string;
  ShowTime: any;
  milisecondsDiff: number;
  miliseconds: number;
  endDate: any;
  startDate: any;
  timeArray = [];
  public localState = { value: '' };
  @Input() slug;                             //from @Input
  @Input() cardId;                             //from @Input
  public newComment; 
  public comments;
  public unOrderedComments;
  public dispViewMoreComments : boolean = false;
  public isContentPage : boolean = false;
  public today: number = Date.now();

  constructor(@Attribute("format") format,
    // public appState: AppState,
    public router : Router,
    public commentsService : CommentsService
  ) {}

  public ngOnInit() {
    var urlBreakup = this.router.url.split("/"); 
    console.log(this.cardId);
    if(urlBreakup[1] == "content") {
      this.isContentPage = true;
    }
    if(this.cardId) {
      this.getComments('null');
    } else {

    }
    
  }

  getComments(body) {
      this.commentsService.getCommentsByContentId(this.cardId).subscribe(
          comments => {
              console.log(comments);
              if(comments.Count){
                if(comments.Count > 2) {
                  this.dispViewMoreComments = true;
                }
              }
                if(comments.Items)
                  this.timeArray=[];
                // this.comments = comments.Items;
                this.unOrderedComments = comments.Items;
                this.comments = this.unOrderedComments.sort(this.compare);
                console.log(this.comments.length);
                for(let i=0; i<this.comments.length;i++){
                console.log(this.comments.length);
                  this.timeArray.push(this.timeDifference(this.comments[i].createdAt));
                }
                console.log(this.timeArray);

                if(body != 'null') {
                 
                  this.timeArray.push('1s');
                }
              
            },
            error => {
              console.log("Error in getting comments");
              // this.mockComments();
            }
      );
  }

  compare(a,b) {
    if (new Date(a.createdAt).getTime() < new Date(b.createdAt).getTime())
      return -1;
    if (new Date(a.createdAt).getTime() > new Date(b.createdAt).getTime())
      return 1;
    return 0;
  }

  timeDifference(created){
    console.log(created);
    console.log(new Date);
    this.startDate = new Date(created);
    this.endDate = new Date;
    this.miliseconds = this.endDate - this.startDate;
    console.log(this.miliseconds);
    this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
    if(this.milisecondsDiff >=1){
      this.ShowTime= Math.floor(this.milisecondsDiff); 
      this.time='w';
    }
    else {
      this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
      if(this.milisecondsDiff >=1){
      this.ShowTime= Math.floor(this.milisecondsDiff); 
      this.time='d';
      }
      else{
        this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
        if(this.milisecondsDiff >=1){
          this.ShowTime= Math.floor(this.milisecondsDiff);
          this.time='h';
        }
        else{
          this.milisecondsDiff=this.miliseconds /1000/60;
          if(this.milisecondsDiff >=1){
          this.ShowTime= Math.floor(this.milisecondsDiff);
          this.time='m';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000;
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='s';
          }
        } 

      }
    }
   
    console.log(this.ShowTime,this.time);
    // if(this.ShowTime==0){
    //   console.log(this.ShowTime);
    //   this.ShowTime='Just Now';
    //   this.time=' ';
    //   return  this.ShowTime;
    // }
    return this.ShowTime+this.time;   
  }
  

  postComment(body) {
      this.commentsService.postComment(body).subscribe(
          res => {
              this.getComments(body);
              console.log(res);
              // if(!this.comments) {
              //   this.comments = new Array();
              // }
              // this.comments.push(body);
              // this.timeArray.push('1s');
            },
            error => {
              console.log("Error in posting comments");
            }
      );
  }

  public comment(){
    //console.log(this.comments.name);
    if(localStorage.getItem("isLoggedIn") == "true") {
      if(this.newComment) {
        let newCommentObj = {
          userName : localStorage.getItem("name"),
          userId : localStorage.getItem("username"),
          entityId : this.cardId,
          commentText : this.newComment,
          timestamp : "1s"
        };
        this.postComment(newCommentObj);
        this.newComment = "";
      }
    } else {
      // this.router.navigate(['login']); 
      this.displayLoginPopup = true; 
      console.log("redirect to login.");
    }
  }

  goToContentPage() {
    this.isContentPage=true;
  }

  public displayLoginPopup : boolean = false;

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }
  
  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }

 
  
}
