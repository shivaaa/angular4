import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class CommentsService {
    private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  getCommentsByContentId(id) : Observable<any>{  
  	let commentsUrl = this.environmentUrl.baseUrl + 'comments-ms/getAllCommentsByEntity/'+id;
      return this.http.get(commentsUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  postComment(body) : Observable<any>{
  	let commentsUrl = this.environmentUrl.baseUrl +'comments-ms/postComment';
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(commentsUrl, body, options)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
