import {
  Component,
  OnInit,
  Input
} from '@angular/core';

// import { AppState } from '../../app.service';

@Component({
  
  selector: 'card-nowshowing', 
  providers: [ ],
  styleUrls: [ './card-nowshowing.component.css' ],
  templateUrl: './card-nowshowing.component.html'
})
export class CardNowshowingComponent implements OnInit {
  isMiniCard: boolean;
  public localState = { value: '' };
  @Input() content: string;
  @Input() miniCard : boolean;
  public contentData : any;
  constructor(
    // public appState: AppState,
  ) {}

  public ngOnInit() {
    console.log('hello `card-nowshowing` component');
    this.contentData = this.content;
    if(this.miniCard) {
      this.isMiniCard = this.miniCard;
    }
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
