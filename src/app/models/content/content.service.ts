import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class ContentService {
  private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  getContentBySlug(slug) {
    console.log(slug);
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE%20AND%20slug:%20"'+slug+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getContentById(slug) {
    let contentUrl = this.environmentUrl.baseUrl+'content-ms/getContentById/'+slug;
    return this.http.get(contentUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    
  }

  industryTweets(body) {
  
    let contentUrl = this.environmentUrl.baseUrl+'content-ms/getTweetContentByIndustry';
    let bodyString = JSON.stringify(body);
    let headers      = new Headers({ 'Content-Type': 'application/json' });
    let options       = new RequestOptions({ headers: headers });
    return this.http.post(contentUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json() || 'Server error getting content'))
  }
  getTopContent(type,industry) {
    let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND contentType:%22'+type+'%22%20AND%20industry:%22'+industry+'%22';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getAllContent() {
    let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getIndustryFeed(industry,pageNo) {
  	// let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:'+industry;
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=5&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND tags:'+industry;
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  getUserFeed(body,pageNo) : Observable<any>{
     //let likeUrl = this.environmentUrl.baseUrl+'likes-ms/getUserPosts';
    //  let likeUrl = this.environmentUrl.elasticUrl+'like/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND userId:"'+body.userId+'" AND type:"'+body.type+'"';
     let likeUrl = this.environmentUrl.elasticUrl+'like/_search?pretty=true&sort=createdAt:desc&size=5&from='+((pageNo-1)*5)+'&q=userId:"'+body.userId+'" AND type:"'+body.type+'"';
     return this.http.get(likeUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  //     let headers = new Headers({ 'Content-Type': 'application/json' });
  // //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
  //   let options = new RequestOptions({ headers: headers });
  //   return this.http.post(likeUrl, body, options)
  //     .map((res:Response) => res.json())
  //     .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getContentByType(slug) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND contentType:"'+slug+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getContentByTypeIndustry(slug,industry,pagenumber) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=5&from='+((pagenumber-1)*5)+'&q=status:ACTIVE AND contentType:"'+slug+'" AND industry:"'+industry+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getRelatedFliks(slug,tag) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND contentType:"'+slug+'" AND tags:"'+tag+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getContentByTag(slug,pageNo) {
  	// let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=5&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND tags:"'+slug+'"';    
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getContentFilterByTag(slug,pageNo,type,pageType) {
    if(type=='Gallery' || type=='news'  ){
  	// let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"'+type+'" AND tags:"'+slug+'"';    
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    if(type=='Videos'){
      // let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
      let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"video-song" AND tags:"'+slug+'"';    
        return this.http.get(contentUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
      }
      if(type=='Social' && pageType=='movie'){
        let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"social-buzz" AND movie.slug:"'+slug+'"';    
          return this.http.get(contentUrl)
          .map((res:Response) => res.json())
          .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
        }
        if(type=='Social' && pageType=='celeb'){
          let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"social-buzz" AND celeb.slug:"'+slug+'"';    
            return this.http.get(contentUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
          }
        if(type=='*'){
          // let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
          let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"*" AND tags:"'+slug+'"';    
            return this.http.get(contentUrl)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
          }
          if(type=='interview'){
            // let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
            let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=9&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND contentType:"interview" AND tags:"'+slug+'"';    
              return this.http.get(contentUrl)
              .map((res:Response) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
            }
  }

  getTweetsByTag(slug) {
    let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'" AND contentType:"tweet"';
    return this.http.get(contentUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }


  getContentGalley() {  
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=20&q=status:ACTIVE AND contentType:"gallery"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getProductsByTag(slug,pageNo) {
    let contentUrl
    if(pageNo == 'all')
  	contentUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"';
    else
  	contentUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=5&from='+((pageNo-1)*5)+'&q=status:ACTIVE AND tags:"'+slug+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getContentByTag2(slug,type) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'" AND contentType:"'+type+'"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getPhotosByTag(slug) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'" AND contentType:"gallery"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getMoviesByTag(slug) {
  	let contentUrl = this.environmentUrl.elasticUrl+'contents/_search?pretty=true&sort=createdAt:desc&size=10000&q=status:ACTIVE AND tags:"'+slug+'"  AND contentType:"movie"';
      return this.http.get(contentUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

}
