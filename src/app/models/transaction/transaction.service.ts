import { Injectable, Inject } from '@angular/core';
import { Jsonp, Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class TransactionService {

    private environmentUrl :  any = environment;
	constructor(
		private http: Http
	) { }

	getOptions(){
    	let headers      = new Headers({ 'Content-Type': 'application/json' }); 
    	// let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); 
	  	headers.append('X-Api-Key','e413f4a94cf1c7de02ef2ebfd2a0f487'); 
	  	headers.append('X-Auth-Token','ce3ad5340b8f1d91977929093a0f6e3d'); 
  		let options = new RequestOptions({ headers: headers, method: "post" });
		return options;
	}

    getTokenForPayment() : Observable<any>{
    let transactionUrl = "https://api.instamojo.com/oauth2/token/";
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let body = {
            'grant_type': 'client_credentials',
            'client_id': 'wO2pAs4FPHfwwXfvOfo2mdjr0V7O2GQnUDspTaaA',
            'client_secret': 'McYKf15F41GzHBQsKnblVGqk9ohxv4FS9OocKdJVYzgii8FqWUguknr1TAoktmx3fCO2bnEpc0WpEc6JTECjs1fD0cclfsdauii7n76QTUcEzt8aXu5YfnIqO3D0bZTn',
        }
        console.log(headers);
        let options = new RequestOptions({ headers: headers });
        return this.http.post(transactionUrl, body, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    createTransaction(body) : Observable<any>{
        body = {
            reqData : body
        };
    let transactionUrl = this.environmentUrl.baseUrl+"checkout-ms/paymentRequest";
        let headers = new Headers({ 'Content-Type': 'application/json' });
    	// let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); 
        // headers.append('X-Api-Key','e413f4a94cf1c7de02ef2ebfd2a0f487'); 
        // headers.append('X-Auth-Token','ce3ad5340b8f1d91977929093a0f6e3d'); 
        console.log(headers);
        let options = new RequestOptions({ headers: headers });
        return this.http.post(transactionUrl, body, options)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    verifyPayment(payment_id,payment_request_id){
        let body = {
            payment_id : payment_id,
            payment_request_id : payment_request_id,
        };
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let transactionUrl = this.environmentUrl.baseUrl+"checkout-ms/checkPaymentStatus";
            let options = new RequestOptions({ headers: headers });
            return this.http.post(transactionUrl, body, options)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    requestInvoice(body) : Observable<any>{
        let invoiceUrl = this.environmentUrl.baseUrl + "email-ms/sendInvoiceMail";
        let headers = new Headers({ 'Content-Type': 'application/json' });
      //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
        let options = new RequestOptions({ headers: headers });
        return this.http.post(invoiceUrl, body, options)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

}
