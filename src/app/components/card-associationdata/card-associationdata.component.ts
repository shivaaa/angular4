import { Component, OnInit, Input} from '@angular/core';


// import { AppState } from '../../app.service';
import { Router } from '@angular/router';
import { LikeService } from '../../models/like/like.service';
@Component({
  
  selector: 'card-associationdata', 
  providers: [ ],
  styleUrls: [ './card-associationdata.component.css' ],
  templateUrl: './card-associationdata.component.html'
})
export class CardAssociationdataComponent implements OnInit {
  contentTags: any;
  public localState = { value: '' };
  @Input() assnData : any;
  @Input() isUser : any;
  @Input() time : any;
  @Input() date : any;
  public isFollowing : boolean = false;
  public type = "movie";
  public assnDataId;
  public pageType :any;


  constructor(
    // public appState: AppState,
    public router: Router,
    public likeService: LikeService,
   
  ) {}

  public ngOnInit() {
    console.log('hello `card-associationdata` component');
    // console.log(this.assnData);
    if(!this.isUser) {
      if(this.assnData) {
        console.log(this.assnData);
        this.type = this.assnData.type;
        this.assnDataId = this.assnData.id;
        var urlBreakup = this.router.url.split('/');
        this.pageType=urlBreakup[1];
        if(this.assnData.tag)
        this.contentTags=this.assnData.tag;

        if(localStorage.getItem("isLoggedIn") == "true" && urlBreakup[1] != "user") {
          this.isFollowed(this.assnDataId);
        } else if(urlBreakup[1] == "user") {
          this.isFollowing = true;
        }
      }
    }
  }

  isFollowed(id) {
    let isLikeBody = {
      userId : localStorage.getItem("username"),
      entityId : id,
      type : "follow"
    };
    this.likeService.isLiked(isLikeBody).subscribe(
        res => {
            console.log(res); 
            if(res.data) {
              if(res.data.Count == 0) {
                this.isFollowing = false;
              } else {
                this.isFollowing = true;
              }
            }
          },
          error => {
            console.log("Error");
          }
    );
  }

  follow(){
    if(localStorage.getItem("isLoggedIn") == "true") {
      this.isFollowing = !this.isFollowing;
      let likeBody = {
        userId : localStorage.getItem("username"),
        entityId : this.assnDataId,
        entityObj : this.assnData,
        type : "follow"
      };
      this.likeService.like(likeBody).subscribe(
          res => {
              console.log(res);
            },
            error => {
              console.log("Error in posting follow");
            }
      );
    } else {
      //redirect to login
      //  this.router.navigate(['login']);
      this.displayLoginPopup = true; 
      console.log("rediret to login");
    }
  }

  gotomoviepage(first,second){
    this.router.navigate([first,second]);
  }
  

  public displayLoginPopup : boolean = false;

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }
  
  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
