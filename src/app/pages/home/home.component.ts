import {
  Component,
  OnInit
} from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import { ActivatedRoute, Router } from "@angular/router";
// import { AppState } from '../../app.service';
import { FashionService } from "../../models/fashion/fashion.service";
import { MetaService } from '@ngx-meta/core';
declare var $:any;
@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './home.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  shopbyVideosData: any;
  /**
   * Set our default values
   */
 
  public localState = { value: '' };
  public shopbyvideos ='shopbyvideos';
  public displayLoginPopup : boolean = false;
  public displayLoginText : boolean = false;
  public latestObsessionstxt = "latestObsessions";
  public threeArray;
  public items;
  public widgets;
  public shopTheLook =[];
  public latestObsessions : any;
  public styleBoards : any;
  public styleboard : any;
  public isLoggedIn = localStorage.getItem("isLoggedIn");
  public industry:any;
  /**
   * TypeScript public modifiers
   */
  constructor(
    // public appState: AppState,
    private fashionService : FashionService,
    public router: Router, 
    private metaService: MetaService,

    
  ) {
   
  }

  

  public ngOnInit() {

// $('#media12').on('slide', '', checkitem);  // on caroussel move
// $('#media12').on('slide.bs.carousel', '', checkitem); // on carousel move

$(document).ready(function(){         // on document ready

// $('#media12 .left.carousel-control').click(function(){checkitem();});
// $('#media12 .right.carousel-control').click(function(){checkitem();});
//   checkitem();
});

// function checkitem()                        // check function
// { console.log('nnnnnnnnnn');
//    var $this = $('#media12');
//     if($('.carousel-inner .item:first').hasClass('active')) {
//         $this.children('.left.carousel-control').hide();
//         $this.children('.right.carousel-control').show();
//     } else if($('.carousel-inner .item:last').hasClass('active')) {
//         $this.children('.left.carousel-control').show();
//         $this.children('.right.carousel-control').hide();
//     } else {
//         $this.children('.carousel-control').show();
//     } 
// }


    window.scrollTo(0,0);
    console.log('hello `Home` component');
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */
    var urlBreakup = this.router.url.split("/"); 
    
    this.metaService.setTitle('Movie News | Celebrity Dresses |Trailers | Reviews | Songs');
    this.metaService.setTag('description','Buy Celebrity Dresses and Accessories, Latest Movie News, Reviews, Box Office Collections, Celebrity Photos, Latest Trailers, Video Songs, Audio Songs, Gossips, Music, Trending Videos, Fun Videos, Celebrity Tweets, Movie Dialogues, Movie Store, Celebrity Store, First Looks, Tv Shows, Movie Store, Designer Store, Women FAshion, Men Fashion, Celebrity Dresses Auctions, Share Your Style and much more only on flikster.com');
    this.metaService.setTag('keywords','Movie News, Trending News, Latest Movie Trailers, Latest News, Movie Reviews,Celebrity Images, Video Songs, Music, Evergreen Songs, Funny Videos, Prank Videos, Popular Songs,Celebrity Gossips, Celebrity Dresses');
    this.metaService.setTag('og:title','Latest Movie News | Trailers | Telugu | Hindi | Tamil | Malayalam | Kannada | Buy Celebrity Costumes & Accessories Online');
    this.metaService.setTag('og:description','Buy Celebrity Dresses and Accessories, Latest Movie News, Reviews, Box Office Collections, Celebrity Photos, Latest Trailers, Video Songs, Audio Songs, Gossips, Music, Trending Videos, Fun Videos, Celebrity Tweets, Movie Dialogues, Movie Store, Celebrity Store, First Looks, Tv Shows, Movie Store, Designer Store, Women FAshion, Men Fashion, Celebrity Dresses Auctions, Share Your Style and much more only on flikster.com');
    this.metaService.setTag('og:keywords','Movie News, Trending News, Latest Movie Trailers, Latest News, Movie Reviews,Celebrity Images, Video Songs, Music, Evergreen Songs, Funny Videos, Prank Videos, Popular Songs,Celebrity Gossips, Celebrity Dresses');
    this.metaService.setTag('fb:app_id', '433236906796403');
    this.metaService.setTag('og:type', 'article');
    this.metaService.setTag('og:url', 'http://flikster.com');

    this.getShopTheLook();
    this.getLatestObsessions();
    this.getStyleBoard();
    this.allgetshopbyvideos();
    this.getCategoryWidgets("home");
    this.industry=localStorage.getItem('Industry');
    if(!this.industry){
    this.industry='bollywood';
    }
    if(!localStorage.getItem("isLoggedIn")) {
      localStorage.setItem("isLoggedIn","false");

    }
    
  }

  goToIndustry(industry) {
    localStorage.setItem("Industry",industry);
    var login =localStorage.getItem('isLoggedIn');
    if(login=='false')
    localStorage.setItem("displayLoginInIndustryPage","true");
    this.industry=localStorage.getItem("Industry");
    this.router.navigate(['industry',industry]);
  }

  checkFunction(){
  var isLoggedIn=localStorage.getItem("isLoggedIn");
  if(isLoggedIn=='false'){
  return true;
  }
  else
  return false;

  }

  allgetshopbyvideos(){
    this.fashionService.allgetshopbyvideos().subscribe(
      res => {
        console.log("THIS IS shopbyvideos DATA");
        console.log(res);
        this.shopbyVideosData=res.hits.hits;
        console.log( this.shopbyVideosData);

        // if(res){
          
        //     this.pagecontent = res;
        // }
      },
      error => {
        console.log("Error in getting top content");
      }
    );
  }

    getCategoryWidgets(tag) {
      this.fashionService.getCategoryWidgets(tag).subscribe(
        res => {
          if(res) {
            console.log(res);
            this.widgets = res.hits.hits;
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log("***********widgets**********")
           console.log(this.widgets);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }

  getShopTheLook() {
    this.shopTheLook=[];
    this.fashionService.shopTheLook().subscribe(
      res => {
        console.log(res);
        if(res) {
          for(let i=0;i<res.hits.hits.length;i++){
            this.shopTheLook.push(res.hits.hits[i]._source);
          }
            
            console.log("******SHOP THE LOOK***************");
            console.log(this.shopTheLook);
            
        }
      },
      error => {
        console.log("error getting shopTheLook");
      }
    );
  }

  getLatestObsessions(){
    this.fashionService.latestObsession().subscribe(
      res => {
        console.log(res);
        if(res){
            this.latestObsessions = res.hits.hits;
            console.log("****************LATEST OBSESSIONS***********");
            console.log(this.latestObsessions);
        }
      },
      error => {
        console.log("Error in LatestObsessions");
      }
    );
  }

  getStyleBoard(){
    this.fashionService.styleBoard().subscribe(
      res => {
        console.log(res);
        // this.styleBoards = res.hits.hits;
        this.styleBoards = res.hits.hits;
      },
      error => {
        console.log("Error in StyleBoard");
      }

    );

  }

  loginPopup() {
    localStorage.setItem("preLogin","home");
    // this.router.navigate(['login']);
    this.displayLoginText = true;
  }

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }
  
  goToProduct(slug) {
    this.router.navigate(['product',slug]);
  }


  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
