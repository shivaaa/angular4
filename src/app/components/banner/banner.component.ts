import {
    Component,
    Input,
    EventEmitter,
    OnInit, OnChanges, Output
  } from '@angular/core';
 
  import { ShareButtons } from '@ngx-share/core';
  // import { AppState } from '../../app.service';
  import { NgSwitch, CommonModule,Location,DatePipe } from '@angular/common';
  import { ActivatedRoute, Router } from "@angular/router";
  import { Observable, } from "rxjs/Observable";
  import { FashionService } from "../../models/fashion/fashion.service";
  import { LikeService } from '../../models/like/like.service';
  import { MovieService } from "../../models/movie/movie.service";
  import { appStoreLinks } from "../../models/globals";
  import { MetaService } from '@ngx-meta/core';
  // import { FacebookSer175vice, LoginResponse,InitParams,UIParams, UIResponse } from 'ngx-facebook';
 declare var $:any;
  @Component({
    
    selector: 'banner', 
    providers: [CommonModule, ],
    styleUrls: [ './banner.component.css' ],
    templateUrl: './banner.component.html'
  })
  export class bannerComponent implements OnInit, OnChanges {
    index: string='*';
    showMobIndustry:boolean=false;
    openSideMenu: boolean =false;
    shoShare: boolean=false;
    pageUrl: string;
    clothingArray: any;
    industryName: string;
    pageTypes: string;
    womenCategories: any;
    mencategories: any;
    liked: any;
    pageId: string;
    accept: number = 22;
    topMoviesData: any;
    min: number;
    hours: number;
    userRated: any;
    movieCount: any;
    useroption: boolean = true;
    id: any;
    tempUser;
    countArray: any;
    
    userid: string;
    movieObject: any;
    public userRatedIndex: any; 
    @Output() showmore : EventEmitter<any> = new EventEmitter();
    @Output() showReview : EventEmitter<any> = new EventEmitter();
    @Output() filterDesignerData : EventEmitter<any> = new EventEmitter();
    @Output() filterIndustryData : EventEmitter<any> = new EventEmitter();
    @Input() profileData: any;
    @Input() industry: string;
    @Input() bannerType:string;
    @Input() brandContent:any;
    @Input() statusArray:any;
    @Input() pageType: string;
    @Input() topMovies: any;
    @Input() closeRatingPopup:any;
    @Input() pagecontent: any;
    @Input() userFollowedData: any;
    @Output() feedFilter : EventEmitter<any> = new EventEmitter();
    @Output() feedFilterFromBanner : EventEmitter<any> = new EventEmitter();
    @Output() celebFilterFromBanners : EventEmitter<any> = new EventEmitter();
    appStoreLinks : any = appStoreLinks;
    public today: number = Date.now();
    public localState = { value: '' };
    public isLoggedIn = String(localStorage.getItem("isLoggedIn"));
    // public isReleased = localStorage.getItem("isLoggedIn");
    public currentpage = this.activatedRoute.snapshot.url[0].path;
    public isMovie : boolean = false;
    public isMen : boolean = false;
    public isWomen : boolean = false;
    public isAmovie : boolean = false;
    public isStore : boolean = false;
    public isCelebrity : boolean = false;
    public isCeleb : boolean = false;
    public isIndustry : boolean = false;  
    public isFashion : boolean = false;
    public isShare : boolean = false;
    public isProfile : boolean = false;
    public isAuctions : boolean = false;
    public isDesigner : boolean = false;
    public isDesignerProfile : boolean = false;
    public Designer : boolean = false;
    public displayRatingPopup : boolean = false;
    public displayLoginPopup : boolean = false;
    public isEditPofile : boolean = false;
    public displayUploadPopup : boolean = false;
    public toggleEditProfileText : boolean = false;
    public celebName: string;
    public followedCelebData: any;
    public followedMovieData: any;
    public followedDesignerData: any;
    public followedBrandData: any;
    public designer;
    public profileDataObj;
    public categories;
    public userrating:any;
    public userName;
    public movies =[];
    public industryArray = [
      "tollywood",
      "bollywood",
      "mollywood",
      "kollywood",
      "sandalwood"
    ];
   
    private rating;
    private isFollow : boolean = false;
    private numOfFollowers;
    // public $: any;
    // public jQuery: any;
    
    constructor(
      // public appState: AppState,
      public router: Router,
      public activatedRoute: ActivatedRoute,
      public fashionService: FashionService,
      public likeService: LikeService,
      public movieService:MovieService,
      public share: ShareButtons,
      private metaService: MetaService,
      // public fb: FacebookService,
      // public param: UIParams,
    ) {
     
  
      console.log(activatedRoute.snapshot.url[0].path);    
    }
    
    public ngOnInit() {
    
   $(document).ready(function(){
    $('.feed-li').click(function(){
        $('.mob-celeb-feed').show();
      });
    $('.mob-Celeb-s-menu > ul > li:last-child').click(function(){
        $('.Mob-Shar').show();
      });
   });
        
    
      this.industryName=null;
      // this.loginWithFacebook();
      console.log("*********Social LINKS***********");
      console.log(this.pageType);
      

      // console.log('hello `banner-comments` component');
      if(this.topMovies)
      console.log(this.topMovies);
      if(this.statusArray)
        console.log(this.statusArray[0]);
      console.log(this.pagecontent);
      if(this.profileData)
      console.log(this.profileData);
      if(localStorage.getItem("name"))
      this.userName = localStorage.getItem("name");
      let userrating = {
        nameofmovie:"",
        rating:"",

      };
      // this.movies.push(userrating);
      this.pageUrl=this.router.url;
      var urlBreakup = this.router.url.split('/');
      console.log(urlBreakup);
      var page = urlBreakup[1];
      this.pageTypes=urlBreakup[1];
      this.pageId= urlBreakup[1];
      this.celebName = urlBreakup[2];
      if(this.statusArray)
      console.log(this.statusArray);
      this.getcategories();
       console.log(page);
       
      console.log(this.isLoggedIn);
      
      console.log(this.industry);

      this.tempUser = {
        name : localStorage.getItem("name"),
        username : localStorage.getItem("username"),
        profilePic : localStorage.getItem("profilePic")?localStorage.getItem("profilePic"):"http://www.hjakober.ch/wp-content/uploads/nobody_m_1024x1024.jpg"
      }

      switch (page) {
        case "movie":
          this.isMovie = true;
          break;
        case "celeb":
          this.isCeleb = true;
          break;
        case "designer":
          this.isDesignerProfile = true;
          break;
        case "industry":
          this.isIndustry = true;  
          break;
        case "menfashion":
          this.isFashion = true;
          this.isMen =true;
          this.checkFashionLike();
          break;
        case "womenfashion":
          this.isFashion = true;
          this.isWomen =true;
          this.checkFashionLike();
          break;
        case "celeb-store":
        this.industryName=localStorage.getItem('Industry');
        for(let i=0;i<this.industryArray.length;i++){
          if(this.industryName==this.industryArray[i]){
            var temp =this.industryArray[i];
            this.industryArray[i]=this.industryArray[4];
            this.industryArray[4]=temp;
            break;
          }
          console.log(i);
        }
          if(urlBreakup[2])
            this.isCeleb = true;
          else
            this.isFashion = true;
            this.isCelebrity = true;
            this.checkFashionLike();  
          break;
        case "movie-store":
        this.industryName=localStorage.getItem('Industry');
        for(let i=0;i<this.industryArray.length;i++){
          if(this.industryName==this.industryArray[i]){
            var temp =this.industryArray[i];
            this.industryArray[i]=this.industryArray[4];
            this.industryArray[4]=temp;
            break;
          }
          console.log(i);
        }
          if(urlBreakup[2])
            this.isMovie = true;
          else
            this.isFashion = true;
          this.isAmovie =true;
          this.checkFashionLike();
          break;
         case  "share-your-style":
         this.isShare = true;
         break;
         case  "designer-store":
         if(urlBreakup[2])
           this.isDesigner = true;
         else {
          this.isFashion = true;
          this.Designer = true;
          this.checkFashionLike();
         }
         break;
         case "auctions":
         this.isAuctions = true;
         this.checkFashionLike();
         break;
         case "user":
         this.isProfile = true;
         break;
         case  "editprofile":
           this.isEditPofile = true;
           break;
      }

      if((page == 'celeb' || page == 'movie' || page == 'designer' || page =='celeb-store') && this.profileData) {
        if(this.profileData.numOfLikes)
        // this.numOfFollowers = this.profileData.numOfLikes
        this.isFollowing(this.profileData.id);
      }
      if(page=='brand-store'){
        this.isFollowing(this.brandContent.id);
        this.id=this.brandContent.id;

      }

      
      

    }

  
    ngOnChanges(changes) {
      console.log(changes);
      

      if(changes.userFollowedData) {
          if(changes.userFollowedData.currentValue){
            this.getIndividualData(this.userFollowedData);
          }
      }

      if(changes.profileData) {
        
                if(changes.profileData.currentValue){
                    //  this.isLoggedIn = localStorage.getItem('isLoggedIn');
                    //  console.log(this.userid);
                        console.log(changes.profileData.currentValue);
                        if(changes.profileData.currentValue.name)
                        this.metaService.setTitle(' '+changes.profileData.currentValue.name);
                        if(changes.profileData.currentValue.title)
                        this.metaService.setTitle(' '+changes.profileData.currentValue.title);
                     
                        this.id=changes.profileData.currentValue.id;
                      this.userid= localStorage.getItem('username');
                      this.isFollowing(this.id);
                      var type = changes.profileData.currentValue.serviceName;
                      if(type == "celebrity")
                        type = "celeb";
                      this.profileDataObj = {
                        id : this.id,
                        name : changes.profileData.currentValue.name,
                        slug : changes.profileData.currentValue.slug,
                        profilePic : changes.profileData.currentValue.profilePic,
                        type : type
                      };
                        console.log(this.id);
                          this.movieWatchStatus('will'+this.id);
                          if(this.isLoggedIn=='true'){
                            let userdata={
                        
                        'entityId':'will'+this.id,
                        'userId': String(this.userid),
                        
                       
                      };
                      console.log(userdata);
                      this.sendUserWatchStatus(userdata);
                    
                        }
                        
                        
                      }
              }

    }

   
    checkFunction(data,data2){
      // this.accept=true;
      console.log(data);
      console.log(data2);
      if(data.length==0)
      return true;

    }
    onNavigate(url){
      console.log(url);
      window.open(url, "_blank");
    }
    selectIndustry(i){
      console.log(i);
      this.industryName=i;
      localStorage.setItem('Industry',i);
      for(let i=0;i<this.industryArray.length;i++){
        if(this.industryName==this.industryArray[i]){
          var temp =this.industryArray[i];
          this.industryArray[i]=this.industryArray[4];
          this.industryArray[4]=temp;
          break;
        }
        console.log(i);
      }
      this.filterIndustryData.next(this.pageId);

    }
    checkFull(data){
      console.log(data);
      if(data.length==1)
      return true;

    }
    getIndividualData(data) {  
        this.followedCelebData = {
          data : new Array(),
          type : "celeb"
        }
        this.followedMovieData = {
          data : new Array(),
          type : "movie"
        }
        this.followedDesignerData = {
          data : new Array(),
          type : "designer"
        }
        this.followedBrandData = {
          data : new Array(),
          type : "brand"
        }

      for(let i=0; i<data.length; i++) {
        if(data[i].entityObj){
        if(data[i].entityObj.type == "celeb")
          this.followedCelebData.data.push(data[i]);
        if(data[i].entityObj.type == "movie")
          this.followedMovieData.data.push(data[i]);
        if(data[i].entityObj.type == "designer")
          this.followedDesignerData.data.push(data[i]);
        if(data[i].entityObj.type == "brand")
          this.followedBrandData.data.push(data[i]);
      }
    }
    }
    showTime(time){
      if(time){
      this.hours=Math.floor(time/60);
      this.min=time%60;
      return true;
      }
      else
        return false;
    }
    openMenu(){
      this.openSideMenu=!this.openSideMenu;
    }
    sendUserStatus(id){
      var logged=localStorage.getItem('isLoggedIn');
      if(logged=='true'){
      this.userRated=true;
     
      let userdata={
        
        'entityId':'will'+this.id,
        'userId': String(this.userid),
        'type':String(id),
        
       
      };
      console.log(userdata);
      this.sendWatchStatus(userdata);
     
    }
    else{
      this.loginPopup();
    }

    }
    show() {
      this.showmore.next();
      
    }
    showReviewCard() {
      this.showReview.next();
      
    }
    filterDesignerPage(item){
      console.log(item);
      this.filterDesignerData.next(item);
    }

    likeMovie(id,i){  
      
      // this.statusArray[i].Items.length=1;
      var login=String(localStorage.getItem('isLoggedIn'));
      if(login=='true'){
        if(this.accept!=i){
          this.accept=i;
        this.statusArray[i].totalCount=this.statusArray[i].totalCount+1;
      let followBody = {
        userId : localStorage.getItem("username"),
        entityId : id,
        type : "follow"
      };
      this.likeService.like(followBody).subscribe(
          res => {
              console.log(res);
              for(let i=0;i<this.topMovies.length;i++){
                console.log(this.topMovies[i].id);
              this.checkStatus(this.topMovies[i].id,i);
   
              }
              console.log(this.statusArray);
             
            },
            error => {
              console.log("Error in posting follow");
            }
      );
    }
    }
    else{
      this.loginPopup();

    }
    }
    likeFashion(){


      var login=String(localStorage.getItem('isLoggedIn'));
      if(login=='true'){
        this.liked=1;
        
      let followBody = {
        userId : localStorage.getItem("username"),
        entityId : this.pageId,
        // entityObj : this.profileDataObj,
        type : "follow"
      };
      this.likeService.like(followBody).subscribe(
          res => {
              console.log(res);
              
             
            },
            error => {
              console.log("Error in posting follow");
            }
      );
    
    }
    else{
      this.loginPopup();

    }

    }
    checkFashionLike(){
      let isFollowBody = {
        userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
        entityId :this.pageId,
        type : "follow"
      };
        this.likeService.isLiked(isFollowBody).subscribe(
            res => {
                console.log(res);
              if(res.data) {
                this.liked=res.data.Count;
                
              }
              },
              error => {
                console.log("Error");
              }
        );

    }
    showShare(){
      this.shoShare=!this.shoShare;
    }
    checkStatus(id,i){
      let isFollowBody = {
        userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
        entityId : id,
        type : "follow"
      };
        this.likeService.isLiked(isFollowBody).subscribe(
            res => {
                console.log(res);
              if(res.data) {
                this.statusArray[i]=res;
                
                console.log(res.totalCount);
                
              }
              },
              error => {
                console.log("Error");
              }
        );

    }

  filterFromBanner(filter) {
    if(filter == "edit-profile") {
      this.toggleEditProfileText = !this.toggleEditProfileText;
    }
    this.feedFilterFromBanner.emit(filter);
  }
  filterFromBanners(filter){

          console.log(filter);
  
        this.celebFilterFromBanners.emit(filter);
    
     }

    
    showDropdown(){
      this.showMobIndustry=!this.showMobIndustry;
    }
    goToIndustry(type){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['industry/' + type]));

    }
    eventEmitt(type){
      this.index=type;
      this.openSideMenu=!this.openSideMenu;
    console.log(type);
    this.feedFilter.next(type);
    }
   
    sendWatchStatus(userdata){
      this.movieService.sendWatchStatus(userdata).subscribe(
        res => {
            console.log(res);
            let userStatus={
              
              'entityId':'will'+this.id,
              'userId': String(this.userid),
             };
            this.sendUserWatchStatus(userStatus);
            
            
            
          },
          error => {
            console.log("Error in posting orders");
          }
    );
    
    }
    movieWatchStatus(userdata){
      console.log(userdata);
      this.movieService.movieStatus(userdata).subscribe(
        res => {
            console.log(res);
            this.movieCount=res;
            
            
          },
          error => {
            console.log("Error in posting movie data");
          }
    );

    }
    sendUserWatchStatus(userdata){
      console.log(userdata);
      this.movieService.UsermovieStatus(userdata).subscribe(
        res => {
            console.log(res);
            if(res.result)
            this.userRated=res.result.Items[0];
            console.log(this.userRated);
           
            
          },
          error => {
            console.log("Error in posting movie data");
          }
    );
    this.movieWatchStatus('will'+this.id);
    }

    isFollowing(id) {
    let isFollowBody = {
      userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
      entityId : id,
      type : "follow"
    };
      this.likeService.isLiked(isFollowBody).subscribe(
          res => {
              console.log(res);
            if(res.data) {
              if(res.data.Count == 0) {
                this.isFollow = false;
              } else {
                this.isFollow = true;
              }
              this.numOfFollowers = res.totalCount;
            }
            },
            error => {
              console.log("Error");
            }
      );
    }

    public gotodatefunction(date2,date1){
      return (new Date(date2) > new Date(date1));
    }

    follow() {
      if(this.isFollow) {
        this.numOfFollowers--;
      } else {
        this.numOfFollowers++;
      }
      this.isFollow = !this.isFollow;
      let followBody = {
        userId : localStorage.getItem("username"),
        entityId : this.id,
        entityObj : this.profileDataObj,
        type : "follow"
      };
      this.likeService.like(followBody).subscribe(
          res => {
              console.log(res);
            },
            error => {
              console.log("Error in posting follow");
            }
      );
    }


  



   private submitingRatingPopup(userrating)
   {
     if(userrating){
      
      console.log(userrating.rating);
      this.movies[this.userRatedIndex]=userrating;
       console.log(this.movies);
      this.displayRatingPopup = false;
     }
      else
      this.displayRatingPopup = false;
     }
    
    loginPopup() {
      // this.displayLoginPopup = true;
      let urlBreakup = this.router.url.split('/');
      urlBreakup.splice(0, 1);
      let redirect = urlBreakup.join('/');
      localStorage.setItem("preLogin",redirect);
      // this.router.navigate(['login']);  
      this.displayLoginPopup = true; 
    }
    closeLoginPopup () {
      console.log("event emmitted");
      this.displayLoginPopup = false; 
    }
   
    
    goToSeeAll(subPage,category,page,items) { 
      console.log(items);
      localStorage.removeItem('subCatagori'); 
      localStorage.setItem("subPage",subPage);
      localStorage.setItem("category",category);
      localStorage.setItem("page",page);
      localStorage.setItem("items",items);
      this.router.navigate([page,subPage,category]);  
    }
    goToSeeAllSubPage(subcategori,subPage,category,page,items) {  
      console.log(items);
      if(subcategori=='Clothing')
      localStorage.setItem('subItem','Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
      if(subcategori=='Accessories')
      localStorage.setItem('subItem','Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');
      if(subcategori=='Eyewear')
      localStorage.setItem('subItem','Eyewear,Eyeglasses,Sunglasses');
      localStorage.setItem("subCatagori",subcategori);
      localStorage.setItem("subPage",subPage);
      localStorage.setItem("category",category);
      localStorage.setItem("page",page);
      localStorage.setItem("items",items);
      this.router.navigate([page,subcategori,subPage,category]);  
    }
    goToSubPage(subcategori,subPage,page,items){
      console.log(items);
      if(subcategori=='Clothing')
        localStorage.setItem('subItem','Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
        if(subcategori=='Accessories')
        localStorage.setItem('subItem','Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');
        if(subcategori=='Eyewear')
        localStorage.setItem('subItem','Eyewear,Eyeglasses,Sunglasses');
        localStorage.setItem("subCatagori",subcategori);
        localStorage.setItem("subPage",subPage);
        localStorage.removeItem("category");
        localStorage.setItem("page",page);
        localStorage.setItem("items",items);
        this.router.navigate([page,subcategori,subPage]);  
    }
 
    goToSeeAllmenAccessories(subcategori,subPage,page,items) {  
      console.log(items);
      localStorage.setItem('subItem','Accessories,Bags,Belts,Caps & Hats,Jewellery,Mufflers & Scarves,Tech Accessories,Ties & Cuf,Watches');
      localStorage.setItem("subCatagori",subcategori);
      localStorage.setItem("subPage",subPage);
      localStorage.removeItem("category");
      localStorage.setItem("page",page);
      localStorage.setItem("items",items);
      this.router.navigate([page,subcategori,subPage]);  
    }

    uploadPopup () {
      this.displayUploadPopup = true;
    }

    closeUploadPopup () {
      this.displayUploadPopup = false;
    }

    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
   

    getcategories(){ 
      this.fashionService.getCategories().subscribe(
        res => {
          console.log("THIS IS Categories DATA");
          console.log(res);
          if(res){
              this.mencategories = res.Items[0].menFashion;
              this.womenCategories= res.Items[0].womenFashion;
          }
          this.clothingArray=this.womenCategories[0];
          console.log(this.clothingArray);
        },
        error => {
          console.log("Error in getting categories");
        }
      );
    }

      
  }