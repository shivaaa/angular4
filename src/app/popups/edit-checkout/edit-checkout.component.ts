import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output
} from '@angular/core';

// import { AppState } from '../../app.service';
import { ProductService } from "../../models/product/product.service";

@Component({

  selector: 'edit-checkout',
  providers: [],
  styleUrls: ['./edit-checkout.component.css'],
  templateUrl: './edit-checkout.component.html'
})
export class EditCheckoutComponent implements OnInit {
  public productcolor: any;
  public productquant: any;
  public productsize: any;
  public sendObject: any;

  public close: boolean = true;
  public localState = { value: '' };
  public get: boolean = false;
  @Input() sendObjects: any;
  @Input() productOrder: any;
  @Input() item: any;
  @Input() id: any;
  @Output() changingOrder: EventEmitter<any> = new EventEmitter();
  constructor(
    // public appState: AppState,
    public productService: ProductService,
  ) { }

  public ngOnInit() {
    console.log('hello `get-the-app` component');

    if (this.sendObjects) {
      console.log(this.sendObjects);
      if(this.productOrder)
      console.log(this.productOrder);
      this.productcolor = this.sendObjects.color;
      this.productsize = this.sendObjects.size;
      console.log(this.sendObject);
      this.getProduct(this.sendObjects.productSlug);
    }
  }

  closePopup() {
    this.changingOrder.next();

  }
  subcounter(number) {

    if (number != 1) {
      this.sendObjects.quantity = +this.sendObjects.quantity - 1;

    }
  }
  placeOrder() {


    if (this.sendObjects) {
      if (this.sendObject.isDiscount == false)
        this.productOrder.product[this.item].price = String(this.sendObject.price);
      if (this.sendObject.isDiscount == true)
        this.productOrder.product[this.item].price = String(this.sendObject.discountObj.discPrice);
      this.productOrder.product[this.item].size = this.productsize;
      this.productOrder.product[this.item].color = this.productcolor;
      this.updateOrder(this.productOrder, this.id);
      console.log(this.productOrder);
    }

  }
  removeItem() {
    this.productOrder.product.splice(this.item, 1);
    this.updateOrder(this.productOrder, this.id);
    this.changingOrder.next();
    // this.list = this.list.filter(item => item.id !== id);
  }
  add() {
    this.sendObjects.quantity = +this.sendObjects.quantity + 1;
  }
  updateOrder(order, id) {
    this.productService.updateOrders(order, id).subscribe(
      res => {
        console.log("updateOrder service");
        console.log(res);
        if (res) {
          console.log(res);
          if (res.statusCode == 200) {
            let changeOdd = {
              quantity: this.sendObjects.quantity,
              size: this.productsize,
              color: this.productcolor,
            }

            this.changingOrder.emit(changeOdd);
          }


        }

      },
      error => {
        console.log("error updateOrder product");
      }
    );

  }


  getProduct(slug) {
    this.productService.getProductBySlug(slug).subscribe(
      res => {
        console.log("Product service");
        console.log(res);
        if (res) {
          this.sendObject = res.hits.hits[0]._source;

        }

      },
      error => {
        console.log("error getting product");
      }
    );

    //  this.topMovies = this.topMoviesMock;
    //this.product = this.productMock;
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
