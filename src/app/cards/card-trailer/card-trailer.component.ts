import { Component, OnInit, Input} from '@angular/core';
  
  // import { AppState } from '../../app.service';
import { Router } from '@angular/router';
  
  @Component({
    
    selector: 'card-trailer', 
    providers: [ ],
    styleUrls: [ './card-trailer.component.css' ],
    templateUrl: './card-trailer.component.html'
  })
  export class cardtrailerComponent implements OnInit {
    shareProfilePic: any;
    shareSlug: any;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    isMiniCard: any;
    public localState = { value: '' };
    @Input() content : any;
    @Input() miniCard : any;
    public contentData : any;
    public assnData : any;
    public cardId;
    public cardSlug :any;
    public today: number = Date.now();

    constructor(
      // public appState: AppState,
      public router : Router
    ) {}
  
    public ngOnInit() {
      var urlBreakup = this.router.url.split('/');
      this.cardSlug=urlBreakup[2];
      console.log('hello `card-trailer` component');
      console.log("FORM TRAILER COMPONENT");
      console.log(this.content);
      this.contentData = this.content;
      this.createdAt=this.content.createdAt;
      this.assnData = this.content.assnData;
      if(this.content) {
        this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
      //  console.log(this.content.assets.image.value); 
      //  console.log("****&&&&&&&&&%Y$YJTNBTJI^&UYTTT"); 
      }
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='s';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
    }
    gotocontent(){
      if(this.cardSlug !=this.contentData.slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }
  