import {
    Component,
    OnInit
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  import { Router } from "@angular/router";
  import { FashionService } from "../../models/fashion/fashion.service";
  import { LikeService } from '../../models/like/like.service';
  import { MovieService } from '../../models/movie/movie.service';
  import { MetaService } from '@ngx-meta/core';
declare var $:any;

  @Component({
    
    selector: 'userprofile', 
    providers: [ ],
    styleUrls: [ './userprofile.component.css' ],
    templateUrl: './userprofile.component.html'
  })
  export class UserProfileComponent implements OnInit {
    
    public viewOnly: string='Profile';
    public localState = { value: '' };
    public pageType:any;
    public allContentData;
    public filteredData;
    public userName;
    public wishListItems;
    public followedCelebArray;
    public castCrewArray;
    public userFollowedData;
    public imageUploaded : boolean = false;
    public isUserProfile : boolean = false;
    public isFilteredData : boolean = false;
  
    constructor(
      // public appState: AppState,
      public router: Router,
      public fashion : FashionService,
      public likeService: LikeService,
      public movieService: MovieService,
      private metaService: MetaService,

    ) {}
  
    public ngOnInit() {

    $(document).ready(function() {
    $('.fs-Preferencesfrm .check').click(function(){
    $(this).toggleClass('active');
    });
    });

      window.scrollTo(0,0);
      console.log('hello `user-profile` component');
      var urlBreakup = this.router.url.split("/");
      this.metaService.setTitle(' '+urlBreakup[1]);
      this.userName = localStorage.getItem("name");
      this.pageType = urlBreakup[1];
      this.getContentForSYS();
      this.getUserFollowedData(0);
    }

    getUserPageFeed() {
      this.isFilteredData = false;
    }

    feedFilterFromBanner(dataObj) {
      if(dataObj == 'edit-profile') {
        this.isUserProfile = !this.isUserProfile;
      } else {
        console.log(dataObj);
        this.isFilteredData = true;
        this.filteredData = dataObj.data;
      }
    }
    setView(viewType){
   this.viewOnly=viewType;
    }

    getUserFollowedData(num) {
      let followBody = {
        userId : localStorage.getItem("username"),
        type : "follow"
      };
      this.likeService.getUserPosts(followBody).subscribe(
          res => {
              console.log(res);
              this.userFollowedData = res.result.Items;
              // if(num == 0) {
                let celebArray = [];
                let movieArray = [];
                for(let i=0; i<this.userFollowedData.length; i++) {
                  if(this.userFollowedData[i].entityObj)
                  if(this.userFollowedData[i].entityObj.type == "celeb") {
                    celebArray.push(this.userFollowedData[i].entityObj);
                  }
                  if(this.userFollowedData[i].entityObj)
                  if(this.userFollowedData[i].entityObj.type == "movie") {
                    movieArray.push(this.userFollowedData[i].entityObj.slug);
                  }
                }
                this.getMovieObjects(movieArray);
                this.followedCelebArray = celebArray;
              // }
            },
            error => {
              console.log("Error");
            }
      );
    }


    changePhoto(fileInput: any) {
        let AWSservice = (<any>window).AWS;
        AWSservice.config.accessKeyId = 'AKIAJT7SFJQEJTU5AOKA';
        AWSservice.config.secretAccessKey = '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';
        let bucket = new AWSservice.S3({params: {Bucket : 'bucket-for-image-test'}});
        console.log(fileInput.target.files);
        for(let i=0; i<fileInput.target.files.length; i++) {
          let file = fileInput.target.files[i];
          let params = {Key : file.name, Body : file};
          var _self = this;
          bucket.upload(params, function(err, res){
            console.log('error', err);
            console.log('response', res);
            if(res) {
              localStorage.setItem("profilePic",res.Location);
              _self.imageUploaded = true;
            }
          })
        }
    }

    save() {
      localStorage.setItem("name",this.userName);
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['user']));

    }

    emitCartItems(items) {
      this.wishListItems = items;
    }

    getContentForSYS() {
      this.fashion.getContentForSYS().subscribe(
        res=> {
          if(res){
            this.allContentData = this.reverseItems(res.Items);
            console.log("************All SYS Content Data***************");
            console.log(this.allContentData);
            }
        },
        error => {
          console.log("error");
        }
      );
    }

    getMovieObjects(array) {
      var castCrewArray = new Array();
      for(let i=0; i<array.length; i++) {
        this.movieService.getMovieBySlug(array[i]).subscribe(
          res=> {
            if(res.Items[0].cast) {
              castCrewArray = castCrewArray.concat(res.Items[0].cast);
              console.log(castCrewArray);
            }
            if(res.Items[0].crew) {
              castCrewArray = castCrewArray.concat(res.Items[0].crew)
              console.log(castCrewArray);              
            }
            // console.log(i);    
            if(i == array.length-1) {
              // console.log(i);    
              // console.log(castCrewArray);    
              this.getCastCrewForRSB(castCrewArray);
            }
          },
          error => {
            console.log("error");
          }
        );
      }
    }

    getCastCrewForRSB(array) {
      console.log(array);
      let tempArray = new Array();
      let tempArray2 = new Array();
      for(let i=0; i<array.length; i++) {
        if(tempArray.indexOf(array[i]) == -1) {
          tempArray.push(array[i]);
        }
      }
      for(let i=0; i<tempArray.length; i++) {
        let obj = {
          id : tempArray[i].id,
          name : tempArray[i].name,
          profilePic : tempArray[i].profilePic,
          slug : tempArray[i].slug,
          type : "celeb"
        }
        tempArray2.push(obj);
      }
      console.log(tempArray2)
      console.log(this.followedCelebArray)
      for(let i=0; i<this.followedCelebArray.length; i++) {
        for(let j=0; j<tempArray2.length; j++){
          if (this.followedCelebArray[i].slug === tempArray2[j].slug) {
              tempArray2.splice(j, 1);
          }
        }
      }
      console.log(tempArray2)
      this.castCrewArray = tempArray2;
    }

    follow(celeb,index) {
      let likeBody = {
        userId : localStorage.getItem("username"),
        entityId : celeb.id,
        entityObj : celeb,
        type : "follow"
      };
      this.likeService.like(likeBody).subscribe(
          res => {
              console.log(res);
              this.castCrewArray.splice(index, 1);
              this.getUserFollowedData(1);
            },
            error => {
              console.log("Error in posting follow");
            }
      );
    }

    getPhotos() {
      // let isGalleryBody = {
      //   celebSlug : this.slug,
      // };
      // this.galleryService.gallery(isGalleryBody).subscribe(
      //     res => {
      //         console.log(res);
      //         // this.galleryCounter = res.totalCount;
      //         this.galleries = res.data;
      //       },
      //       error => {
      //         console.log("Error");
      //       }
      //   );
    }

  reverseItems(array) {
    let tempArray = new Array(array.length);
    for(let i=0; i<array.length; i++) {
      tempArray[array.length-i-1] = array[i];
    }
    return tempArray;
  }

    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }
  