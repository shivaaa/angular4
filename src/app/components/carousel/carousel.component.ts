import {
  Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

// import { AppState } from '../../app.service';
import { Router } from '@angular/router';
import { CarouselService } from '../../models/carousel/carousel.service';
import {DomSanitizer} from '@angular/platform-browser'
import { ProductService } from "../../models/product/product.service";

@Component({
  
  selector: 'carousel', 
  providers: [ ],
  styleUrls: [ './carousel.component.css' ],
  templateUrl: './carousel.component.html'
})
export class CarouselComponent implements OnInit, OnChanges {
  disable: boolean=false;
  dataArray = [];
  public productArray = [];
  public title: any;
  public videourl: any;
  public popupVideo: boolean;
  public popup: boolean;
  public popupViewAll: boolean;
  public page: string;
  
  public localState = { value: '' };

  @Input() carouselDatas : any;
  @Input() carouselData : any;
  @Input() brandData : any;
  @Input() carouselType : any;
  @Input() type : string;
  @Input() data : any;
  // @Input() latestObsessions : any;
  @Input() stealThatStyleData : any;
  // @Input() stealThatStyle: any;
  @Input() design : any;
  @Input() carouselsData: any;
  @Output() productPivot : EventEmitter<any> = new EventEmitter();
  @Output() seemoreDesigners : EventEmitter<any> = new EventEmitter();
  //@Input() carouselType : any;

  public mainRoute:any;
  public fourArray=[1,2,3,4];
  public threeArray=[1,2,3];
  public activeArray = [];
  public inactiveArray = [];
  public inactiveArrays = [];
  public carouselArray = [];
  public arrayLen : number;
  public miniCard : boolean = true;
  // public carouselData;
  public videoData;
  public shopByVideoData;
  public cData;

  // player: YT.Player;
  private id: string;
  private height: number = 460;
  private width: any = '100%';
 
   

  constructor(
    // public appState: AppState,
    public router : Router,
    public carouselService : CarouselService,
    public sanitizer: DomSanitizer,
    public productService:ProductService,
  ) {}

  public ngOnInit() {
    var urlBreakup = this.router.url.split("/");
    this.page=urlBreakup[1];
  // this.contentSlug = urlBreakup[2];
    console.log('hello `carousel` component');
    this.mainRoute = localStorage.getItem("mainRoute");
    if(this.type == "stealThatStyle" || this.type == "latestObsessions") {
      this.arrayLen = 4;
    } else{
      this.arrayLen = 3;
    }
    // this.getAllDesigners();
  //  if(this.carouselType == "shopByVideos"){
  //   this.getShopByVideo();
  //  }
  if(this.brandData)
    console.log(this.brandData);
    console.log(this.carouselType);
    console.log("***********Carousel of looks*****************");
    console.log(this.carouselsData);
    this.cData = this.carouselsData;
   // console.log(this.cData.productProfilePic);
  }

  ngOnChanges(changes) {
    console.log(changes);
    if(this.data) {
      console.log("***********Carousel NEWS || TRAILERS || GALLERY*****************")
      this.getCarouselArray(this.data, this.arrayLen);
      for(let i=0;i<this.data.length;i++){
        this.dataArray.push(this.data[i]._source)
      }
    }
    if(this.carouselData) {
      console.log("***********carouselData*****************")
      this.getDesignerArray(this.carouselData, 3);
    }
    if(this.stealThatStyleData) {
      console.log("***********stealThatStyleData*****************")
      this.getCarouselArray(this.stealThatStyleData, 4);
    }
    // if(this.productArray){
    //   this.getDesignerArray(this.productArray, 3);

    // }
  }
 
  // getAllDesigners(){  
  //   this.carouselService.getCarouselBySlug().subscribe(
  //     res => {
  //       console.log(res);
  //       if(res){
  //         this.carouselData = res.Items;  
  //         console.log("***********Designere****************");
  //         console.log(this.carouselData);
  //       }
  //     },
  //     error => {
  //       console.log("Error in carouselDesiner");
  //     }

  //   );
  // }
  getShopByVideo(){  
    this.carouselService.getShopByVideo().subscribe(
      res => {
        console.log(res);
        if(res){
         // this.shopByVideoData = res.Items;
          this.carouselDatas =res.Items;
                  // this.videoData = res;
          console.log("***********SHOP BY VIDEO****************");
          console.log(this.carouselDatas);
          //console.log(this.carouselType);
          // this.id=this.videoData.videoUrl;
        }
      },
      error => {
        console.log("Error in carouselVideo");
      }
    );
  }

  getCarouselArray(data, num) { 
    var tempData = [];
    for(let i=0; i<data.length; i++) {
      if(data[i]) {
        if(data[i]._source) {
          if(data[i]._source.slug) {
            if(data[i]._source.slug.split('-')[0] != "temp" && data[i]._source.slug.split('-')[0] != "test") {
              tempData.push(data[i]);
            }
          }
        }
      }
    }
    data = tempData;
    for(let i=0; i<num; i++) {
      if(data[i]) {
        console.log(data[i]._source);
        this.activeArray.push(data[i]._source);
      }
    }
    if(data.length > num) {
      var inactiveArrayCount = 0;
      for(let i=num; i<data.length; i++) {
        if(i%num == 0) {
          this.inactiveArrays.push(new Array());
          inactiveArrayCount++;
        }
        this.inactiveArrays[Number(inactiveArrayCount-1)].push(data[i]._source);
      }
    }
    console.log(this.activeArray);
    console.log(this.inactiveArrays);
    this.inactiveArray = this.inactiveArrays[0];
  }

  getDesignerArray(data, num) {
    for(let i=0; i<num; i++) {
      if(data[i]) {
        console.log(data[i]);
        this.activeArray.push(data[i]);
      }
    }
    if(data.length > num) {
      var inactiveArrayCount = 0;
      for(let i=num; i<data.length; i++) {
        if(i%num == 0) {
          this.inactiveArrays.push(new Array());
          inactiveArrayCount++;
        }
        this.inactiveArrays[Number(inactiveArrayCount-1)].push(data[i]);
      }
    }
    console.log(this.activeArray);
    console.log(this.inactiveArrays);
    this.inactiveArray = this.inactiveArrays[0];
  }
  goToSeeAll(first,second,third) {
     localStorage.removeItem('page');
     localStorage.removeItem('subPage');
     localStorage.removeItem('category');
    this.router.navigate([first,second,third]);  
  }
  goToAllContent(content,type){
    localStorage.removeItem('page');
    localStorage.removeItem('subPage');
    localStorage.removeItem('category');
    var pageRedirect=localStorage.getItem('rootRoute');
    this.router.navigate([content,type,pageRedirect]);  
  }
  goToSeeDesigner(page,celebrity) {
    localStorage.setItem("page",page);
    localStorage.setItem("celebrity",celebrity);
    this.router.navigate([page,celebrity]);  
   
     
  }
  goToProduct(product,item){
    localStorage.setItem("product",product);
    localStorage.setItem("item",item);
    this.router.navigate([product,item]);  

  }

  goToLooks(pivot) {
    
    this.productPivot.emit(pivot);
  }
  seeDesigners(){
    this.seemoreDesigners.emit();
  }
  

  displayPopup(item) {
    window.scrollTo(0,0);
    console.log(item);
    this.productArray = item.products;
    this.getEmbedVideoUrl(item.videoUrl);
    this.title=item.title;
    this.popupViewAll = false;
    this.popup = true;
    this.popupVideo = true;
    console.log("popup displayed");
  }
  getEmbedVideoUrl(url) {
    if(url.split('/')[2] == "www.youtube.com") {
      if(url.split('/')[3] == "embed") {
        this.videourl = url;
      } else {
        this.videourl = "https://www.youtube.com/embed/" + url.split('/')[3].split('=')[1];
      }
    } else {
      this.videourl = "https://www.youtube.com/embed/" + url.split('/')[3];
    }
    console.log(this.videourl);
  }

  gotoBrand(slug){
    this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
    this.router.navigate(['brand-store/' + slug]));
  }

  viewAll() {
    window.scrollTo(0,0);
    this.popupVideo = false;
    this.popupViewAll = true;
  }

  backToVideo() {
    this.popupViewAll = false;
    this.popupVideo = true;
  }

  closePopup() {
    this.backToVideo();
    this.popup = false;
    this.productArray=[];
  }
  disablePopup(){
    this.disable=true;
    
  }
  closeDisable(){
    if(this.disable == false){
      this.backToVideo();
      this.popup = false;
      this.productArray=[];

    }

  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
