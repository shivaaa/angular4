import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input
} from '@angular/core';

// import { AppState } from '../../app.service';
import { UserService } from "../../models/user/user.service";
import { ActivatedRoute, Router } from "@angular/router";

import { AuthService } from "angular4-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
import { SocialUser } from "angular4-social-login";
@Component({

  selector: 'login',
  providers: [],
  styleUrls: ['./login.component.css'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public localState = { value: '' };

  public mainLogin: boolean = true;
  public next: boolean = false;
  public phone: boolean = false;
  public email: boolean = false;
  public semail: boolean = false;
  public otp123: boolean = false;
  public afterOtp: boolean = false;
  public signUp: boolean = false;
  public login: boolean = false;
  public signPhone: boolean = false;
  public forgetPhoneNumber: boolean = false;
  public newPassword: boolean = false;
  public forgetEmailPassword: boolean = false;
  public resetEmailLink: boolean = false;
  public forgetOtp: boolean = false;
  public otpSent: boolean = false;
  public displayVerify: boolean = false;

  public signUpErrorText: string = ""

  private socialUser: SocialUser;
  public user: any;
  public username: any;
  public rePassword: any;
  public registeredId: any;
  public registrationType: any;
  // public firstName : any;
  // public lastName : any;
  // public password : any;
  // public e_mail : any;
  // public phoneNumber : any;
 
  @Output() closeLoginPopup: EventEmitter<any> = new EventEmitter();
  @Input() logType: any;
  constructor(
   // public appState: AppState,
    public userService: UserService,
    private authService: AuthService,
    public router: Router,
  ) { }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }


  public ngOnInit() {
    if(this.logType)
    console.log(this.logType);
    if(this.logType=='signup'){
      this.signUp=true;
    }
    this.user = this.createUser();
    console.log('hello `login` component');
    let urlBreakup = this.router.url.split('/');
    urlBreakup.splice(0, 1);
    let redirect = urlBreakup.join('/');
    localStorage.setItem("preLogin",redirect);

    this.authService.authState.subscribe((user) => {
      this.socialUser = user;
      console.log(this.socialUser);
      if (this.socialUser) {
        if (this.socialUser.provider == "GOOGLE") {
          localStorage.setItem("isLoggedIn", "true");
          localStorage.setItem("profilePic", this.socialUser.photoUrl);
          localStorage.setItem("name", this.socialUser.name);
          localStorage.setItem("userObj", JSON.stringify(this.socialUser));
          this.createSocialUser(this.socialUser, 'google');
          this.signUpErrorText = "Getting data, please wait...";
        }
        // if(this.socialUser.provider =="Facebook")
        else {
          localStorage.setItem("isLoggedIn", "true");
          localStorage.setItem("profilePic", this.socialUser.photoUrl);
          localStorage.setItem("name", this.socialUser.firstName + ' ' + this.socialUser.lastName);
          localStorage.setItem("userObj", JSON.stringify(this.socialUser));
          this.createSocialUser(this.socialUser, 'facebook');
          this.signUpErrorText = "Getting data, please wait...";
        }
      }
    });
  }

  createSocialUser(user, loginType) {


    if (loginType == 'facebook') {
      let registerUser = {
        email: user.email,
        mobile: undefined,
        firstname: user.firstName,
        lastname: user.lastName,
        profilePic: user.photoUrl,
        status: "active",
        provider: "Facebook"
      };
      this.userService.socialLogin(registerUser).subscribe(
        res => {
          console.log(res);

          if (res.statusCode == 400)
            this.signUpErrorText = res.message;
          else {
            localStorage.setItem("username", res.id);
            let redirectPages = localStorage.getItem("preLogin");
            this.router.navigateByUrl('/dummy', { skipLocationChange: true }).then(() =>
              this.router.navigate([redirectPages]));
          }
        },
        error => {
          console.log("Error");
        }
      );
    }
    if (loginType == 'google') {
      let registerUser = {
        email: user.email,
        mobile: undefined,
        firstname: user.name,
        lastname: user.name,
        profilePic: user.photoUrl,
        status: "active",
        provider: user.provider,
      };
      this.userService.socialLogin(registerUser).subscribe(
        res => {
          console.log(res);

          if (res.statusCode == 400)
            this.signUpErrorText = res.message;
          else {
            localStorage.setItem("username", res.id);
            let redirectPages = localStorage.getItem("preLogin");
            this.router.navigateByUrl('/dummy', { skipLocationChange: true }).then(() =>
              this.router.navigate([redirectPages]));
          }
        },
        error => {
          console.log("Error");
        }
      );
    }



  }

  createUser() {
    return {
      profilePic: "",
      coverPic: "",
      email: "",
      mobile: "",
      username: "",
      firstname: "",
      lastname: "",
      mobileVerify: "",
      emailVerify: "",
      otp: "",
      pwd: "",
      role: "",
      address: {}
    };
  }

  userLogin(type) {
    let loginUser = {
      username: this.user.mobile ? this.user.mobile : this.user.email,
      password: this.user.pwd
    };

    if (this.user.email) {
      if (!this.validateEmail(this.user.email)) {
        this.signUpErrorText = "Please enter a valid email";
        return;
      }
    } else {
      if (!this.phonenumber(this.user.mobile)) {
        this.signUpErrorText = "Please enter a valid phone number";
        return;
      }
    }
    this.userService.login(loginUser).subscribe(
      res => {
        console.log(res);
        if (res.message == "No results found") {
          this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
        } else if (res.message == "No User found") {
          this.signUpErrorText = res.message + " with this " + type;
        } else if (res.message == "Authentication failed.") {
          this.signUpErrorText = res.message;
        } else if (res.message == "Your OTP Authentication is pending") {
          this.signUpErrorText = res.message;
        } else {
          this.storeUserData(res.data);
        }
      },
      error => {
        console.log("Error");
      }
    );
  }

  storeUserData(user) {
    localStorage.setItem("isLoggedIn", "true");
    localStorage.setItem("username", user.id);

    localStorage.setItem("name", user.firstname + ' ' + user.lastname);
    localStorage.setItem("userObj", JSON.stringify(user));
    this.closePopup();
    let redirectPage = localStorage.getItem("preLogin");
    console.log(redirectPage);
    if (redirectPage) {
      this.router.navigateByUrl('/dummy', { skipLocationChange: true }).then(() =>
        this.router.navigate([redirectPage]));
    } else {
      let industry;
      if (!localStorage.getItem("Industry")) {
        localStorage.setItem("Industry", "tollywood");
      }
      this.router.navigateByUrl('/dummy', { skipLocationChange: true }).then(() =>
        this.router.navigate(['home']));
    }
  }

  userSignUp(type) {
    let checkType;
    let checkItem;
    let registerUser = {
      email: undefined,
      mobile: undefined,
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      pwd: this.user.pwd
    };
    if (type == 'email') {
      this.user.mobile = "";
      checkItem = this.user.email;
      registerUser.email = this.user.email;
      this.username = this.user.email;
      if (!this.validateEmail(this.user.email)) {
        this.signUpErrorText = "Please enter a valid email";
        return;
      }
      checkType = 'checkEmail';
      this.registrationType = "email";
    } else if (type == 'phone') {
      this.user.email = "";
      checkItem = this.user.mobile;
      registerUser.mobile = this.user.mobile;
      this.username = this.user.mobile;
      checkType = 'checkMobile';
      this.registrationType = "mobile";
      if (!this.phonenumber(this.user.mobile)) {
        this.signUpErrorText = "Please enter a valid phone number";
        return;
      }
    }

    console.log(this.user);

    if (this.user.pwd != this.rePassword) {
      this.signUpErrorText = "Your passwords do not match.";
    } else {
      if (!this.passwordValidation(this.rePassword)) {
        this.signUpErrorText = "Password must contain atleast a number, a letter and length of 8";
        return;
      }
      this.signUpErrorText = "";
      this.userService.checkEmailOrPhone(checkType, checkItem).subscribe(
        res => {
          console.log(res);
          if (res.Count != 0) {
            if (type == 'email')
              this.signUpErrorText = "There is already an account with this email address";
            else if (type == 'phone')
              this.signUpErrorText = "There is already an account with this phone number";
          } else {
            this.userService.registration(registerUser).subscribe(
              res => {
                console.log(res);
                if (res.statusCode == 200) {
                  this.registeredId = res.id;
                  localStorage.setItem("registeredId", this.registeredId);
                  this.otpSent = res.otpStatus;
                  if (!this.otpSent)
                    this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
                  else
                    this.goToOtp();
                } else {
                  this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
                }
              },
              error => {
                console.log("Error");
                this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
              }
            );
          }
        },
        error => {
          console.log("Error");
        }
      );
    }
    // this.closeLoginPopup.next();
  }

  passwordValidation(string) {
    var re = /[a-z]\d|\d[a-z]/i;
    return re.test(string) && string.length >= 8;
  }

  validateEmail(email) {
    var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return re.test(email);
  }

  phonenumber(phone) {
    var phoneno = /^\d{10}$/;
    if (phone.match(phoneno)) {
      return true;
    } else {
      return false;
    }
  }

  resendOtp() {
    this.registeredId = localStorage.getItem("registeredId");
    console.log(this.registeredId);
    console.log(this.registrationType);
    console.log(this.username);
    let body = {
      id: this.registeredId,
      type: this.registrationType,
      username: this.username
    };
    this.userService.resendOtp(body).subscribe(
      res => {
        console.log(res);
        if (res.statusCode == 400)
          this.signUpErrorText = res.message;
        if (res.statusCode == 200)
          this.signUpErrorText = "OTP resent.";
      },
      error => {
        console.log("Error");
      }
    );
  }

  clearErrorText() {
    this.signUpErrorText = "";
  }

  submitOtp(type) {
    this.registeredId = localStorage.getItem("registeredId");
    let body = {
      id: this.registeredId,
      otp: this.user.otp,
      type: this.registrationType
    };
    this.userService.submitOtp(body).subscribe(
      res => {
        console.log(res);
        if (res.statusCode == 400)
          this.signUpErrorText = res.message;
        else if (res.statusCode == 401)
          this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
        else {
          if (type == "register")
            this.storeUserData(res);
          else if (type == "forgotpass")
            this.goToNewPassword();
        }
      },
      error => {
        console.log("Error");
      }
    );
  }

  goToOtpForgot(type) {
    var item;
    var checkType;
    var body = {
      username: type == 'mobile' ? this.user.mobile : this.user.email,
      type: type
    }
    this.userService.forgotPassword(body).subscribe(
      res => {
        console.log(res);
        if (res.statusCode == 200) {
          this.registeredId = res.id;
          localStorage.setItem("registeredId", this.registeredId);
          this.otpSent = res.otpStatus;
          if (!this.otpSent)
            this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
          else
            this.goToOtpFromForget();
        } else {
          this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
        }
      },
      error => {
        console.log("Error");
        this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
      }
    );
  }

  changePassword() {
    this.registeredId = localStorage.getItem("registeredId");
    var body = {
      password: this.user.pwd,
      id: this.registeredId
    };
    if (this.user.pwd != this.rePassword) {
      this.signUpErrorText = "Your passwords do not match.";
    } else {
      if (!this.passwordValidation(this.rePassword)) {
        this.signUpErrorText = "Password must contain atleast a number, a letter and length of 6";
        return;
      } else {
        this.userService.changePassword(body).subscribe(
          res => {
            console.log(res);
            if (res.statusCode == 400)
              this.signUpErrorText = "Our servers are experiencing high traffic, Please try after sometime.";
            else {
              this.storeUserData(res);
            }
          },
          error => {
            console.log("Error");
          }
        );
      }
    }
  }

  goToResetEmailLink() {
    this.makeAllFalse();
    this.resetEmailLink = true;
  }

  goToLoginPopup() {
    this.makeAllFalse();
    this.mainLogin = true;
  }

  goToOtp() {
    this.user.otp = "";
    this.makeAllFalse();
    this.otp123 = true;
  }

  goToOtpFromForget() {
    this.user.otp = "";
    this.makeAllFalse();
    this.forgetOtp = true;
  }

  goAfterOtp() {
    this.makeAllFalse();
    this.afterOtp = true;

  }

  closePopup() {
    this.closeLoginPopup.next();
  }

  goToPhone() {
    this.makeAllFalse();
    this.phone = true;
  }

  goToEmail() {
    this.makeAllFalse();
    this.email = true;
  }

  goToSignUp() {
    this.makeAllFalse();
    this.signUp = true;
    this.consolePopups();
  }
  goToLogin() {
    this.makeAllFalse();
    this.login = true;
  }
  signUpEmail() {
    this.makeAllFalse();
    this.semail = true;
  }
  signUpPhone() {

    this.makeAllFalse();
    this.signPhone = true;

  }
  goToForgetPhoneNumber() {
    this.makeAllFalse();
    this.forgetPhoneNumber = true;
  }
  goToNewPassword() {
    this.makeAllFalse();
    this.newPassword = true;
  }
  goToForgetEmaiPassword() {
    this.makeAllFalse();
    this.forgetEmailPassword = true;
  }


  makeAllFalse() {
    this.afterOtp = false;
    this.otp123 = false;
    this.signPhone = false;
    this.semail = false;
    this.login = false;
    this.phone = false;
    this.email = false;
    this.signUp = false;
    this.mainLogin = false;
    this.forgetPhoneNumber = false;
    this.newPassword = false;
    this.forgetEmailPassword = false;
    this.resetEmailLink = false;
    this.forgetOtp = false;
    this.clearErrorText();
  }
  consolePopups() {
    console.log("this.afterOtp:" + this.afterOtp);
    console.log("this.otp123:" + this.otp123);
    console.log("this.signPhone:" + this.signPhone);
    console.log("this.semail:" + this.semail);
    console.log("this.login:" + this.login);
    console.log("this.phone:" + this.phone);
    console.log("this.email:" + this.email);
    console.log("this.signUp:" + this.signUp);
  }


  public submitState(value: string) {
    console.log('submitState', value);
    //this.appState.set('value', value);
    this.localState.value = '';
  }
}
