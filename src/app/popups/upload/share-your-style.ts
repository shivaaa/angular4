export class ShareYourStyle {
	// _id: string;

	title:string;
    slug:string;
    serviceName:string;
    userId:string;
    userObject:any;
    styleTemplate:string;
    image:any[];     
    product:any[];    
    shareType:string;
    price:any[];    //not using(?)
    parentTagging:any;    //not using(?)
    styleTags:any;    //not using(?)
	contentType:string;    //not required
	profilePic:string;    //not required
	createdBy:string;
	updatedBy:string[];
	status: string;
}