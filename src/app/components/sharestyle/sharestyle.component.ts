import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  
  @Component({
    
    selector: 'share-style', 
    providers: [ ],
    styleUrls: [ './sharestyle.component.css' ],
    templateUrl: './sharestyle.component.html'
  })
  export class ShareStyleComponent implements OnInit {
    public localState = { value: '' };
    
  
    constructor(
      // public appState: AppState,
    ) {}
  
    public ngOnInit() {
      console.log('hello `share-style` component');
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }