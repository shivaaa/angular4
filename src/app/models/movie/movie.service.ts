import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Movie } from './movie';
import { environment } from "../globals";

@Injectable()
export class MovieService {
    private environmentUrl :  any = environment;
    

  constructor(
  	    private http: Http,
    ) { 

  }
//this.baseUrl+'movie/'+slug
  getMovieBySlug(slug) {
  	let movieUrl = this.environmentUrl.baseUrl+"movie-ms/getMovieBySlug/"+slug ;
      return this.http.get(movieUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getTopMovies(industry) {
    let movieUrl = this.environmentUrl.elasticUrl+"movies/_search?sort=createdAt:desc&size=10000&pretty=true&q=industry:"+industry+" AND nowShowing:true";
    return this.http.get(movieUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getReviewCard(slug) {
  	let movieUrl = this.environmentUrl.elasticUrl+"contents/_search?pretty=true&size=1000&q=contentType:critic-review%20AND%20"+slug ;
      return this.http.get(movieUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }



  getCastAndCrew(slug){
    let movieUrl = this.environmentUrl.baseUrl+'movie-ms/castCrew/'+slug;
   // let movieUrl = "http://apiv3.flikster.com/v3/celeb-ms/getCelebById/cce4db5c-ad63-4ae2-b546-56e438757979";
    return this.http.get(movieUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  moviePhotos(slug){
    let movieUrl = this.environmentUrl.baseUrl+'movie-ms/movie/photos/'+slug;
    return this.http.get(movieUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }

  getMoviesForCeleb(slug){
    let movieUrl = this.environmentUrl.elasticUrl+'movies/_search?pretty=true&q=tags:"'+slug+'"';
    // let movieUrl = this.environmentUrl.elasticUrl+'movies/_search?pretty=true&q=*';
    return this.http.get(movieUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getMovieByGenre(genre,industry){
    let movieUrl = this.environmentUrl.elasticUrl+'movies/_search?sort=createdAt:desc&size=5&pretty=true&q=industry:'+industry+' AND genre:'+genre+'';
    // let movieUrl = this.environmentUrl.elasticUrl+'movies/_search?pretty=true&q=*';
    return this.http.get(movieUrl)
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }
  
  postReview(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'ratings-ms/createRating';
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  checkUserRating(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'ratings-ms/getRatingByUser';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

 }
//  movieStatus(body) : Observable<any>{
//   let ordersUrl = this.environmentUrl.baseUrl+'likes-ms/isMovieWatchStatus';
//     let headers = new Headers({ 'Content-Type': 'application/json' });
//     let options = new RequestOptions({ headers: headers });
//     return this.http.post(ordersUrl, body, options)
//     .map((res:Response) => res.json())
//     .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

// }
movieStatus(slug){
  let movieUrl = this.environmentUrl.baseUrl+'likes-ms/isMovieWatchStatus/'+slug;
  // let movieUrl = this.environmentUrl.elasticUrl+'movies/_search?pretty=true&q=*';
  return this.http.get(movieUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
sendWatchStatus(body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'likes-ms/postCardStatus';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

}
UsermovieStatus(body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'likes-ms/getUserWatchStatus';
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

}
}