import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
  
// import { AppState } from '../../app.service';
import { CarouselService } from '../../models/carousel/carousel.service';
import { FashionService } from "../../models/fashion/fashion.service";
import { ProductService } from "../../models/product/product.service";
import {DomSanitizer} from '@angular/platform-browser'
import { ContentService } from "../../models/content/content.service";
import {Location} from '@angular/common';

  @Component({
    
    selector: 'see-all', 
    providers: [ ],
    styleUrls: [ './see-all.component.css' ],
    templateUrl: './see-all.component.html'
  })
  export class SeeAllComponent implements OnInit {
    catago: string;
    subCatagori: string;
    showSeeMore: boolean = true;
    disable: boolean=false;
    productsArray = [];
    Products: any;
    showProducts: boolean;
    public showtqd = 3;
    public showTweet=true;
    public showQuote=true;
    public showDialogue=true;
    public string: any;
    public womenArray = [];
    public menArray=[];
    public dialougeArray: any;
    public quoteArray: any;
    public tweetArray: any;
    public Data: any;
    public displayArray = [];
    public tqd=[];
    public industry: string;
    public contentType: string;
    public stealThatStyleData: any;
    public productArray = [];
    public videourl: any;
    public title: any;
    public videosArray = [];
    public shopbyvideos: any;
    public items: string[];
    public item: string;
    public womenCatImages = [
      "https://bucket-for-image-test.s3.amazonaws.com/women-clothing.png",
      "https://bucket-for-image-test.s3.amazonaws.com/footwear.png",
      "https://bucket-for-image-test.s3.amazonaws.com/women-accessories.png",
      "https://bucket-for-image-test.s3.amazonaws.com/jewellery.png"
    ];
    public menCatImages = [
      "https://bucket-for-image-test.s3.amazonaws.com/men-clothing.png",
      "https://bucket-for-image-test.s3.amazonaws.com/men-accesories.png",
      "https://bucket-for-image-test.s3.amazonaws.com/menfootwear.png",
      "https://bucket-for-image-test.s3.amazonaws.com/ethinc-colection.png"
    ];
    public womencata: any;
    public mencatg: any;
    public caurosel: any;
    public localState = { value: '' };
    public seeAllType:any;
    public content;
    public mainRoute;
    public rootRoute;
    public mainPage;
    public subPage;
    public category:any;
    public categoryProducts;
    public previous: boolean= true;
    public page;
    public isShopByVideos: boolean = false;
    public popup: boolean = false;
    public popupVideo: boolean = true;
    public popupViewAll: boolean = false;
    public nocarosel;
    public miniCard: boolean = true;
    public activeArray = [];
    public inactiveArray = [];
    public inactiveArrays = [];
    
    // player: YT.Player;
    private id: string = 'vlkNcHDFnGA';
    private height: number = 460;
    private width: any = '100%';
   

  
    constructor(
      private _location: Location,
      // public appState: AppState,
      public router : Router,
      public route: ActivatedRoute,
      public carouselService : CarouselService,
      public fashionService : FashionService,
      public productService:ProductService,
      public sanitizer: DomSanitizer,
      public contentService : ContentService,
    ) {}
  
    public ngOnInit() {
      window.scrollTo(0,0);
        
      console.log('hello `see-all` component');
      var urlBreakup = this.router.url.split("/");
      this.content = urlBreakup[1];
      this.contentType=urlBreakup[2];
      this.seeAllType = urlBreakup[3];
      this.catago= urlBreakup[4];
      this.displayArray=[];
      console.log(this.content);
     
      
      
      this.mainRoute = localStorage.getItem("mainRoute");
      this.rootRoute = localStorage.getItem("rootRoute");
      
      this.page = localStorage.getItem('page');
      this.industry=localStorage.getItem('Industry');
     
      //this.nocarosel=this.rootRoute;
      this.subPage = localStorage.getItem("subPage");
      this.subCatagori=localStorage.getItem('subCatagori');
      // if(this.content !='see-all') {
      //   this.subPage = localStorage.getItem("subPage");
      //   this.category=false;
        this.category = localStorage.getItem("category");
      //   // this.getProductsByCategory(this.category.toLowerCase());
      // }
      if(this.contentType == "shop-by-videos") {   
        this.isShopByVideos = true;
        var industry=localStorage.getItem('Industry');
        if(this.seeAllType == "menfashion") {
          this.getshopbyvideos('menfashion', industry);
        }
        if(this.seeAllType=='womenfashion')
          this.getshopbyvideos('womenfashion',industry);
        if(this.seeAllType=='movie-store')
          this.getshopbyvideos('movie',industry);
         if(this.seeAllType=='celeb-store')
          this.getshopbyvideos('celebrity',industry);
         if(this.seeAllType=='designer-store')
          this.getshopbyvideos('designer',industry);

      }
      
      if(this.contentType=='steal-that-style'){
        var industry=localStorage.getItem('Industry');
        if(industry==this.seeAllType)
        this.getStealThatStyle(this.seeAllType);
        else
          if(this.seeAllType=='menfashion')
          this.getStealThatStyleByCatagory(industry,'men');
        if(this.seeAllType=='womenfashion')
          this.getStealThatStyleByCatagory(industry,'women');
        if(this.seeAllType=='celeb-store')
          this.getStealThatStyleByCatagory(industry,'celebrity');
        if(this.seeAllType=='movie-store')
          this.getStealThatStyleByCatagory(industry,'movie');
       

      }
      if(this.contentType=='justin'){
        this.getJustinProducts(this.seeAllType);
      }
      if(this.contentType=='clothing'){
        this.getClothingProducts(this.seeAllType);
      }
      if(this.contentType=='footwear'){
        this.getFootwearProducts(this.seeAllType);
      }
      if(this.contentType=='designerwear'){
        this.getDesignerWear(this.seeAllType);
      }
      if(this.contentType=='accessories'){
        this.getAccessories(this.seeAllType);
      }
      if(this.contentType=='shirts-tees'){
        this.shirtsProducts(this.seeAllType);
      }
      if(this.contentType=='movie-house'){
        this.getMovieScreenProducts();
      }
      if(this.contentType=='small-screen'){
        this.getSmallScreenProducts();
      }
      if(this.contentType=='shop-the-look'){
        this.getShopTheLookProducts();
      }
      if(this.contentType=='news'){
        this.getTopContent('news', this.seeAllType);

      }
      if(this.contentType=='whats-new'){
        this.getLatestObsessions();

      }
      if(this.contentType=='videos'){
        this.getTopContent('trailer', this.seeAllType);

      }
      if(this.contentType=='video-song'){
        this.getTopContent('video-song', this.seeAllType);

      }
      if(this.contentType=='interview'){
        this.getTopContent('interview', this.seeAllType);

      }

      if(this.contentType=='gallery'){
        this.getTopContent('gallery', this.seeAllType);

      }
      if(this.contentType=='social'){
        this.getTweets( this.seeAllType);

      }
      if(this.seeAllType=='womenwear'|| this.seeAllType=='menwear'){
        this.getAllCatogeries();

      }
      if(this.content=='menfashion'||this.content=='womenfashion'){
        
        if(this.seeAllType)
          {
            console.log('sssssssssssss');
            console.log(this.seeAllType);
           
            if(this.content=='menfashion'){
              if(this.contentType=='Eyeware'){
                if(this.catago){
                  this.showProducts=true;
                this.getProductsByCatagori('men',this.seeAllType,this.catago);
                  }

              }
              else if(this.contentType=='Accessories'){
                if(this.catago){
                this.showProducts=true;
              this.getProductsByCatagori('men',this.seeAllType,this.catago);
                }
              }


            else{
              this.showProducts=true;
            this.getProductsByCatagori('men',this.contentType,this.seeAllType);
            }

          }
            if(this.content=='womenfashion'){
              if(this.contentType=='Clothing'){
                if(this.catago){
                this.showProducts=true;
              this.getProductsByCatagori('women',this.seeAllType,this.catago);
                }
              }
             else if(this.contentType=='Accessories'){
                if(this.catago){
                this.showProducts=true;
              this.getProductsByCatagori('women',this.seeAllType,this.catago);
                }
              }
              else{
                this.getProductsByCatagori('women',this.contentType,this.seeAllType);
                this.showProducts=true;
              }
             }
          }
        }



this.route.params.subscribe( params => 
{
   this.seeAllType = params["slug"];         //very important
});



    }
    getProductsByCatagori(fashion,category,type){
      this.productService.getProductByCategory(fashion,category,type).subscribe(
       res => {
         console.log("Product service");
         console.log(res);
         this.Products=res.hits.hits;
         for(let i=0;i<this.Products.length;i++){
         
          this.productsArray.push(this.Products[i]._source)
         }
         console.log(this.productsArray);
        //  var productdetails=res.hits.hits[0];
        //   if( this.productArray)
        //     this.productArray.push(productdetails._source);
           
          
        //   console.log(this.productArray);
 
       },
       error => {
         console.log("error getting product");
       }
     );
 
 
     
 
   }
   

    showallTweets(){
      this.showSeeMore=false;
      this.showtqd=12;
      this.showQuote=false;
      this.showDialogue=false;

    }
    showallQuotes(){
      this.showSeeMore=false;
      this.showTweet=false;
      this.showtqd=12;
      this.showDialogue=false;

    }
    showallDialogues(){
      this.showSeeMore=false;
      this.showQuote=false;
      this.showTweet=false;
      this.showtqd=12;

    }
    backState(){
      this.showSeeMore=true;
      this.showtqd=3;
      this.showQuote=true;
      this.showTweet=true;
      this.showDialogue=true;
    }


    getProductsByCategory(tag) {
      this.productService.getProductsByTag(tag).subscribe(
        res=> {
          if(res){
            this.categoryProducts =res.hits.hits;
            console.log("************Category Products***************");
            console.log(this.categoryProducts);
            }
        },
        error => {
          console.log("error");
        }
      );
    }

    getJustinProducts(pageType){
      this.fashionService.getLatestProducts(pageType).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
          
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    goBack(){
      this._location.back();
    }
    getClothingProducts(type){
      this.fashionService.getClothingProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
            this.displayArray=this.displayArray.reverse();
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }
    getFootwearProducts(type){
      this.fashionService.getFootwareProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
            
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getDesignerWear(type){
      this.fashionService.getDesignerProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }
    getAccessories(type){
      this.fashionService.getAccessories(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
            // this.getCarouselArray(this.latestArrivedProducts,4);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    shirtsProducts(type){
      this.fashionService.getShirtsProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
           
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getMovieScreenProducts(){
      
      this.fashionService.getBigScreenProds(this.industry).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getSmallScreenProducts(){
      this.fashionService.getSmallScreenProds().subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getShopTheLookProducts() {
     
      this.fashionService.shopTheLook().subscribe(
        res => {
          console.log(res);
          if(res) {
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
              
              
              
          }
        },
        error => {
          console.log("error getting shopTheLook");
        }
      );
    }
    getLatestObsessions(){
      this.fashionService.latestObsession().subscribe(
        res => {
          console.log(res);
          if(res){
            for(let i=0;i<res.hits.hits.length;i++){
              this.displayArray.push(res.hits.hits[i]._source);
            }
              
          }
        },
        error => {
          console.log("Error in LatestObsessions");
        }
      );
    }
    goToSubpage() {
    localStorage.removeItem('category');
      this.previous=false;
      // this.category = localStorage.getItem("subPage");
      if(this.subCatagori){
        this.content='subcatagori';
      this.router.navigate([this.page+'/'+this.subCatagori+'/'+this.subPage]);
    }
      else
        this.router.navigate([this.page+'/'+this.subPage]);
      
    }
    goToSubsubpage() {
      // localStorage.setItem('items',localStorage.getItem('subItem'));
      localStorage.removeItem('subPage');
      localStorage.removeItem('category');
        this.previous=false;
        // this.category = localStorage.getItem("subPage");
        this.router.navigate([this.page+'/'+this.subCatagori]);
        
      }
    getshopbyvideos(page,industry){
      this.fashionService.getshopbyvideos(page,industry).subscribe(
        res => {
          console.log("THIS IS shopbyvideos DATA");
          console.log(res);
          this.shopbyvideos=res.hits.hits;
          for(let i=0;i<this.shopbyvideos.length;i++){
            this.videosArray.push(this.shopbyvideos[i]._source);
          }


          // if(res){
            
          //     this.pagecontent = res;
          // }
        },
        error => {
          console.log("Error in getting top content");
        }
      );


    }
    getAllShopbyVideos(){
      this.fashionService.getAllShopbyVideos().subscribe(
        res => {
          console.log("THIS IS shopbyvideos DATA");
          console.log(res);
          this.shopbyvideos=res.hits.hits;
          // this.shopbyVideosData=res.hits.hits;
          console.log( this.shopbyvideos);
          for(let i=0;i<this.shopbyvideos.length;i++){
            this.videosArray.push(this.shopbyvideos[i]._source);
          }

          // if(res){
            
          //     this.pagecontent = res;
          // }
        },
        error => {
          console.log("Error in getting top content");
        }
      );
    }
    displayPopup(item) {
      console.log(item);
      window.scrollTo(0,0);
      console.log(item);
      // this.productArray = item.products;
      if(item.products)
      for(let i=0;i<item.products.length;i++){
       this.getProductDetails(item.products[i].productSlug);
      }
      
      this.getEmbedVideoUrl(item.videoUrl);
      this.title=item.title;
      this.popupViewAll = false;
      this.popup = true;
      this.popupVideo = true;
      console.log("popup displayed");
    }
    getEmbedVideoUrl(url) {
      if(url.split('/')[2] == "www.youtube.com") {
        if(url.split('/')[3] == "embed") {
          this.videourl = url;
        } else {
          this.videourl = "https://www.youtube.com/embed/" + url.split('/')[3].split('=')[1];
        }
      } else {
        this.videourl = "https://www.youtube.com/embed/" + url.split('/')[3];
      }
      console.log(this.videourl);
    }
    getProductDetails(slug){
      this.productService.getProductBySlug(slug).subscribe(
       res => {
         console.log("Product service");
         console.log(res);
         var productdetails=res.hits.hits[0];
          if( this.productArray)
            this.productArray.push(productdetails._source);
           
          
          console.log(this.productArray);
 
       },
       error => {
         console.log("error getting product");
       }
     );
 
 
     
 
   }

    viewAll() {
      window.scrollTo(0,0);
      this.popupViewAll = true;
       this.popupVideo = false;
      
    }

    backToVideo() {
      this.popupViewAll = false;
      this.popupVideo = true;
    }

    closePopup() {
      this.backToVideo();
      this.popup = false;
      this.productArray=[];
    }
    disablePopup(){
      this.disable=true;
      
    }
    closeDisable(){
      if(this.disable == false){
        this.backToVideo();
        this.popup = false;
        this.productArray=[];
  
      }
  
    }

   
    goToCatogery(page,subPage,category,) {
      localStorage.setItem("subPage",subPage);
      localStorage.setItem("page",page);
      // localStorage.setItem("category",category);
      
      this.router.navigate([page,subPage]);  
    }
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    getStealThatStyle(industry){
      this.fashionService.stealThatStyle(industry).subscribe(
        res => {
          console.log(res);
          if(res){
            this.stealThatStyleData = res.hits.hits;
            for(let i=0;i<this.stealThatStyleData.length;i++){
              this.displayArray.push(this.stealThatStyleData[i]._source);
              
            }
            console.log(this.displayArray);
            console.log(this.stealThatStyleData);
          }
        },
        error => {
          console.log("Error in stealThatStyle");
        }

      );
    }
    getStealThatStyleByCatagory(industry,catagory){
      this.fashionService.stealThatStyleFashion(industry,catagory).subscribe(
        res => {
          console.log(res);
          if(res){
            this.stealThatStyleData = res.hits.hits;
            for(let i=0;i<this.stealThatStyleData.length;i++){
              this.displayArray.push(this.stealThatStyleData[i]._source);
              
            }
            console.log(this.displayArray);
            console.log(this.stealThatStyleData);
          }
        },
        error => {
          console.log("Error in stealThatStyle");
        }

      );
    }
    getTopContent(type,industry) {
      this.contentService.getTopContent(type,industry).subscribe(
        res => {
          console.log(res.hits.hits);
          if(res){
            this.Data=res.hits.hits;
            for(let i=0;i<this.Data.length;i++){
              this.displayArray.push(this.Data[i]._source);

            }
            // if(type == "news") {
            //   this.topContentNews = res.hits.hits;  
            // } else if(type == "gallery") {
            //   this.topContentGallery = res.hits.hits;
            // } else if(type == "video-song") {
            //   this.topContentVideos = res.hits.hits;
            // }
          }
        },
        error => {
          console.log("Error in getting top content");
        }

      );
    }
    getTweets(industry){
      let body ={
        'industry':industry,
      };
      this.contentService.industryTweets(body).subscribe(
        res => {
            console.log(res);
             this.tweetArray=res.tweet.hits.hits;
             this.quoteArray=res.quote.hits.hits;
             this.dialougeArray=res.dialouge.hits.hits; 
             this.tqd.push(this.tweetArray);
             this.tqd.push(this.quoteArray);
             this.tqd.push(this.dialougeArray);
             console.log(this.tqd);
             console.log(this.quoteArray);
             console.log(this.dialougeArray);
        
           },
          error => {
            console.log("Error in posting orders");
          }
    );
  
  
  }
  goTomenCatogery(page,subPage,i) {
    console.log(page+subPage+i);

    if (subPage == 'Clothing') {
      localStorage.setItem('items', 'Clothing,Polos & Tees,Casual Shirts,Casual Shirts,Jeans,Casual Trousers,Formal Shirts,Suits & Blazers,Track Wear,Shorts & 3/4ths,Ethnic Wear,Winter Wear');

      localStorage.setItem('subPage', 'Clothing');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Footwear') {
      localStorage.setItem('items', 'Footwear,Casual Shoes,Sports Shoes,Sneakers,Boots,Loafers,Formal Shoes,Outdoor & Hiking,Floaters,Flip Flop');

      localStorage.setItem('subPage', 'Footwear');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Eyewear') {
      localStorage.setItem('items', 'Eyewear,Eyeglasses,Sunglasses');
      localStorage.setItem('subItem', 'Eyewear,Eyeglasses,Sunglasses');
      localStorage.setItem('subCatagori', 'Eyewear');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Accessories') {
      localStorage.setItem('items', 'Accessories,Bags,Belts,Caps & Hats,Jewellery,Mufflers & Scarves,Tech Accessories,Ties & Cuf,Watches');
      localStorage.setItem('subItem', 'Accessories,Bags,Belts,Caps & Hats,Jewellery,Mufflers & Scarves,Tech Accessories,Ties & Cuf,Watches');
      localStorage.setItem('subCatagori', 'Eyewear');
      localStorage.removeItem('subPage');
    }

    localStorage.removeItem('category');

    localStorage.setItem("rootRoute", 'menfashion');
     this.router.navigate([page,subPage]);  
  }
  goTowomenCatogery(page,subPage,i) {
    console.log(page+subPage+i);
    if (subPage == 'Clothing') {
      localStorage.setItem('items', 'Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
      localStorage.setItem('subItem', 'Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
      localStorage.setItem('subCatagori', 'Clothing');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Accessories') {
      localStorage.setItem('items', 'Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');
      localStorage.setItem('subItem', 'Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');

      localStorage.setItem('subCatagori', 'Accessories');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Footwear') {
      localStorage.setItem('items', 'Footwear,Flats,Sandals,Bellies,Boots,Heels,Wedges,Stilettos,Peep Toes,Flip Flops,Sports Shoes,Sneakers,Loafers');

      localStorage.setItem('subPage', 'Footwear');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Jewellery') {
      localStorage.setItem('items', 'Jewellery,Earrings,Necklaces & Necklace Sets,Bangles & Bracelets,Rings & Bands,Pendants & Pendant Sets,Traditional Jewellery,Anklets,Brooches,Body Chains');

      localStorage.setItem('subPage', 'Jewellery');
      localStorage.removeItem('subCatagori');
    }

    localStorage.removeItem('category');

    localStorage.setItem("rootRoute", 'womenfashion');
    
    
    
    this.router.navigate([page,subPage]);  
  }
  getAllCatogeries(){
    this.carouselService.getCatogeries().subscribe(
      res=>{
        console.log(res);
        if(res){
          this.caurosel = res.Items;
          console.log("**********Collections********");
          console.log(this.caurosel[0].menFashion);
          this.mencatg=this.caurosel[0].menFashion;
          for(let i=0;i<this.mencatg.length;i++){
            this.menArray.push(Object.keys(this.mencatg[i])[0]);
          }
          
          this.womencata=this.caurosel[0].womenFashion;
          for(let i=0;i<this.womencata.length;i++){
            this.womenArray.push(Object.keys(this.womencata[i])[0]);
          }
          console.log("*********men********");
          console.log(this.menArray);
          
        }

    },
    error => {
      console.log("Error in c togeries");
    }
  );
  }
  }
  