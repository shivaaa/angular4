import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Search } from './search';
import { environment } from "../globals";
import { HttpClient } from '@angular/common/http';
@Injectable()
export class SearchService {
    public baseUrl = "http://mock.flikster.com:4000/";
    private environmentUrl :  any = environment;

  constructor(
  	    private http: Http,
    ) { 

  }

  getSearchBySlug(slug) {
  	let searchtUrl = this.baseUrl+'product/'+slug;
      return this.http.get(searchtUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  globalSearch(text) : Observable<any>{
      let searchUrl = this.environmentUrl.baseUrl+"search-ms/globalSearch";    //elastic implemented in node for this
      let body = {
        searchTag : text
      };
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(searchUrl, body, options)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  

}