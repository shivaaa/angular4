import {
    Component,
    OnInit
  } from '@angular/core';
  import { Router } from '@angular/router';
  
  // import { AppState } from '../../app.service';

  @Component({
    selector: 'auction', 
    providers: [ ],
    styleUrls: [ './edit-profile.component.css' ],
    templateUrl: './edit-profile.component.html'
  })

  export class EditProfileComponent implements OnInit {
    public localState = { value: '' };
    public pageType;
    public userName;


    constructor(
        // public appState: AppState,
        public router : Router,
    ){}

     public ngOnInit() {
          this.userName = localStorage.getItem("name");
      }

      save() {
        localStorage.setItem("name",this.userName);
      }

      public submitState(value: string) {
        console.log('submitState', value);
        // this.appState.set('value', value);
        this.localState.value = '';
      }

  }