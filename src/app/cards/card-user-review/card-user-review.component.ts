import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  import { MovieService } from "../../models/movie/movie.service";

  @Component({
    
    selector: 'card-user-review', 
    providers: [ ],
    styleUrls: [ './card-user-review.component.css' ],
    templateUrl: './card-user-review.component.html'
  })
  export class UserReviewComponent implements OnInit {
    shareProfilePic: any;
    shareSlug: any;
    userId: string;
    username: string;
    public page: any;
    public router: any;
    isMiniCard: any;
    public range: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9,10,];
    public localState = { value: '' };
    public isReview = true;
    public reviews = [];
    @Input() content: any;
    @Input() miniCard: any;
    public contentData : any;
    public assnData : any;
    public cardId : any;
    public type : any;

    public displayRatingPopup : boolean = false;
  
    constructor(
      // public appState: AppState,
      public movieService:MovieService,
    ) {}
  
    public ngOnInit() {
     // var urlBreakup = this.router.url.split('/');
     // console.log(urlBreakup);
      //var slug = urlBreakup[2];
      //this.page = urlBreakup[1];
      console.log('hello `card-user-review` component');
      this.contentData = this.content;
      if (this.contentData)
      this.type = this.contentData.contentType;
      console.log("****************REVIEW CARD******************");
      console.log(this.type);
      this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
      if(this.contentData.celeb)
       this.assnData=this.contentData.celeb[0];
       if(this.contentData.movie)
         this.assnData=this.contentData.movie[0];
     // this.assnData = this.content.assnData;
     if(this.miniCard) {
       this.isMiniCard = this.miniCard;
     }
     this.username=localStorage.getItem('username')
      // let review = {
      //   title : "Title",
      //   name : "User Name",
      //   rating : 7,
      //   review : "When an unknown printer took a galley of type and scrambled it to make a type specimen book.This is a test reviews from developers/testers for testing the text for review tests.",
      //   profilePic : "http://images.desimartini.com/media/uploads/2017-5/arjunreddyfirstlook.jpg"
      // };
      // this.reviews.push(review);
      // this.reviews.push(review);
      // this.reviews.push(review);
      // this.reviews.push(review);
      // this.reviews.push(review);
      
      this.contentData = this.content;
      this.userId=localStorage.getItem('username');
      let userdata={
        
        'entityId':this.cardId,
        'userId': String(this.userId),
       
      };
      this.getcontentcomments(userdata);

    }
    getcontentcomments(id){
      
        this.movieService.checkUserRating(id).subscribe(
        res => {
            console.log(res);
            // this.movies[i].rating=res.message.Items[0].ratingValue;
            if(res){
              this.reviews=res.message.Items;
              console.log( this.reviews);
            // this.movies[i]={
            //   'rating':res.message.Items[0].ratingValue,
            // }
          }
            
          },
          error => {
            console.log("Error in posting orders");
          }
    );
  
      

    }


    ratingPopup () {
      this.displayRatingPopup = true;
    }

    submitingRatingPopup (review) {
      if(review) {
        console.log(review);
        this.reviews.push(review);
      }
      this.displayRatingPopup = false;
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
    }
    
    gotocontent(){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }