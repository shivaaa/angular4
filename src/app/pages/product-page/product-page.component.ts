import {
  Component,
  OnInit,
  Input,
  OnDestroy
 
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ShareButtons } from '@ngx-share/core';
import { Meta } from '@angular/platform-browser';
// import { AppState } from '../../app.service';
import { ProductService } from "../../models/product/product.service";
import { FashionService } from "../../models/fashion/fashion.service";
import {DomSanitizer} from "@angular/platform-browser";
import { Product } from "../../models/product/product";
import { Observable } from "rxjs/Observable";
import { MetaService } from '@ngx-meta/core';

require('aws-sdk/dist/aws-sdk')
declare var $:any;

@Component({
  
  selector: 'productpage', 
  providers: [ ],
  styleUrls: [ './product-page.component.css' ],
  templateUrl: './product-page.component.html'
})


export class ProductPageComponent implements OnInit , OnDestroy {
  relsecondsLeft: number;
  relminutsLeft: number;
  relhoursLeft: number;
  reldaysLeft: number;
  relmiliseconds: number;
  relendDate: any;
  relstartDate: any;
  currentMaxPrice: number;
  userids=[];
  secondsLeft: number;
  productUrl: string;
  displayText: string;
  sizes: any;
  show: boolean = true;
  hideIndex:any =false;
  sendObject: any;
  productOrder: any;
  orderId: any;
  get: boolean;
  string: any;
  categories: any;
  pageType: string;
  currentData: any;
  auctionId: any;
  minutsLeft: number;
  daysLeft: number;
  bidArray: any;
  showbid: boolean = false;
  hoursLeft: number;
  milisecondsDiff: number;
  miliseconds: number;
  endDate: any;
  startDate: any;

  auctionsize: any;
  sizeArray = [];
  public startingprice: number;
  public orderproduct:any;
  public bidmore:number;
  public bidincrement:number;
  public bidstarting:number;
  public relatedLooks: any;
  public relatedFliks=[];
  public submitted: boolean = false;
  public bidCounts: any;
  public bidAmount: number;
  public success: Product;
  public productcolor: any;
  public productsize: any;
  public countDown: Observable<number>;
  public time: number=100;
  public auctionBidding;
  public today: number = Date.now();
  public enter:boolean=false;
  public bidprice:number;
  public  orderid: any;
  public quantity: any;
  public price: any;
  public  name: any;
  public lookproduct: any;
  public  Auctionproduct: any;
  public auctionslug: string;
  public getLooks:any;
  public page: string;
  public slug: string;
  public localState = { value: '' };
  public product;
  public isAuction : boolean = false;
  public imagesArray;
  public leadImage;
  public isShareyourStyleProduct : boolean = false;
  public isIndividualProducts : boolean = false;
  public stealThatStyle : any;
  public pdpitems = "pdpitems";
  public counter = 1;
  //public min  = 1;
  public uploadedImage;
  public item;
  public isLooks;
  public imgs;
  public addedToBag : boolean = false;
  public icurrentarray=[];
  public currentarray=[];
  public productsArray=[];
  private alive: boolean = true;
  public zoomOptions = {
    peepView: {
      borderColor: '#fff',
      borderWidth: '1px',
      borderStyle: 'solid',
      cursor: 'zoom-in',
    },
    settings: {
      zoom: 2,   // 4x zoom 
    }
  }

  constructor(
    // public appState: AppState,
    private productService : ProductService,
    public fashionService : FashionService,
    private sanitizer: DomSanitizer,
    public router : Router,
    public activatedRoute: ActivatedRoute,
    private metaService: MetaService,
    public share: ShareButtons,
    private meta: Meta,
    

  ) {
    // console.log(this.time);
    // this.countDown = Observable.timer(0,1000)
    // .take(this.time)
    // .map(()=> --this.time);
  }

  public ngOnInit() {
 $(document).ready(function() {
      $(function() {
    
  });
  });


    // this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    this.productUrl=this.router.url;
    this.displayText=null;
    var urlBreakup = this.router.url.split("/");
    console.log(urlBreakup);
    this.page = urlBreakup[1];
    this.slug = urlBreakup[2];
    if(urlBreakup[3])
    this.auctionslug=urlBreakup[3];
    if(this.slug=='auction' && this.auctionslug)
    Observable.interval(1000).takeWhile(() => this.alive).subscribe(() => this.updateTime());

    console.log('hello `productpage` component');
    

    this.getStealThatStyle("tollywood");
    window.scrollTo(0,0);
    this.item = localStorage.getItem("item");
    this.pageType=localStorage.getItem("rootRoute");
    if(!this.auctionslug){
      if(this.page=='product'){
        
        let route = this.activatedRoute.params.subscribe(params => {
          console.log(params);
          this.getProduct(params.slug);
          window.scrollTo(0,0);
        });
       
      }
      if(this.page=='looks') {
        let route = this.activatedRoute.params.subscribe(params => {
          console.log(params);
          this.getLook(params.slug);
          window.scrollTo(0,0);
        });
        
        this.isLooks = true;
      }
     }
    //  if(this.slug=='auction'){
    //    if()
    //   this.gethighestbid(this.auctionId);
    //  }
    if(this.auctionslug){
      this.getAuctionProduct(this.auctionslug);
      if(this.auctionId)
      this.gethighestbid(this.auctionId);
      
    }
    
  }
  public ngOnDestroy(){
    this.alive = false;
  }
  swaplead(){
    this.leadImage=this.product.profilePic;
  }
  showSelected(item){
    this.displayText=null;
    this.productsize=item;
    //this.selected = true
    console.log(item);
    //this.selected = item;
  }

  swapImages(index) {
    console.log(index);
    //console.log("index:"+index);
    // if(index=='profile'){
    //   var temp =this.product.profilePic;
    // this.leadImage=temp;
    // }
    // else{
    if(!this.auctionslug){ 
      var temp=this.product.imageGallery[index];
    this.leadImage = this.product.imageGallery[index];
    this.leadImage=temp;
    }
    if(this.auctionslug){
      var temp=this.Auctionproduct.gallery[index];
    this.leadImage = this.Auctionproduct.gallery[index];
    this.leadImage=temp;
    }
  // }
  }
  swapProfileImages(){
    this.leadImage = this.Auctionproduct.profilePic;


  }

  fileEvent(fileInput: any) {
     let AWSservice = (<any>window).AWS;
     console.log(AWSservice);
     let file = fileInput.target.files[0];
     console.log("fileInput.target:");
     console.log(fileInput.target);
     AWSservice.config.accessKeyId = 'AKIAJT7SFJQEJTU5AOKA';
     AWSservice.config.secretAccessKey = '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';
     let bucket = new AWSservice.S3({params: {Bucket : 'bucket-for-image-test'}});
     let params = {Key : file.name, Body : file};
     bucket.upload(params, function(err, res){
       console.log('error', err);
       console.log('response', res);
       this.uploadedImage = res.Location;
       console.log(this.uploadedImage);
     })
  }
  mouseEnter(){
  this.hideIndex='true';
  
  }
   mouseLeave(){
   this.hideIndex='false';
 
  }
  getLook(slug) {
    this.productService.getLookBySlug(slug).subscribe(
     res => {
       console.log("Look service");
       console.log(res);
        if(res) {
          if(res.hits.hits[0])
          this.lookproduct = res.hits.hits[0]._source;
         //this.imgs = [this.sanitizer.bypassSecurityTrustResourceUrl(res.imageGallery)];
          // console.log(this.imgs);
          if(this.lookproduct)
          if(this.lookproduct.products)
          this.getProduct(this.lookproduct.products[0].productSlug);
        }

     },
     error => {
       console.log("error getting product");
     }
   );

  //  this.topMovies = this.topMoviesMock;
  //this.product = this.productMock;
 }

 gotoPage(page){
   console.log(page);
   if(page=='Men Fashion'){
    this.router.navigate(['menfashion']);
   }
   else
    this.router.navigate(['womenfashion']);

 }
 gotoSubpage(page,subpage){
   console.log(page+subpage);
   if(page=='Men Fashion'){
     localStorage.removeItem('category');
     localStorage.setItem('subPage',subpage);
     localStorage.setItem('rootRoute','menfashion');
     this.getcategories('men',subpage,'mencatagory');
    // this.router.navigate(['menfashion/'+subpage]);
   }
   else if(page=='Women Fashion'){
    localStorage.removeItem('category');
    localStorage.setItem('subPage',subpage);
    localStorage.setItem('rootRoute','womenfashion');
    this.getcategories('women',subpage,'womencatagory');
    // this.router.navigate(['womenfashion/'+subpage]);
   }

 }
 gotoCatagory(page,subpage,catagory){
  console.log(page+subpage+catagory);
  if(page=='Men Fashion'){
    localStorage.setItem('category',catagory);
    localStorage.setItem('subPage',subpage);
    localStorage.setItem('rootRoute','menfashion');
    this.getcategories('men',subpage,'products');
   this.router.navigate(['menfashion/'+subpage+'/'+catagory]);
  }
  else if(page=='Women Fashion'){
   localStorage.setItem('category',catagory);
   localStorage.setItem('subPage',subpage);
   localStorage.setItem('rootRoute','womenfashion');
   this.getcategories('women',subpage,'products');
   this.router.navigate(['womenfashion/'+subpage+"/"+catagory]);
  }
 }
 gotosubCatagory(page,subpage,catagory,subcatagory){
  if(page=='Men Fashion'){
    localStorage.setItem('category',catagory);
    localStorage.setItem('subPage',subpage);
    localStorage.setItem('rootRoute','menfashion');
    this.getcategories('men',subpage,'products');
   this.router.navigate(['menfashion/'+subpage+'/'+catagory+'/'+subcatagory]);
  }
  else if(page=='Women Fashion'){
   localStorage.setItem('category',catagory);
   localStorage.setItem('subPage',subpage);
   localStorage.setItem('rootRoute','womenfashion');
   this.getcategories('women',subpage,'products');
   this.router.navigate(['womenfashion/'+subpage+"/"+catagory+'/'+subcatagory]);
  }

 }


 getcategories(type,page,navigat){   
   console.log(page);
  this.fashionService.getCategories().subscribe(
    res => {
      console.log("THIS IS Categories DATA");
      console.log(res);
      if(res){
        if(type=='men'){
          this.categories = res.Items[0].menFashion;
          console.log(this.categories);
        }
        else if(type=='women'){
          this.categories = res.Items[0].womenFashion;
          console.log(this.categories);
        }
        if(this.categories)
          for(let i=0;i<this.categories.length;i++){
        if(this.categories[i][0]==page)
          {console.log('hit');
          console.log(this.categories[i]);
          this.string=this.categories[i].join(',');
          console.log(this.string);
            localStorage.setItem("items",this.string);
          }
        }
        if(navigat=='mencatagory'){
          this.router.navigate(['menfashion/'+page]);

        }
        if(navigat=='womencatagory'){
          this.router.navigate(['womenfashion/'+page]);

        }
          
      }
    },
    error => {
      console.log("Error in getting categories");
    }
  );
}

 productPivot(pivot) {
   console.log(pivot);
   window.scrollTo(0,0);
   this.getProduct(this.lookproduct.products[pivot].productSlug);
 }

  getProduct(slug) {
    this.productService.getProductBySlug(slug).subscribe(
     res => {
       console.log("Product service");
       console.log(res);
        if(res) {
          this.addedToBag = false;
          this.counter=1;
          this.product = res.hits.hits[0]._source;
          
          if(this.product.size)
          this.sizes = this.product.size.length;
          console.log(this.product);
          console.log(this.sizes);
          let name;
          if(this.product.name)
          name = this.product.name;
          if(this.product.title)
          name = this.product.title; 
          console.log(name);
          this.metaService.setTitle(' '+name);
          this.metaService.setTag('og:title', this.product.seo_title?this.product.seo_title:name);
          this.metaService.setTag('og:description',this.product.seo_description?this.product.seo_description:name);
          this.metaService.setTag('og:keywords',this.product.seo_keywords?this.product.seo_keywords.toString():name);
          this.metaService.setTag('og:image',this.product.profilePic);
          this.metaService.setTag('fb:app_id','433236906796403');
          this.metaService.setTag('og:type','article');
          this.metaService.setTag('og:url','http://flikster.com');
          //this.imgs = [this.sanitizer.bypassSecurityTrustResourceUrl(res.imageGallery)];
         if(this.product.color)
         this.productcolor=this.product.color[0];
        //  if(this.product)
        // this.productsize=this.product.size[0];
          if(this.product)
          this.leadImage = this.product.profilePic;
          if(this.page=='product')
          if(this.product.celeb)
          this.getrelatedfliks(this.product.celeb[0].name);
          else
          this.getrelatedfliks(this.product.tags[0]); 
          if(this.page=='looks')
            this.getrelatedLooks();
        }

     },
     error => {
       console.log("error getting product");
     }
   );

  //  this.topMovies = this.topMoviesMock;
  //this.product = this.productMock;
 }
 getrelatedLooks(){
  this.productService.getAllLooks().subscribe(
    res => {
      console.log("relatedFliks service");
      console.log(res);
       if(res) {
         this.relatedLooks = res.Items;
         console.log( this.relatedFliks);
         }

    },
     error => {
       console.log("error getting relatedFliks");
     }
   );

 }
 getrelatedfliks(tag){
   this.relatedFliks=[];
  this.productService.getAllProducts(tag).subscribe(
    res => {
      console.log("relatedFliks service");
      console.log(res);
       if(res) {
         for(let i=0;i<res.hits.hits.length;i++){
          if(this.slug !=res.hits.hits[i]._source.slug) 
         this.relatedFliks.push(res.hits.hits[i]._source);
         }
         console.log( this.relatedFliks);
         }

    },
     error => {
       console.log("error getting relatedFliks");
     }
   );}

 getAuctionProduct(slug) {
  this.productService.getAuctionProductBySlug(slug).subscribe(
   res => {
     console.log("Product service");
     console.log(res);
      if(res) {
        if(res.hits.hits[0])
        this.Auctionproduct = res.hits.hits[0]._source;
        this.metaService.setTitle(' '+this.Auctionproduct.name);
        this.metaService.setTag('og:title',this.Auctionproduct.seo_title?this.Auctionproduct.seo_title:name);
        this.metaService.setTag('og:description',this.Auctionproduct.seo_description?this.Auctionproduct.seo_description:name);
        this.metaService.setTag('og:keywords',this.Auctionproduct.seo_keywords?this.Auctionproduct.seo_keywords.toString():name);
        // this.metaService.setTag('og:image',this.content.profilePic);
        this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        this.auctionId=this.Auctionproduct.id;
        this.isAuction =true;
        if(this.Auctionproduct.size)
        this.auctionsize=this.Auctionproduct.size[0];
       //this.imgs = [this.sanitizer.bypassSecurityTrustResourceUrl(res.imageGallery)];
        console.log(this.imgs);
        this.leadImage = this.Auctionproduct.profilePic;
        this.sizeArray= this.Auctionproduct.size;
        console.log(this.sizeArray);
        this.startingprice=Number(this.Auctionproduct.startingPrice);
        this.bidincrement=Number(this.Auctionproduct.bidIncrement);
        this.bidstarting=Number(this.startingprice+ this.bidincrement);
      //   this.time =  new Date(this.Auctionproduct.endDate).getTime()- new Date(this.today).getTime();
      // console.log(this.time);
      this.updateTime();
      this.gethighestbid(this.auctionId);
      this.getCurrentAuctions();

      }

   },
    error => {
      console.log("error getting product");
    }
  );

  //  this.topMovies = this.topMoviesMock;
  //this.product = this.productMock;
}
updateTime(){
  if(this.Auctionproduct)
      this.startDate = new Date( this.Auctionproduct.endDate);
      this.endDate = this.getPresentTime();
      this.miliseconds = this.startDate - this.endDate;
      this.daysLeft=Math.floor(this.miliseconds / 1000 /60 /60 /24);
      this.hoursLeft= Math.floor(this.miliseconds / 1000 /60 /60-this.daysLeft*24); 
      this.minutsLeft= Math.floor(this.miliseconds / 1000 /60- this.hoursLeft*60-this.daysLeft*60*24 ); 
      this.secondsLeft=Math.floor(this.miliseconds / 1000 - this.hoursLeft*60*60-this.daysLeft*60*60*24-this.minutsLeft*60);


}
getPresentTime(){
  return new Date(Date.now());
}

getCurrentAuctions(){
  this.productService.getRelatedAuctions().subscribe(
    res => {
      console.log(res);
       if(res) {
         this.currentData = res;
         console.log("********AUCTIONS DATA*********");
         console.log(this.currentData);
         for(let i=0;i< this.currentData.Items.length;i++){
           if(this.checkdate(this.currentData.Items[i].startDate,this.today)){
             
             this.icurrentarray.push(this.currentData.Items[i]);
             

           }
          //  else{
             
          //    this.upcomingarray.push(this.currentData.Items[i]);
            
          //  }

         }
         for(let i=0;i< this.icurrentarray.length;i++){
           if(this.checkdate(this.today,this.icurrentarray[i].endDate)){
             if(this.icurrentarray[i].slug != this.auctionslug)
             this.currentarray.push(this.icurrentarray[i]);  
             

           }
           

         }
        
         
        
      
         console.log(this.currentarray);
        //  console.log(this.upcomingarray);


       }

    },
    error => {
      console.log("error getting currentData");
    }
  );



}
getEndTime(item){

    this.relstartDate = new Date(item.endDate);
    this.relendDate = this.getPresentTime();
    this.relmiliseconds = this.relstartDate - this.relendDate;
    this.reldaysLeft=Math.floor(this.relmiliseconds / 1000 /60 /60 /24);
    this.relhoursLeft= Math.floor(this.relmiliseconds / 1000 /60 /60-this.reldaysLeft*24); 
    this.relminutsLeft= Math.floor(this.relmiliseconds / 1000 /60- this.relhoursLeft*60-this.reldaysLeft*60*24 ); 
    this.relsecondsLeft=Math.floor(this.relmiliseconds / 1000 - this.relhoursLeft*60*60-this.reldaysLeft*60*60*24-this.relminutsLeft*60);
    
    return true;

}
public checkdate(date1,date2){
  console.log(date1);
  console.log(date2);
return (new Date(date2) > new Date(date1));


}

  addToBag() {
    if(localStorage.getItem("isLoggedIn") == "true") {
      if(this.product.size && !this.productsize){ 
        this.displayText='Please select size';
       }
      else{
      let bagObject = this.createObjectForBag();
        this.productService.addToCart(bagObject).subscribe(
        res => {
          console.log(res);
          this.addedToBag = true;
        },
        error => {
          console.log("error adding to cart");
        }
      );
    }
    } else {
      let urlBreakup = this.router.url.split('/');
      urlBreakup.splice(0, 1);
      let redirect = urlBreakup.join('/');
      localStorage.setItem("preLogin",redirect);
      // this.router.navigate(['login']);  
      this.displayLoginPopup = true; 
      console.log("redirect to login");
    }
  }

  createObjectForBag()
   {
    if(this.product.isDiscount==true){
    let obj = {
      userId : localStorage.getItem("username"),
      productId : this.product.id,
      productDetails :  {
        productId : this.product.id,
        productTitle : this.product.name,  
        productSlug : this.product.slug,
        productPic : this.leadImage,
        color: this.productcolor,
        size : this.productsize,
        price : this.product.discountObj.discPrice,
        movieTags : this.product.movie? this.product.movie : [],
        celebTags : this.product.celeb? this.product.celeb : [],
        createdDate : this.product.createdDate,
        quantity : String(this.counter),
      },
      
      // size : "S",
      // color : "Red"
    }
    return obj;
  }
  else{
    let obj = {
      userId : localStorage.getItem("username"),
      productId : this.product.id,
      productDetails :  {
        productId : this.product.id,
        productTitle : this.product.name,  
        productSlug : this.product.slug,
        productPic : this.leadImage,
        color: this.productcolor,
        size : this.productsize,
        price : this.product.price,
        movieTags : this.product.movie? this.product.movie : [],
        celebTags : this.product.celeb? this.product.celeb : [],
        createdDate : this.product.createdDate,
        quantity : String(this.counter),
      },
      
      // size : "S",
      // color : "Red"
    }
    return obj;
  }

   
  }
//  this.topMovies = this.topMoviesMock;
//this.product = this.productMock;
//  }
gethighestbid(slug) {
this.productService.gethighestbid(slug).subscribe(
 res => {
   console.log("Product service");
   console.log(res);
    if(res) {
      this.auctionBidding = res;
      this.bidmore=Number(this.auctionBidding.highestBid?this.auctionBidding.highestBid:this.bidstarting)+this.bidincrement;
      this.bidCounts=res.bidCount;
     }

 },
 error => {
   console.log("error getting product");
 }
);

//  this.topMovies = this.topMoviesMock;
//this.product = this.productMock;
}
gotoProduct(slug){
  this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
  this.router.navigate(['product/' + slug]));
 
}
gotoAuctionProduct(slug){
  this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
  this.router.navigate(['product/auction/' + slug]));
  
  }
showbids(){
this.showbid= !this.showbid;
if(this.showbid==true)
this.showbidData(this.auctionId);
}
setMaxPrice(currentbid,maxPrice){
  if(maxPrice){
    if(0.8*maxPrice<currentbid)
  this.currentMaxPrice= Math.floor(currentbid*2);
  else{
    this.currentMaxPrice=maxPrice;
  }
  return true;
  }
  else{
  this.currentMaxPrice= Math.floor(1.8*currentbid);
  return true;
  }

}
showbidData(slug){
this.productService.showbiddingData(slug).subscribe(
  res => {
    console.log("Product service");
    console.log(res);
     if(res) {
       this.bidArray=res.bids;
       for(let i=0;i<this.bidArray.length;i++){
       this.bidArray[i].bidAmount=Math.floor(this.bidArray[i].bidAmount);
       if(this.userids.indexOf(this.bidArray[i].userId)==-1){
         this.userids.push(this.bidArray[i].userId);
       }

       }

      }

  },
  error => {
    console.log("error getting product");
  }
);

}
gotoLooks(slug){
  this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
  this.router.navigate(['looks/' + slug]));

}


  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }

  // toggleAuction() {
  //   this.isAuction = !this.isAuction;
  // }

  private productMock = [
    {
      productTitle : "Fidaa",
      productSlug : "fidaa",
      profilePic : "<image address>",
      averageRating : 8.6,
      productPrice : "$ 999"
  },
    
];

subcounter(number){

  if(number!=1){
    this.counter=this.counter -1;

  }
}
refresh(){

  this.gethighestbid(this.auctionId);
}

getStealThatStyle(industry){
  this.fashionService.stealThatStyle(industry).subscribe(
    res => {
      console.log(res);
      if(res){
        this.stealThatStyle = res;
      }
    },
    error => {
      console.log("Error in stealThatStyle");
    }

  );
}
checktime(date){
  // this.time = new Date(this.today).getTime() - new Date(date).getTime();
  return true;
}

enterbid(){
  
 if(localStorage.getItem('isLoggedIn'))
 {
  this.enter=true;
  this.submitted=false;
  this.bidprice=0;
 }
 else
  // this.router.navigate(['login']);  
    this.displayLoginPopup = true; 
  
}
placebid(){
  var loggedin=localStorage.getItem("isLoggedIn");
  if(loggedin=='true'){
  if ( this.bidprice > ((this.auctionBidding)? this.bidmore:this.bidstarting)){
    
    // this.bidAmount=this.bidprice;
    this.bidCounts= this.bidCounts+1;
    // this.auctionBidding=true;
    var userId=localStorage.getItem('username');
    
    let bidding={
      
      'auctionId':this.Auctionproduct.id,
      'userId': userId,
      'bidAmount':String(Math.floor(this.bidprice)),
    // 'bidValue' :this.bidprice,
    // 'userId':this.Auctionproduct.id,
    };
    this.auctionbidding(bidding);
  }
  else{
    this.enter=false;
  }
}else {
      let urlBreakup = this.router.url.split('/');
      urlBreakup.splice(0, 1);
      let redirect = urlBreakup.join('/');
      localStorage.setItem("preLogin",redirect);
      // this.router.navigate(['login']);  
    this.displayLoginPopup = true; 
}
this.bidprice=null;
}

placeOrder(){
  var loggedin=localStorage.getItem("isLoggedIn");
  if(loggedin=='true'){
  if (this.product){
    if(this.page=='product'){
      if( this.product.size  && !this.productsize){
       this.displayText='Please select size';
      }
      else{
      if(this.product.isDiscount==true){
      let order={
        product :[
            {
            'productId' : this.product.id,
            'productTitle':this.product.name,
            'productSlug' : this.product.slug,
            'productPic': this.leadImage,
            'color':String(this.productcolor),
            'price':String(this.product.discountObj.discPrice),
            'size': String(this.productsize),
            
            'quantity':String(this.counter),
            'movieTags' : this.product.movie? this.product.movie : [],
            'celebTags' : this.product.celeb? this.product.celeb : [],
            'createdDate' : this.product.createdDate
          }
              ],
        userId : localStorage.getItem("username"),
        userName : String(localStorage.getItem("name")),
        status : "pending"
      };

  console.log(order);
  this.Order(order);
    }
    else {
      let order={
        product :[
            {
            'productId' : this.product.id,
            'productTitle':this.product.name,
            'productSlug' : this.product.slug,
            'productPic': this.leadImage,
            'color':String(this.productcolor),
            'price':String(this.product.price),
            'size': String(this.productsize),
            
            'quantity':String(this.counter),
            'movieTags' : this.product.movie? this.product.movie : [],
            'celebTags' : this.product.celeb? this.product.celeb : [],
            'createdDate' : this.product.createdDate
          }
              ],
        userId : localStorage.getItem("username"),
        userName : String(localStorage.getItem("name")),
        status : "pending"
      };

  console.log(order);
  this.Order(order);
    }
  }
}
if(this.page=='looks'){
  if(!this.productsize){
       this.displayText='Please select size';
      }
  else{
    if(this.product.isDiscount==true){
      let order={
    'product' :[
        {
          'productId' : this.product.id,
          'productTitle':this.product.name,
          'productSlug' : this.product.slug,
          'productPic': this.leadImage,
          'color':this.productcolor,
          'price':String(this.product.discountObj.discPrice),
          'size': this.productsize,
          'quantity':String(this.counter),
        },
        // Quantity : this.quantity,
    ],
    userId : localStorage.getItem("username"),
    userName : String(localStorage.getItem("name")),
    'status' : "pending"
  };
    console.log(order);
    this.Order(order);
    }
    else {
  let order={
    'product' :[
        {
          'productId' : this.product.id,
          'productTitle':this.product.name,
          'productSlug' : this.product.slug,
          'productPic': this.leadImage,
          'color':this.productcolor,
          'price':this.product.price,
          'size': this.productsize,
          'quantity':String(this.counter),
        },
        // Quantity : this.quantity,
    ],
    userId : localStorage.getItem("username"),
    userName : String(localStorage.getItem("name")),
    'status' : "pending"
  };
  
    console.log(order);
    this.Order(order);
  }
  }
  }
}

if (this.Auctionproduct){
  let order={
    'product' :[
     {
     'productId' : this.Auctionproduct.id,
     'productTitle':this.Auctionproduct.name,
     'productSlug' : this.Auctionproduct.slug,
     'productPic': this.leadImage,
     'price':String(this.currentMaxPrice),
     
     'size': String(this.auctionsize),
     'quantity':String(this.counter),
     'color':this.Auctionproduct.color[0],
    }],
    userId : localStorage.getItem("username"),
    userName : String(localStorage.getItem("name")),
    'status' : "pending"
};
 console.log(order);
 this.Order(order);
}}
else  {
      let urlBreakup = this.router.url.split('/');
      urlBreakup.splice(0, 1);
      let redirect = urlBreakup.join('/');
      localStorage.setItem("preLogin",redirect);
      // this.router.navigate(['login']);  
    this.displayLoginPopup = true; 
}
}

Order(body){
  this.productService.postOrder(body).subscribe(
    res => {
        console.log(res);
        this.orderid =res.id;
        if(this.Auctionproduct)
        this.router.navigate(['auction/checkout/'+this.orderid]);
        else
        this.router.navigate(['checkout/'+this.orderid]);
      },
      error => {
        console.log("Error in posting orders");
      }
);
}
auctionbidding(body){
  this.productService.auctionbidding(body).subscribe(
    res => {
        console.log(res);
        this.success=res;
        this.submitted=true;
        this.gethighestbid(this.auctionId);
       },
      error => {
        console.log("Error in posting orders");
      }
);


}

public displayLoginPopup : boolean = false;

closeLoginPopup () {
  console.log("event emmitted");
  this.displayLoginPopup = false;
}
close(){
  this.showbid=!this.showbid;
}

getapp(){
  var loggedin=localStorage.getItem("isLoggedIn");
  if(loggedin=='false')
  this.displayLoginPopup = true; 
  else{
this.productsArray=[];
  

    for(let i=0;i<this.lookproduct.products.length;i++){

      this.productService.getProductBySlug(this.lookproduct.products[i].productSlug).subscribe(

        res => {

          console.log("Product service");

          console.log(res);

           if(res) {

            this.productsArray.push(res.hits.hits[0]._source);

             

           }
  
       });
     
     }
     this.get = true; 
    }
  
   }
private changingOrder(changedOrder)
    {
      if(changedOrder){
        console.log(changedOrder);
        this.product[this.item].quantity=changedOrder.quantity;
        this.product[this.item].color=changedOrder.color;
        this.product[this.item].size=changedOrder.size;
        this.get = false; 
       
      }
       else
       this.get = false; 
      }
}
