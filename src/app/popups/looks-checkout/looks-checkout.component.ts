import {

  Component,

  OnInit,

  Input,

  EventEmitter,

  Output,



} from '@angular/core';



//import { AppState } from '../../app.service';

import { ProductService } from "../../models/product/product.service";

import { Router, ActivatedRoute } from '@angular/router';


@Component({

  

  selector: 'looks-checkout', 

  providers: [ ],

  styleUrls: [ './looks-checkout.component.css' ],

  templateUrl: './looks-checkout.component.html'

})

export class LooksCheckoutComponent implements OnInit {

    orderId: any;

 public productcolor=[];

 public productquant: any;

 public  productsize=[];

  public sendObject: any;


  public close: boolean = true;

  public localState = { value: '' };

  public get : boolean = false;
  public productArray :any;

  @Input() sendObjects:any;

  @Input() productOrder:any;

  @Input() item:any;

  @Input() id:any;

  @Output() changingOrder : EventEmitter<any> = new EventEmitter();

  @Input() looksData: any;

@Output() productPivot : EventEmitter<any> = new EventEmitter();

  constructor(

    //public appState: AppState,

    public productService:ProductService,

    public router : Router,

  ) {}



  public ngOnInit() {
    this.productsize=[];

    console.log('hello `get-the-app` component');

    console.log(this.looksData);
    for(let i=0;i<this.looksData.length;i++){
      if(this.looksData[i].size){
        this.productsize.push(this.looksData[i].size[0])
      }
    }

    this.sendObjects=this.looksData;


  }
  emptyProductArray() {
      return {
        productId : "",
        productTitle : "",
        productPic : "",
        price : "",
        size : "",
        quantity : "",
        productSlug :"",
      };
    }


  closePopup(){

    this.changingOrder.next();

    

  }

  subcounter(number){

    

        if(number!=1){

          this.sendObjects.quantity=+this.sendObjects.quantity -1;

    

        }

      }

  placeOrder(){
    this.productArray=[];
    for(let i=0;i<this.looksData.length; i++){
      
      var productDetails = this.emptyProductArray();
      productDetails.productId =this.looksData[i].id;
      productDetails.productTitle =this.looksData[i].name;
      productDetails.productPic = this.looksData[i].profilePic;
      productDetails.size = this.productsize[i];
      productDetails.quantity = '1';
      productDetails.price = this.looksData[i].price;
      productDetails.productSlug = this.looksData[i].slug;
      console.log(productDetails);
      this.productArray.push(productDetails);
    }

   

    if(this.looksData.length >0){
     let order = {
       
          'product' :this.productArray,
       
           userId : localStorage.getItem("username"),
          userName : String(localStorage.getItem("name")),
          'status' : "pending"

         

        };

        console.log(order);

        this.Order(order);

    }

  }

  Order(body){

    this.productService.postOrder(body).subscribe(

      res => {

          console.log(res);

          console.log(res.id);

          this.router.navigate(['checkout/'+res.id]);

        },

        error => {

          console.log("Error in posting orders");

        }

    );

  }

  removeItem() {

    this.productOrder.product.splice(this.item,1);

    this.updateOrder(this.productOrder,this.id);

    this.changingOrder.next();

    // this.list = this.list.filter(item => item.id !== id);

}

  add(){

    this.sendObjects.quantity= +this.sendObjects.quantity+1;

  }

  updateOrder(order,id) {

    this.productService.updateOrders(order,id).subscribe(

     res => {

       console.log("updateOrder service");

       console.log(res);

        if(res) {

          console.log(res);

         

         

        }


     },

     error => {

       console.log("error updateOrder product");

     }

   );


  //  this.topMovies = this.topMoviesMock;

  //this.product = this.productMock;

 }



  getProduct(slug) {

    this.productService.getProductBySlug(slug).subscribe(

     res => {

       console.log("Product service");

       console.log(res);

        if(res) {

          this.sendObject = res.hits.hits[0]._source;

      

          this.orderId = this.sendObject;

      

          console.log(this.orderId);

         

        }


     },

     error => {

       console.log("error getting product");

     }

   );


  //  this.topMovies = this.topMoviesMock;

  //this.product = this.productMock;

 }



  public submitState(value: string) {

    console.log('submitState', value);

    //this.appState.set('value', value);

    this.localState.value = '';

  }

}
