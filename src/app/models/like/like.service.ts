import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class LikeService {
    private environmentUrl :  any = environment;


  constructor(
  	    private http: Http,
    ) { 

  }

  isLiked(body) : Observable<any>{
  	let likedUrl = this.environmentUrl.baseUrl + "likes-ms/isPostStatus";
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(likedUrl, body, options)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getUserPosts(body) : Observable<any>{
  	let likedUrl = this.environmentUrl.baseUrl + "likes-ms/getUserPosts";
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(likedUrl, body, options)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  // like(obj : any) : Observable<any>{
  //     let likeUrl = this.baseUrl + "postCardStatus";
  //     let body = obj;
  //     let headers = new Headers({ 'Content-Type': 'application/json' });
  //   //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
  //     let options = new RequestOptions({ headers: headers });

  //     return this.http.post(likeUrl, body, options)
  // 		.map((res:Response) => res.json())
  // 		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  // }

  like(body) : Observable<any>{
      let likeUrl = this.environmentUrl.baseUrl + "likes-ms/postCardStatus";
      let headers = new Headers({ 'Content-Type': 'application/json' });
    //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
      let options = new RequestOptions({ headers: headers });
      return this.http.post(likeUrl, body, options)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
