import { Component, OnInit, Input,Compiler } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
// import { AppState } from '../../app.service';
import { Router } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import { Location } from '@angular/common';
  
  @Component({
    
    selector: 'card-youtube',   
    providers: [ ],
    styleUrls: [ './card-youtube.component.css' ],
    templateUrl: './card-youtube.component.html'
  })
  export class CardyoutubeComponent implements OnInit {
    shareProfilePic: any;
    shareSlug: any;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    isMiniCard: any;
    relatedfliks: boolean;
    public assnData: any;
    public today: number = Date.now();
    @Input() height: number;
    @Input() width: any;
    @Input() id: any;
    @Input() content;
    @Input() miniCard;
    public iframe_html: any;
    public cardId;
    public cardSlug :any;
    public localState = { value: '' };
    public contentData;
    public page: string;
    public videoUrl;
    public contentType;

    constructor(
      // public appState: AppState,
      private embedService: EmbedVideoService,
      public router : Router,
      public sanitizer: DomSanitizer,
      private _compiler: Compiler,
      private _location: Location,
    ) {
      
    }
  
    public ngOnInit() {
      this._compiler.clearCache();
      var urlBreakup = this.router.url.split("/");
      console.log(urlBreakup);
      this.page = urlBreakup[1]; 
      this.cardSlug=urlBreakup[2];
      this.isMiniCard= this.miniCard;
      this.contentData = this.content;
      this.contentType=this.contentData.contentType;
      this.createdAt=this.content.createdAt;
      // this.contentData.media.video = "https://www.youtube.com/embed/yca6UsllwYs";
      this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
      if(typeof this.contentData.media.video == 'string') {
        this.videoUrl = this.contentData.media.video;
      } else {
        this.videoUrl = this.contentData.media.video[0];
      }
      if(this.page=='content'){
      this.getEmbedVideoUrl(this.videoUrl);}
      
      if(this.contentData.celeb)
        if(this.contentData.celeb[0])
      this.assnData=this.contentData.celeb[0];
       if(this.contentData.movie)
        if(this.contentData.movie[0])
        this.assnData=this.contentData.movie[0];
      console.log(this.assnData);
      // if(!this.contentData.movie &&!this.contentData.celeb)
      //   this.relatedfliks=true;
      console.log("********VIDEO CONTENT*********");
      console.log(this.contentData);
      console.log(this.contentData.media.video);

      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='s';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
       
    }

    getEmbedVideoUrl(url) {
      console.log(url);
      if(url.split('/')[2] == "www.youtube.com") {
        if(url.split('/')[3] == "embed") {
          this.videoUrl = url;
        } else {
          this.videoUrl = "https://www.youtube.com/embed/" + url.split('/')[3].split('=')[1];
        }
      } else if(url.split('/')[2] == "www.dailymotion.com") {
          
      } else {
        this.videoUrl = "https://www.youtube.com/embed/" + url.split('/')[3];
      }
      console.log(this.videoUrl);
    }
    gotocontent(){
      if(this.cardSlug !=this.contentData.slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }
  